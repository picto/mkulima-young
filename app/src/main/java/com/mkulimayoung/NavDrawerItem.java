/**
 * 
 */
package com.mkulimayoung;

/**
 * @author robb
 *
 */
public class NavDrawerItem {
	private String mTitle;
	private String subTitle;
	private int mIcon;

	public NavDrawerItem() {
	}

	public NavDrawerItem(String title, int icon) {
		this.mTitle = title;
		this.mIcon = icon;
	}

	public String getTitle() {
		return this.mTitle;
	}

	public int getIcon() {
		return this.mIcon;
	}

	public void setTitle(String title) {
		this.mTitle = title;
	}

	public void setIcon(int icon) {
		this.mIcon = icon;
	}

	/**
	 * @return the subTitle
	 */
	public String getSubTitle() {
		return subTitle;
	}

	/**
	 * @param subTitle
	 *            the subTitle to set
	 */
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
}
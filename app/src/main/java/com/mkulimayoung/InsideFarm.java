package com.mkulimayoung;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.firebase.client.Firebase;
import com.mkulimayoung.mkulimaApp.R;

import org.joda.time.DateTime;

import database.Database;
import io.realm.Realm;
import models.User;
import tools.Configs;

/**
 * Created by robertnyangate on 4/1/16.
 */
public class InsideFarm extends Fragment {
    Realm realm;
    LinearLayout loader;
    private FragmentManager fmanager;
    private android.support.v4.app.FragmentTransaction ft;
    private Bundle savedInstance;
    private DateTime lastcheck;
    private Handler handler = new Handler();

    public static InsideFarm newInstance(FragmentManager fm) {
        InsideFarm sp = new InsideFarm();
        return sp;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        savedInstance=savedInstanceState;
        realm = Configs.getRealmInstance(getActivity());
        Firebase.setAndroidContext(getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null)
            return null;
        View v = inflater.inflate(R.layout.farmside, container, false);
        /*
        * General farm questions
        * */
        v.findViewById(R.id.quiz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),MQuestions.class));
            }
        });

       /*Direct to creating orders
       * Buyer creating shopping-list
       * */
        v.findViewById(R.id.order).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(realm.allObjects(User.class).size()>0)
                startActivity(new Intent(getActivity(),ChannelHandlerActivity.class));
                else
                    startActivityForResult(new Intent(getActivity(),UserAuthenticationActvity
                            .class),10001);
            }
        });
        /*
        * Pest and disease control
        * */
        v.findViewById(R.id.pests).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),DiseaseControl.class));
            }
        });
        /*
        * Advice on selling
        * */
        v.findViewById(R.id.advice).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),HowToSell.class));

            }
        });
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {



    }




}

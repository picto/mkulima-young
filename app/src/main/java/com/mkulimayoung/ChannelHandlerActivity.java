package com.mkulimayoung;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.mkulimayoung.mkulimaApp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import database.Database;
import io.realm.Realm;
import models.BuyerRequest;
import models.FAnswer;
import models.FQuestioin;
import models.FirebaseListAdapter;
import models.User;
import tools.Configs;

import static com.mkulimayoung.ChatRoom.answers;

/**
 * Created by robertnyangate on 3/30/16.
 */
public class ChannelHandlerActivity extends AppCompatActivity {

    MixpanelAPI mixpanelAPI;
    Realm realm;
    ListView listView;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.howlist);
        mixpanelAPI = Configs.getMixpanel(this);
        realm = Configs
                .getRealmInstance(this);

        listView=(ListView)findViewById(android.R.id.list);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Buy list");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(realm.allObjects(User.class).size()<1){
            startActivityForResult(new Intent(this,UserAuthenticationActvity.class),1020);
        }else{
            loadProducts();
        }


    }
    Dialog pDialog;

    private void showProgress(String message) {
        pDialog = new MaterialDialog.Builder(this)
                .title("Orders")
                .content(message)
                .progress(true, 100).build();
        pDialog.show();


    }

    private void dismissMyDialog() {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }

    private void loadProducts() {
        Firebase mFirebaseRef=Configs.getOrdersFireBase();
        Query queryRef = mFirebaseRef.orderByValue().limitToLast(14);
        showProgress("Loading orders");
        queryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()==null){
                    Toast.makeText(ChannelHandlerActivity.this, "No Quizes", Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        final FirebaseListAdapter<BuyerRequest> quizAdapter=new FirebaseListAdapter<BuyerRequest>(queryRef,
                BuyerRequest.class,
                R.layout.buyer_items, this) {

            @Override
            protected void populateView(View v, BuyerRequest model) {
                dismissMyDialog();
                ((TextView)v.findViewById(R.id.title)).setText(model
                        .getProduct());
                ((TextView)v.findViewById(R.id.user)).setText(model.getUser());
                ((TextView)v.findViewById(R.id.quizid)).setText(model.getId());
                ((TextView)v.findViewById(R.id.desc)).setText(model.getDescription());
                ((TextView)v.findViewById(R.id.location)).setText(model.getCountry()+" - "+model
                        .getLocation());
            }


        };
        listView.setAdapter(quizAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String quzid= ((TextView)view.findViewById(R.id.quizid)).getText().toString();
                String question= ((TextView)view.findViewById(R.id.title)).getText().toString();
                String user= ((TextView)view.findViewById(R.id.user)).getText().toString();
                Intent intent=new Intent(ChannelHandlerActivity.this,ChatRoom.class);
                intent.putExtra("question",question+" - "+((BuyerRequest)quizAdapter.getItem
                        (position)).getDescription());
                intent.putExtra("channel",user);
                intent.putExtra("user",user);
                intent.putExtra("quizid",quzid);
                intent.putExtra("email",((BuyerRequest)quizAdapter.getItem(position)).getEmail()
                        !=null?((BuyerRequest)quizAdapter.getItem(position)).getEmail():"");
                answers.clear();
                if(((BuyerRequest)quizAdapter.getItem(position)).getEmail()
                        !=null) {
                    answers.put(((BuyerRequest) quizAdapter.getItem(position)).getEmail()
                                    != null ? ((BuyerRequest) quizAdapter.getItem(position)).getEmail() : ""
                            , new FAnswer(question, user, "", quzid, "", ((BuyerRequest) quizAdapter.getItem(position)).getEmail()
                                    != null ? ((BuyerRequest) quizAdapter.getItem(position)).getEmail() : ""));
                }
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.buyit, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.register){
            CreateQuestionDialog();
        }
        if(item.getItemId()==android.R.id.home){
           onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    AppCompatSpinner spinner;
    private void CreateQuestionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.buyitnow, null);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(v);
        // Add action buttons
        AppCompatButton postQuiz=(AppCompatButton)v.findViewById(R.id.postit);
          spinner=(AppCompatSpinner)v.findViewById(R.id.spinnercats);
        final EditText name=(EditText)v.findViewById(R.id.pname);
        final EditText town=(EditText)v.findViewById(R.id.town);
        final EditText desc=(EditText)v.findViewById(R.id.description);


        builder.setTitle("New Order: ");
        final AlertDialog dialog = builder.create();
        postQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(TextUtils.isEmpty(name.getText())){
                   name.setError("name");
               }else  if(TextUtils.isEmpty(desc.getText())){
                   desc.setError("description");
               } if(TextUtils.isEmpty(town.getText())){
                    town.setError("town");
                }else{
                    addOrder(name.getText().toString(),town.getText().toString(),desc.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void addOrder(final String name, final String town, String desc) {
        Firebase ref=Configs.getOrdersFireBase();

        BuyerRequest req=new BuyerRequest(name,desc,town,Database.properFormat.format(new Date()),realm.allObjects(User.class).first()
                .getUsername()+" "+realm.allObjects(User.class).first()
                .getMobile(),UUID.randomUUID().toString
                (),realm.allObjects(User.class).first()
                .getEmail());
        req.setCountry(spinner.getSelectedItem().toString());
        showProgress("Posting..");


        ref.push().setValue(req, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                dismissMyDialog();
                loadProducts();
                trackEvent(name,town,"","");
            }
        });


    }
    private void trackEvent(String itemname, String town, String country, String price) {
        try {
            JSONObject props = new JSONObject();
            props.put("email", realm.allObjects(User.class).first().getEmail());
            props.put("phone", realm.allObjects(User.class).first().getMobile());
            props.put("product", itemname);
            props.put("town", town);
            props.put("country", country);
            props.put("description", price);
            props.put("time", new Date().toString());
            mixpanelAPI.track("BuyerEvent", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
        trackRegion(itemname, town, country);
    }
    private void trackRegion(String itemname, String town, String country) {
        try {
            JSONObject props = new JSONObject();
            props.put("product", itemname);
            props.put("town", town);
            props.put("country", country);
            props.put("time", new Date().toString());
            mixpanelAPI.track("RegionBuyerCrops", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mixpanelAPI.flush();
    }
}

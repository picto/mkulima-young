/**
 * 
 */
package com.mkulimayoung;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mkulimayoung.mkulimaApp.R;

/**
 * @author robb
 * 
 */
public class  Events_Frag extends DialogFragment {
	private FragmentManager fmanager;
	private android.support.v4.app.FragmentTransaction ft;
	private EventsBean events;

	public static Events_Frag newInstance(FragmentManager fm,
			EventsBean type) {
		Events_Frag sp = new Events_Frag();
		sp.fmanager = fm;
		sp.events = type;
		return sp;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
        //FacebookSdk.sdkInitialize(getActivity());

	}
private TextView title,venue,time,detail;
    private Button addEvent;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null)
			return null;
		View v = inflater.inflate(R.layout.newevent, container, false);
        addEvent=(Button)v.findViewById(R.id.calendar);
        title=(TextView)v.findViewById(R.id.title);
        venue=(TextView)v.findViewById(R.id.venue);
        time=(TextView)v.findViewById(R.id.schedule);
        detail=(TextView)v.findViewById(R.id.details);

        title.setHint(events.getTitle());
        detail.setHint(events.getBrief());
        venue.setHint(events.getVenue());
        time.setHint(events.getStartDate()+" "+events.getStartTime()+" > "+events.getEndDate()+" " +
                ""+events.getEndTime());
        Toast.makeText(getActivity(),"title: "+title.getHint().toString(),Toast.LENGTH_LONG).show();
        return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
		//displayEvents(events);

	}

	/**
	 * 
	 /**
	 * 
	 * @param result
	 */
	protected void displayEvents(ArrayList<EventsBean> result) {
		final EventsAdapter adapter = new EventsAdapter(getActivity(),
				R.layout.events_layout, result);
		//setListAdapter(adapter);

	}

	/**
	 * @param result
	 */
	protected void displayDates(ArrayList<EventsBean> result) {
		ft = fmanager.beginTransaction();
		ft.replace(R.id.content, EventDatesF.newInstance(fmanager));
		ft.addToBackStack(null);
		ft.commit();
	}
}

package com.mkulimayoung;

//session object creator
public class UserDetails {

	private String user_name;
	private int wv_uid;
	private String user_email;
	private String user_cell;
	private String full_name;

	public UserDetails() {

	}

	public String getusername() {

		return this.user_name;
	}

	public void setusername(String value) {

		this.user_name = value;
	}

	public int getuserid() {

		return this.wv_uid;
	}

	public void setuserid(int value) {

		this.wv_uid = value;
	}

	/**
	 * @return the user_email
	 */
	public String getUser_email() {
		return user_email;
	}

	/**
	 * @param user_email
	 *            the user_email to set
	 */
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	/**
	 * @return the user_cell
	 */
	public String getUser_cell() {
		return user_cell;
	}

	/**
	 * @param user_cell
	 *            the user_cell to set
	 */
	public void setUser_cell(String user_cell) {
		this.user_cell = user_cell;
	}

	/**
	 * @return the full_name
	 */
	public String getFull_name() {
		return full_name;
	}

	/**
	 * @param full_name
	 *            the full_name to set
	 */
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

}

/**
 * 
 */
package com.mkulimayoung;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.mkulimayoung.mkulimaApp.R;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import database.Database;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import models.FAnswer;
import models.FQuestioin;
import models.FirebaseListAdapter;
import models.Question;
import models.User;
import models.UserDevice;
import tools.Configs;

import static com.mkulimayoung.ChatRoom.answers;

/**
 * @author robb
 * 
 */
public class MQuestions extends AppCompatActivity {
    Realm realm;
    LinearLayout loader;
    private FragmentManager fmanager;
    private android.support.v4.app.FragmentTransaction ft;
private Bundle savedInstance;
    Toolbar toolbar;
    private DateTime lastcheck;
//    Spinner catspinner;
    int selection=0;
    private Handler handler = new Handler();
    public static ArrayList<Question>questions;


	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
        savedInstance=savedInstanceState;
        setContentView(R.layout.questions_list);
        realm = Configs.getRealmInstance(this);
//        userId= realm.allObjects(User.class).first()
//                .getMobile();
        Firebase.setAndroidContext(this);
//       userName= "User-" + userId.substring(0, 4);
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
        loader=(LinearLayout)findViewById(R.id.loader);
        listView=(ListView)findViewById(android.R.id.list);
//        catspinner=(Spinner)findViewById(R.id.catspinner);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String[] categ=getResources().getStringArray(R.array.categoriesnew);
//        catspinner.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,
//                android.R
//                .id.text1,
//                categ));
        showQuestions();

	}

    private void showQuestions() {
        Firebase mFirebaseRef=Configs.getQuestionsFireBase();
        Query queryRef = mFirebaseRef.orderByValue().limitToLast(10);
        showProgress("Loading questions");
        FirebaseListAdapter<FQuestioin> quizAdapter=new FirebaseListAdapter<FQuestioin>(queryRef,
                FQuestioin.class,
                R.layout.quiz_adapter, this) {

            @Override
            protected void populateView(View v, FQuestioin model) {
                dismissMyDialog();
                ((TextView)v.findViewById(R.id.title)).setText(model.getTitle());
                ((TextView)v.findViewById(R.id.user)).setText(model.getUser());
                ((TextView)v.findViewById(R.id.email)).setText(model.getEmail()==null?"none":model
                        .getEmail());
                ((TextView)v.findViewById(R.id.quizid)).setText(model.getQuizid());
            }


        };

        listView.setAdapter(quizAdapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               String quzid= ((TextView)view.findViewById(R.id.quizid)).getText().toString();
               String question= ((TextView)view.findViewById(R.id.title)).getText().toString();
               String user= ((TextView)view.findViewById(R.id.user)).getText().toString();
               String email= ((TextView)view.findViewById(R.id.email)).getText().toString();
                Intent intent=new Intent(MQuestions.this,ChatRoom.class);
                intent.putExtra("question",question);
                intent.putExtra("channel",user);
                intent.putExtra("quizid",quzid);
                intent.putExtra("email",email);
                answers.clear();
                answers.put(email,new FAnswer(question,user,"",quzid,"",email));
                startActivity(intent);
            }
        });
//        findViewById(R.id.titlealert).setVisibility(View.VISIBLE);

    }

    // Join a messaging channel.
    private static final int REQUEST_SENDBIRD_CHAT_ACTIVITY = 100;
    private static final int REQUEST_SENDBIRD_CHANNEL_LIST_ACTIVITY = 101;
    private static final int REQUEST_SENDBIRD_MESSAGING_ACTIVITY = 200;
    private static final int REQUEST_SENDBIRD_MESSAGING_CHANNEL_LIST_ACTIVITY = 201;
    private static final int REQUEST_SENDBIRD_USER_LIST_ACTIVITY = 300;
    public static final String appId = "43128A44-C88D-4BA3-BEC6-942652A6BEBC"; /* Sample SendBird
    Application */
//    static String userId =null;
//    static  String userName = null; /* Generate User Nickname */

ListView listView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_menu, menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.postquiz)
            startDialog();
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    Dialog quizDialog;
    private void startDialog() {
        if(realm.allObjects(User.class).size()<1){
            startActivity(new Intent(this,UserAuthenticationActvity.class));

        }else{
        CreateQuestionDialog();}
    }

    private void CreateQuestionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.ask_layout, null);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(v);
        // Add action buttons
        AppCompatButton postQuiz=(AppCompatButton)v.findViewById(R.id.postit);
        final AppCompatSpinner spinner=(AppCompatSpinner)v.findViewById(R.id.spinnercats);
        final EditText quizit=(EditText)v.findViewById(R.id.quizit);


        builder.setTitle("New Question: ");
        final AlertDialog dialog = builder.create();
        postQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(quizit.getText().length()<4){
                    Functions.showToast("Invalid quiz length",MQuestions.this);
                    quizit.setError("Invalid length");
                }else {
                    dialog.dismiss();

                    addQuestion(quizit.getText().toString(), spinner.getSelectedItem().toString
                            ());

                }
            }
        });
       dialog.show();
    }
    Dialog pDialog;

    private void showProgress(String message) {
        pDialog = new MaterialDialog.Builder(this)
                .title("Questions")
                .content(message)
                .progress(true, 100).build();
        pDialog.show();


    }

    private void dismissMyDialog() {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
    private void addQuestion(String question,String channel) {
//        Database database=new Database(this);
//        database.open();
//        database.createQuestion(question, channel, realm.allObjects(User.class).first().getId());
//        database.close();
        Firebase ref=Configs.getQuestionsFireBase();
//        Map<String, String> newquestion = new HashMap<String, String>();
//        newquestion.put("title", question);
//        newquestion.put("channel", channel);
//        newquestion.put("user", realm.allObjects(User.class).first().getMobile());
//        newquestion.put("date", Database.properFormat.format(new Date()));
        FQuestioin fQuestioin=new FQuestioin(question,realm.allObjects(User.class).first()
                .getUsername(),Database.properFormat.format(new Date()), UUID.randomUUID()
                .toString(),realm.allObjects(User.class).first()
                .getEmail());

        showProgress("Posting..");


        ref.push().setValue(fQuestioin, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                showQuestions();
                dismissMyDialog();
            }
        });

    }
}

/**
 * 
 */
package com.mkulimayoung;

public class ProductUpload {

	public ProductUpload() {

	}

	/**
	 * @return the pdct_title
	 */
	public String getPdct_title() {
		return pdct_title;
	}

	/**
	 * @param pdct_title
	 *            the pdct_title to set
	 */
	public void setPdct_title(String pdct_title) {
		this.pdct_title = pdct_title;
	}

	/**
	 * @return the pdct_unitpce
	 */
	public String getPdct_unitpce() {
		return pdct_unitpce;
	}

	/**
	 * @param pdct_unitpce
	 *            the pdct_unitpce to set
	 */
	public void setPdct_unitpce(String pdct_unitpce) {
		this.pdct_unitpce = pdct_unitpce;
	}

	/**
	 * @return the pdct_ttl_selling
	 */
	public String getPdct_ttl_selling() {
		return pdct_ttl_selling;
	}

	/**
	 * @param pdct_ttl_selling
	 *            the pdct_ttl_selling to set
	 */
	public void setPdct_ttl_selling(String pdct_ttl_selling) {
		this.pdct_ttl_selling = pdct_ttl_selling;
	}

	/**
	 * @return the pdct_photo
	 */
	public String getPdct_photo() {
		return pdct_photo;
	}

	/**
	 * @param pdct_photo
	 *            the pdct_photo to set
	 */
	public void setPdct_photo(String pdct_photo) {
		this.pdct_photo = pdct_photo;
	}

	/**
	 * @return the pdct_category
	 */
	public String getPdct_category() {
		return pdct_category;
	}

	/**
	 * @param pdct_category
	 *            the pdct_category to set
	 */
	public void setPdct_category(String pdct_category) {
		this.pdct_category = pdct_category;
	}

	/**
	 * @return the pdct_country
	 */
	public String getPdct_country() {
		return pdct_country;
	}

	/**
	 * @param pdct_country
	 *            the pdct_country to set
	 */
	public void setPdct_country(String pdct_country) {
		this.pdct_country = pdct_country;
	}

	/**
	 * @return the pdct_county
	 */
	public String getPdct_county() {
		return pdct_county;
	}

	/**
	 * @param pdct_county
	 *            the pdct_county to set
	 */
	public void setPdct_county(String pdct_county) {
		this.pdct_county = pdct_county;
	}

	/**
	 * @return the pdct_town
	 */
	public String getPdct_town() {
		return pdct_town;
	}

	/**
	 * @param pdct_town
	 *            the pdct_town to set
	 */
	public void setPdct_town(String pdct_town) {
		this.pdct_town = pdct_town;
	}

	/**
	 * @return the pdct_estate
	 */
	public String getPdct_estate() {
		return pdct_estate;
	}

	/**
	 * @param pdct_estate
	 *            the pdct_estate to set
	 */
	public void setPdct_estate(String pdct_estate) {
		this.pdct_estate = pdct_estate;
	}

	/**
	 * @return the pdct_google_location
	 */
	public String getPdct_google_location() {
		return pdct_google_location;
	}

	/**
	 * @param pdct_google_location
	 *            the pdct_google_location to set
	 */
	public void setPdct_google_location(String pdct_google_location) {
		this.pdct_google_location = pdct_google_location;
	}

	/**
	 * @return the pdct_pub_date
	 */
	public String getPdct_pub_date() {
		return pdct_pub_date;
	}

	/**
	 * @param pdct_pub_date
	 *            the pdct_pub_date to set
	 */
	public void setPdct_pub_date(String pdct_pub_date) {
		this.pdct_pub_date = pdct_pub_date;
	}

	/**
	 * @return the pdct_ownr
	 */
	public String getPdct_ownr() {
		return pdct_ownr;
	}

	/**
	 * @param pdct_ownr
	 *            the pdct_ownr to set
	 */
	public void setPdct_ownr(String pdct_ownr) {
		this.pdct_ownr = pdct_ownr;
	}

	/**
	 * @return the pdct_expdate
	 */
	public String getPdct_expdate() {
		return pdct_expdate;
	}

	/**
	 * @param pdct_expdate
	 *            the pdct_expdate to set
	 */
	public void setPdct_expdate(String pdct_expdate) {
		this.pdct_expdate = pdct_expdate;
	}

	/**
	 * @return the update
	 */
	public int getUpdate() {
		return update;
	}

	/**
	 * @param update
	 *            the update to set
	 */
	public void setUpdate(int update) {
		this.update = update;
	}

	private String pdct_title;
	private String pdct_unitpce;
	private String pdct_ttl_selling;
	private String pdct_photo;
	private String pdct_category;
	private String pdct_country;
	private String pdct_county;
	private String pdct_town;
	private String pdct_estate;
	private String pdct_google_location;
	private String pdct_pub_date;
	private String pdct_expdate;
	private String pdct_ownr;
	private int update;
    private String pdct_desc;
    private int pdct_status;

    public String getPdct_desc() {
        return pdct_desc;
    }

    public void setPdct_desc(String pdct_desc) {
        this.pdct_desc = pdct_desc;
    }

    public int getPdct_status() {
        return pdct_status;
    }

    public void setPdct_status(int pdct_status) {
        this.pdct_status = pdct_status;
    }
}

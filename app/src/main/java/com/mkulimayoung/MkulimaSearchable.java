package com.mkulimayoung;

import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SearchEvent;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.mkulimayoung.mkulimaApp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import database.Database;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import models.Product;
import models.UserDevice;
import tools.Configs;

/**
 * Created by robertnyangate on 11/6/15.
 */
public class MkulimaSearchable extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    ListView mLVCountries;
    SimpleCursorAdapter mCursorAdapter;
    Realm realm;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        realm = Configs.getRealmInstance(this);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        mixpanel=Configs.getMixpanel(this);

        // Getting reference to Country List
        mLVCountries = (ListView)findViewById(R.id.lv_countries);

        // Setting item click listener
        mLVCountries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                startActivity(countryIntent);
//                //showToast("nice one 1");
                mcentral_ton.selectedItem = realm.where(Product.class).equalTo("pid",Integer.valueOf(((TextView) view.findViewById(R.id.idd)).getText()
                                .toString())).findFirst();
                startActivity(new Intent(MkulimaSearchable.this, ItemView.class));

            }
        });


        // Defining CursorAdapter for the ListView
        mCursorAdapter = new SimpleCursorAdapter(getBaseContext(),
                R.layout.searchcats,
                null,
                new String[]{"Name",
                        "Price",
                        "Location", "_id","Imagename"}, new int[]{R.id.category, R.id.product,

                R.id.price, R.id.idd,R.id.productImage}, 0);

        // Setting the cursor adapter for the country listview


        //mLVCountries.setAdapter(mCursorAdapter);

        // Getting the intent that invoked this activity
        Intent intent = getIntent();
        DatabaseTable db = new DatabaseTable(this);
        Cursor c = db.getWordMatches(intent.getStringExtra(SearchManager.QUERY), null);


        mLVCountries.setAdapter(new SearchResultAdapter(getBaseContext(),c));
        if(null==c)
            ((TextView)findViewById(R.id.title)).setText("Results not found");
         else ((TextView)findViewById(R.id.title)).setText(c.getCount()+" Results");
        // If this activity is invoked by selecting an item from Suggestion of Search dialog or
        // from listview of SearchActivity
        if(intent.getAction().equals(Intent.ACTION_VIEW)){
            Intent countryIntent = new Intent(this, ItemView.class);


            mcentral_ton.selectedItem = realm.where(Product.class).equalTo("pid",Integer
                    .valueOf(intent.getData().getLastPathSegment())).findFirst();
            startActivity(new Intent(MkulimaSearchable.this, ItemView.class));
            countryIntent.setData(intent.getData());
            countryIntent.putExtra("theindex", "search");
            startActivity(countryIntent);
            //showToast("nice one");
            Log.d("DATA", "<<<<<<<<<<<<<< " + intent.getData().getLastPathSegment());
            onKeyMetric(mcentral_ton.selectedItem.getPdct_title());
            finish();
        } else if (intent.getAction().equals(Intent.ACTION_SEARCH)) { // If this
            // activity
            // is
            // invoked,
            // when user presses "Go" in the Keyboard of Search Dialog


                String query = intent.getStringExtra(SearchManager.QUERY);
            onKeyMetric(query);
                //doSearch(query);
            {
                Log.d("ANSWERS", "today>>>>>>>>" + new Answers().getVersion());
                Answers.getInstance().logSearch(new SearchEvent()
                                .putQuery(query)
                                .putCustomAttribute("time", new Date().toString())
                );
                //process Cursor and display results
                mCursorAdapter = new SimpleCursorAdapter(getBaseContext(),
                        R.layout.searchcats,
                        c,
                        new String[]{"Name",
                                "Price",
                                "Location", "_id","Imagename"}, new int[]{R.id.category, R.id.product,

                        R.id.price, R.id.idd,R.id.productImage}, 0);

                // Setting the cursor adapter for the country listview
                mLVCountries.setAdapter(new SearchResultAdapter(getBaseContext(), c));
                if(null==c)
                    ((TextView)findViewById(R.id.title)).setText("Results not found");
                else ((TextView)findViewById(R.id.title)).setText(c.getCount()+" Results");
                mLVCountries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        mcentral_ton.selectedItem = realm.where(Product.class).equalTo("pid",Integer.valueOf(((TextView) view.findViewById(R.id.idd)).getText()
                                .toString())).findFirst();
                        startActivity(new Intent(MkulimaSearchable.this, ItemView.class));

                        startActivity(new Intent(MkulimaSearchable.this, ItemView.class));
                    }
                });


            }

        }
    }
    public void showToast(String text){
        Toast.makeText(this, text,
                Toast.LENGTH_LONG).show();
    }

    private void doSearch(String query){
        Bundle data = new Bundle();
        data.putString("query", query);

        // Invoking onCreateLoader() in non-ui thread
        getSupportLoaderManager().initLoader(1, data, this);
    }


    /** This method is invoked by initLoader() */
    @Override
    public Loader<Cursor> onCreateLoader(int arg0, Bundle data) {
        Uri uri = MkulimaContentProvider.CONTENT_URI;
        return new CursorLoader(getBaseContext(), uri, null, null, new String[]{data
                .getString("query")}, null);
    }

//    @Override
//    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//        return null;
//    }

    /** This method is executed in ui thread, after onCreateLoader() */
    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor c) {
        mCursorAdapter.swapCursor(c);
    }


    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        mixpanel.flush();
    }

    public void onKeyMetric(String query) {
        Log.d("ANSWERS", ">>>>>>>>" + new Answers().getVersion());
        Answers.getInstance().logSearch(new SearchEvent()
                        .putQuery(query)
                        .putCustomAttribute("time", new Date().toString())
        );
        trackSearchEvent(query);
    }
    MixpanelAPI mixpanel;
    private void trackSearchEvent(String qry){

        mixpanel.getPeople().set("searchingfor",qry);
        try {
            JSONObject props = new JSONObject();
            props.put("itemName", qry);
            props.put("deviceid", realm.allObjects(UserDevice.class).first().getDeviceId());
            props.put("time", new Date().toString());
            mixpanel.track("SearchEvents", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
    }

}

/**
 * 
 */
package com.mkulimayoung;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;

import com.google.gson.Gson;
import com.mkulimayoung.mkulimaApp.R;

import java.io.File;
import java.util.List;

import database.Database;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import models.Farmers;
import models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import server.ServiceGenerator;
import server.SokoContext;
import server.SokoUsers;
import tools.Configs;
import tools.PhoneNumber;

/**
 * @author robb
 * 
 */
public class ProfileView extends AppCompatActivity {
    private static final int PROFILE_REQUESTCODE = 300;
    private static final int REQCODE = 1;
    EditText country;
    Bundle savedInst;
    private FragmentManager fmanager;
    private EditText fullname, phone, email, username,password;
    private EditText county;
    private Button update;
	private android.support.v4.app.FragmentTransaction ft;
    private Realm realm;
    private User thisUser;
    /**
     *
     */
    private Bitmap mybitmap = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        realm = Configs.getRealmInstance(this);
        savedInst=savedInstanceState;
        findViewById(R.id.btn_updateProfile).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(fullname.getText())){
                    fullname.setError("Invalid");

                }else if(TextUtils.isEmpty(phone.getText())&& false==PhoneNumber.validCellPhone
                        (phone
                        .getText().toString())){
                    phone.setError("Invalid");

                }else if(TextUtils.isEmpty(username.getText())){
                    username.setError("Invalid");

                }else if(TextUtils.isEmpty(password.getText())&&6>password.getText().toString()
                        .length()){
                    password.setError("Invalid");

                }

                else
                {
                    updateMe(fullname.getText().toString(),email.getText().toString(),phone
                            .getText().toString(),password.getText().toString(),username.getText
                            ().toString());
                }
            }
        });
        if (realm.where(User.class).findAll().size()<1) {
            startActivityForResult(new Intent(this, UserAuthenticationActvity.class),PROFILE_REQUESTCODE);


        }else{
            updateFields();
        }
	}

    private void updateFields() {
        {
            if (realm.where(User.class).findAll().size()<1) {
                finish();
            }
            User user=realm.where(User.class).findFirst();

            fullname = (EditText) findViewById(R.id.fullnames);
            phone = (EditText) findViewById(R.id.phonenumber);
            email = (EditText) findViewById(R.id.email);
            username = (EditText) findViewById(R.id.username);
            password = (EditText) findViewById(R.id.password);


            email.setText(user.getEmail());
            phone.setText(user.getMobile());
            fullname.setText(user.getName());
            username.setText(user.getUsername());

            TextInputLayout phoneTextInputLayout = (TextInputLayout)
                    findViewById(R.id.phone_text_input_layout);
            phoneTextInputLayout.setErrorEnabled(true);
//            findViewById(R.id.profile_image).setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    selectPhoto();
//                }
//            });

            TextInputLayout usernameTextInputLayout = (TextInputLayout)
                    findViewById(R.id.username_text_input_layout);
            usernameTextInputLayout.setErrorEnabled(true);

            TextInputLayout emailTextInputLayout = (TextInputLayout)
                    findViewById(R.id.username_text_input_layout);
            emailTextInputLayout.setErrorEnabled(true);


        }
    }

	protected void updateMe(final String fullname, final String email,
			final String cellphone, final String  password,final String username) {
        realm.beginTransaction();
        thisUser=realm.allObjects(User.class).first();
        thisUser.setPasscode(Functions.md5(password));
        thisUser.setUsername(username);
        thisUser.setEmail(email);
        thisUser.setMobile(cellphone);
        thisUser.setName(fullname);
        realm.copyToRealmOrUpdate(thisUser);

        Farmers farmer=SokoContext.getFarmer(thisUser);
        realm.copyToRealmOrUpdate(farmer);
        realm.commitTransaction();

        User user=new User();
        user.setEmail(email);
        user.setMobile(cellphone);
        user.setPasscode(Functions.md5(password));
        user.setName(fullname);
        user.setUsername(username);

         SokoContext.updateUser(user);
        Functions.showToast("Updated",this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void setPhoto() {

        File imgFile = new File(mcentral_ton.photoURl);

        if (imgFile.exists()) {

            mybitmap = BitmapFactory.decodeFile(imgFile
                    .getAbsolutePath());
            CircleImageView myImage = (CircleImageView) findViewById(R.id.profile_image);
            myImage.setVisibility(View.VISIBLE);
            myImage.setImageBitmap(mybitmap);

        }

    }

    // method to start activity for selecting photo to upload.
    public void selectPhoto() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*"); //$NON-NLS-1$
        startActivityForResult(photoPickerIntent, REQCODE);
    }

    private String getPath(Uri uri) {
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private String getName(Uri uri) {
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQCODE){
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImage = data.getData();
                try {
                    String filePath = getPath(selectedImage);
                    String file_extn = filePath.substring(filePath
                            .lastIndexOf(".") + 1); //$NON-NLS-1$

                    if (file_extn.equals("img") || file_extn.equals("jpg") //$NON-NLS-1$ //$NON-NLS-2$
                            || file_extn.equals("png") //$NON-NLS-1$
                            || file_extn.equals("jpeg") //$NON-NLS-1$
                            || file_extn.equals("gif")) { //$NON-NLS-1$
                        if (Functions.isValidSize(filePath)) {
                            mcentral_ton.photoURl = filePath;
                            mcentral_ton.photoname = filePath
                                    .substring(filePath.lastIndexOf("/") + 1);

                            setPhoto();

                        } else {
                            Functions.showToast("Invalid photo size", this);
                        }

                    } else {

                    }

                } catch (Exception ep) {
                    Log.d("s", ">>>>>>>Error " + ep.getLocalizedMessage());
                }
            }
        }else if(requestCode==PROFILE_REQUESTCODE){
            if (resultCode == UserAuthenticationActvity.LOGIN_CODE) {
                updateFields();
            }else{
                finish();
            }
        }
    }

}

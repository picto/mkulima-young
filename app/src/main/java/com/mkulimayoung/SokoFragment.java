/**
 *
 */
package com.mkulimayoung;

import android.accounts.Account;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncStatusObserver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardGridArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.internal.base.BaseCard;
import it.gmariotti.cardslib.library.view.CardGridView;
import models.Product;
import server.GenericAccountService;
import server.SokoContext;
import server.SyncService;
import server.SyncUtils;
import tools.Configs;

/**
 * @author robb
 */
public class SokoFragment extends Fragment implements AdapterView.OnItemClickListener {
//    public static String IMAGE_PATH = MyApp.SERVER + "/soko_/";
//    public static String IMAGE_PATH = "http://mkulimayoung.com/mkulimayoungweb/public/_soko/";
    public static String IMAGE_PATH = "http://mkulimayoung.com/public/_soko/";
    GridView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    GridAdapter customGridAdapter;
    Realm realm;
    TextView flipListTv;
    private FragmentManager fmanager;
    private android.support.v4.app.FragmentTransaction ft;
    private TextView login, showing;

    public static SokoFragment newInstance(FragmentManager fm) {
        SokoFragment sp = new SokoFragment();
        sp.fmanager = fm;
        return sp;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        realm = Configs.getRealmInstance(getActivity());

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        super.onPrepareOptionsMenu(menu);
        //menu.findItem(R.id.search).setVisible(true);
    }


    String country = "Kenya";
    GridView cardgridview;

    //    ArrayList<GlobalItem> products;
    private void loadDashboard(int i) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getContext());

        country = SP.getString("countryorigin", "Kenya");

//        Database database=new Database(getContext());
//        database.open();
//        final RealmResults<Product> persons = realm.where(Product.class).contains
//                ("pdct_google_location", country).notEqualTo("pdct_status","0").findAll().sort
//                ("pid",
//                Sort.DESCENDING);
        final RealmResults<Product> persons = realm.where(Product.class).contains
                ("pdct_google_location", country).findAll().sort
                ("pid",
                        Sort.DESCENDING);

        final ProductAdapter productAdapter=new ProductAdapter(persons,getContext());
        cardgridview.setAdapter(productAdapter);
        persons.addChangeListener(new RealmChangeListener() {
            @Override
            public void onChange() {
                productAdapter.notifyDataSetChanged();
            }
        });
        cardgridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                viewProduct(persons.get(position));
            }
        });



    }

    private void viewProduct(Product product) {
        mcentral_ton.selectedItem = product;
        startActivity(new Intent(getActivity(), ItemView.class));
//
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.mygrid, container,
                false);
        cardgridview = (GridView) rootView.findViewById(R.id.grdview);
//        cardgridview=(GridView)rootView.findViewById(R.id.grdview);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadDashboard(0);


    }

    private synchronized void updateView(RealmResults<Product> items) {
        customGridAdapter = new GridAdapter(
                getActivity(), items);
//        mRecyclerView.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        // mRecyclerView.setLayoutManager(mLayoutManager);

        //  mAdapter = new GridAdapter(getContext(),lstImageItem,fmanager);
        mRecyclerView.setAdapter(customGridAdapter);
        customGridAdapter.notifyDataSetChanged();


    }

    //    protected void displayItemsHaving(String name) {
//
//        updateView(realm.where(GlobalItem.class)
//                .contains("itemname", name)
//                .findAllSorted("item_id", false));
//
//    }

    /**
     * @param category
     */
//    protected void displayFilteredItems(CategoryBean category) {
//        if(category.getCategory().equalsIgnoreCase("All")){
//
//            showing.setHint(category.getCategory());
//
//            updateView(realm.where(GlobalItem.class).findAll
//                    ());
//        }else{
//        showing.setHint(category.getCategory());
//
//		updateView(realm.where(GlobalItem.class).equalTo
//                ("category", category.getCategory()).findAll
//                ());
//        }
//
//    }

    /**
     * @param result
     */
    protected void showToast(String result) {
        Functions.showToast(result, getActivity());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        mcentral_ton.selectedItem = (Product) customGridAdapter.getItem(position);
//        showDialog();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close(); // Remember to close Realm when done.
    }

    private void showDialog() {

        startActivity(new Intent(IntentConstants.ACTION_ITEMVIEW));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
//		DBDataSource ds = new DBDataSource(getActivity());
//		ds.open();
//		menu.findItem(R.id.login).setVisible(ds.getUser() == null);
//		menu.findItem(R.id.logout).setVisible(ds.getUser() != null);
    }
    private Object mSyncObserverHandle;
    @Override
    public void onResume() {
        super.onResume();
        mSyncStatusObserver.onStatusChanged(0);
        final int mask = ContentResolver.SYNC_OBSERVER_TYPE_PENDING |
                ContentResolver.SYNC_OBSERVER_TYPE_ACTIVE;
        mSyncObserverHandle = ContentResolver.addStatusChangeListener(mask, mSyncStatusObserver);
        loadDashboard(0);
    }
    /**
     * Create SyncAccount at launch, if needed.
     *
     * <p>This will create a new account with the system for our application, register our
     * {@link SyncService} with it, and establish a sync schedule.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Create account, if needed
        SyncUtils.CreateSyncAccount(activity);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mSyncObserverHandle != null) {
            ContentResolver.removeStatusChangeListener(mSyncObserverHandle);
            mSyncObserverHandle = null;
        }
    }

    /**
     * Crfate a new anonymous SyncStatusObserver. It's attached to the app's ContentResolver in
     * onResume(), and removed in onPause(). If status changes, it sets the state of the Refresh
     * button. If a sync is active or pending, the Refresh button is replaced by an indeterminate
     * ProgressBar; otherwise, the button itself is displayed.
     */
    private SyncStatusObserver mSyncStatusObserver = new SyncStatusObserver() {
        /** Callback invoked with the sync adapter status changes. */
        @Override
        public void onStatusChanged(int which) {
            getActivity().runOnUiThread(new Runnable() {
                /**
                 * The SyncAdapter runs on a background thread. To update the UI, onStatusChanged()
                 * runs on the UI thread.
                 */
                @Override
                public void run() {
                    // Create a handle to the account that was created by
                    // SyncService.CreateSyncAccount(). This will be used to query the system to
                    // see how the sync status has changed.
                    Account account = GenericAccountService.GetAccount();
                    if (account == null) {
                        // GetAccount() returned an invalid value. This shouldn't happen, but
                        // we'll set the status to "not refreshing".
//                        setRefreshActionButtonState(false);
                        Log.d("SYNC",">>>Sync account is null; not refreshing");
                        return;
                    }
                    Log.d("SYNC",">>>Sync account is not null;  refreshing");

                    // Test the ContentResolver to see if the sync adapter is active or pending.
                    // Set the state of the refresh button accordingly.
                    boolean syncActive = ContentResolver.isSyncActive(
                            account, MkulimaContentProvider.AUTHORITY);
                    boolean syncPending = ContentResolver.isSyncPending(
                            account, MkulimaContentProvider.AUTHORITY);
//                    setRefreshActionButtonState(syncActive || syncPending);
                }
            });
        }
    };



}

package com.mkulimayoung;

/**
 * Created by robertnyangate on 9/24/15.
 */
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import java.util.ArrayList;

import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;

public class MyAdapter extends BaseAdapter {
    private ArrayList<BuyerItems>items;
    private Activity activity;
    LayoutInflater inflater;

    private static class ViewHolder {
        TextView name,location,desc;
    }

    public MyAdapter(ArrayList<BuyerItems>items,Activity activity){
        this.items=items;
        this.activity=activity;
        inflater=activity.getLayoutInflater();
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public BuyerItems getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.product_catadapter, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.location = (TextView) convertView.findViewById(R.id.country);
            viewHolder.desc = (TextView) convertView.findViewById(R.id.description);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        BuyerItems item = getItem(position);
        viewHolder.desc.setText(item.getDescription());
        viewHolder.name.setText(item.getName());
        viewHolder.location.setText(item.getCountry()+" - "+item.getTown());
        return convertView;
    }

}
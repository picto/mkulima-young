package com.mkulimayoung;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.*;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import server.SokoContext;
import tools.Configs;



/**
 * Created by robertnyangate on 9/20/15.
 */
public class MyApp extends Application {
    private static final String TWITTER_KEY = "LJeoFlIr7x9JgwF1RT0DWdGuz";
    private static final String TWITTER_SECRET =
            "5u5wwQfc6Hq8aMlD6S5AKmrZYebY0k3cHHRLm2CuTdeUILOK9e";
    private static MyApp mInstance;
    private Realm realm;
    private RealmConfiguration realmConfiguration;
    //private AuthCallback authCallback;

    public static final String TAG = MyApp.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private com.android.volley.toolbox.ImageLoader mImageLoader;
    public static synchronized MyApp getInstance() {
        return mInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        // Or you can add the migration code to the configuration. This will run the migration code without throwing
        // a RealmMigrationNeededException.


        realm = Configs.getRealmInstance(this);

//        authCallback = new AuthCallback() {
//            @Override
//            public void success(DigitsSession session, String phoneNumber) {
//                // Do something with the session
//            }
//
//            @Override
//            public void failure(DigitsException exception) {
//                // Do something on failure
//            }
//        };

        // register with parse


    }

    public RealmConfiguration getRealmConfiguration() {
        return realmConfiguration;
    }

//    public AuthCallback getAuthCallback(){
//        return authCallback;
//    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }



    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public  static String SERVER="http://93.104.210.198";
    public final static String ServerIPsync = "http://93.104.210.198:31415/sync/mkulimayoung_office-0000";
    public static void startSync(Context context){
        SokoContext.setProducts((context));
        SokoContext.setUsers((context));

    }
}


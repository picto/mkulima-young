/**
 * 
 */
package com.mkulimayoung;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

/**
 * @author robb
 * 
 */
public class EventDateAdapter extends ArrayAdapter<String> {
	private final LayoutInflater inflator;
	private final Context context;
	EventHolder holder = null;
	private final List<String> events;

	/**
	 * @param context
	 * @param resource
	 * @param objects
	 */
	public EventDateAdapter(Context context, int resource, List<String> objects) {
		super(context, resource, objects);
		inflator = ((Activity) context).getLayoutInflater();
		this.context = context;
		events = objects;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = inflator.inflate(R.layout.events_layout, parent,
					false);
			holder = new EventHolder();
			holder.brief = (TextView) convertView.findViewById(R.id.brief);
			holder.eventTitle = (TextView) convertView.findViewById(R.id.title);
			holder.startDate = (TextView) convertView
					.findViewById(R.id.start_date);
			holder.via = (TextView) convertView.findViewById(R.id.via);
			holder.endDate = (TextView) convertView.findViewById(R.id.end_date);
			holder.startTime = (TextView) convertView
					.findViewById(R.id.start_time);
			holder.endTime = (TextView) convertView.findViewById(R.id.end_time);
			holder.venue = (TextView) convertView.findViewById(R.id.venue);
			holder.county = (TextView) convertView.findViewById(R.id.county);
			holder.town = (TextView) convertView.findViewById(R.id.town);
			holder.brief = (TextView) convertView.findViewById(R.id.brief);
			holder.ly = (LinearLayout) convertView.findViewById(R.id.end);
			convertView.setTag(holder);

		} else {
			holder = (EventHolder) convertView.getTag();

		}

		holder.eventTitle.setHint(Functions.getHumanDate(events.get(position)));
		holder.eventTitle.setCompoundDrawablesWithIntrinsicBounds(context
				.getResources().getDrawable(R.drawable.calendar), null, null,
				null);
		holder.startDate.setVisibility(View.GONE);
		StringBuffer eventsbuff = new StringBuffer();
		for (EventsBean beans : mcentral_ton.common_events.get(events
				.get(position)))
			eventsbuff.append(" " + beans.getTitle() + "\n\n");
		holder.town.setHint(eventsbuff.toString());
		holder.town.setHintTextColor(context.getResources().getColor(
				R.color.teal));
		return convertView;
	}

	static class EventHolder {
		TextView eventTitle, brief, venue, county, town;
		TextView startDate, endDate, via;
		TextView startTime, endTime;
		LinearLayout ly;

	}

	public void showContent(int pos, AdapterView<?> parent, View v) {
		getView(pos, v, parent);

		if (holder.ly.getVisibility() == View.VISIBLE)
			holder.ly.setVisibility(View.GONE);
		else {
			holder.ly.setVisibility(View.VISIBLE);
			holder.ly.requestFocus();
		}
	}
}

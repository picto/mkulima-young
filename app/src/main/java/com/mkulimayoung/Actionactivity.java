package com.mkulimayoung;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.widget.ShareDialog;
import com.firebase.client.Firebase;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.mkulimayoung.mkulimaApp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import database.Database;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
import io.realm.Realm;
import models.Product;
import models.User;
import models.UserDevice;
import server.SokoContext;
import server.SyncUtils;
import tools.Configs;

public class Actionactivity extends AppCompatActivity implements NavigationView
        .OnNavigationItemSelectedListener, SearchView.OnQueryTextListener,
        SearchView.OnSuggestionListener{
    private static final String GCM_KEY = "289058778014";

    static final String dtitle = "title";
    static final String Extitle = "expln";
	private static final int REQCODE = 1;
	public static int NEWPRODUCT = 1;
	public static int MYPRODUCTS = 2;
    static int stage = 0;
    static FragmentManager fm;
    private final int REQ_SIGNUP = 1;
    CallbackManager callbackManager;
    MkulimaFragmentsAdapter mpagerAdapter;
	ShareDialog shareDialog;
	Toolbar toolbar;
    ViewPager viewPager;
    ProgressBar mProgress;
    TextView loadingtv;
    SearchView searchView;
    MenuItem login, logout;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    Realm realm;

    public static String getGcmKey() {
        return GCM_KEY;
    }

    public void onKeyMetric() {
        Answers.getInstance().logCustom(new CustomEvent("Launch")
                .putCustomAttribute("LaunchTime", new Date().toString()));
        try {
            JSONObject props = new JSONObject();
            props.put("Launch", "Soko");
            props.put("time", new Date().toString());
            mixpanel.track("MkulimaApp opened", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
    }
private MixpanelAPI mixpanel;
    @SuppressLint("Recycle")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mdrawer);
        FacebookSdk.sdkInitialize(this);
        Fabric.with(this, new Crashlytics());
        Firebase.setAndroidContext(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mProgress = (ProgressBar) findViewById(R.id.progressBar);
        loadingtv=(TextView)findViewById(R.id.loadingtv);
        mProgress.setIndeterminate(true);
        mixpanel=Configs.getMixpanel(this);
        realm=Configs.getRealmInstance(this);


		// Hide the action bar title
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayUseLogoEnabled(true);
		// getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_launcher);



		fm = getSupportFragmentManager();
		callbackManager = CallbackManager.Factory.create();
		shareDialog = new ShareDialog(this);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		//mDrawerList = (ListView) findViewById(R.id.left_drawer);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerShadow(R.color.DeepSkyBlue, GravityCompat.START);


		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
				R.string.open, R.string.close);
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerToggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
         viewPager = (ViewPager) findViewById(R.id.viewpager);

            onKeyMetric();
            realm = Configs.getRealmInstance(this);

        if(realm.allObjects(UserDevice.class).size()<1){
            startActivity(new Intent(this,IntroScreenActivity.class));

        }

            int size=realm.allObjects(Product.class).size();
            if (size< 1) {
                MyApp.startSync(this);

                showPager();
                showProgress();

                new AsyncTask<String, String, String>() {
                    @Override
                    protected String doInBackground(String... strings) {
                       int count=0;
                        while (count<5){
                            //chill out
                            Realm realm=Configs.getRealmInstance(Actionactivity.this);
                            count=realm.allObjects(Product.class).size();
                            realm.close();
                            SokoContext.echo("counting.."+count);
                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        dismissProgress();
                        try{
                        showPager();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }.execute("");






            } else {
                dismissProgress();
                showPager();
            }



	}


    private void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
        loadingtv.setVisibility(View.VISIBLE);
    }
    private void dismissProgress(){
        mProgress.setVisibility(View.GONE);
        loadingtv.setVisibility(View.GONE);
    }

    private void showPager() {
        int count=realm.allObjects(UserDevice.class).size();
        int count2=realm.allObjects(User.class).size();
        String myDeviceId=count>0?realm.allObjects(UserDevice.class).first().getDeviceId(): UUID
                .randomUUID().toString();

        if(count2<1){//user is not logged in
        mixpanel.getPeople().identify(myDeviceId);
        }else {
            //user is logged in. set alias
            mixpanel.alias(realm.allObjects(User.class).first().getEmail(),myDeviceId);
        }
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        mixpanel.getPeople().initPushHandling(GCM_KEY);

        new DatabaseTable(Actionactivity.this);
        mpagerAdapter= new MkulimaFragmentsAdapter(getSupportFragmentManager(),
                Actionactivity.this);

        viewPager.setAdapter(mpagerAdapter);
        Set<String> selections = sharedPrefs.getStringSet("channels", Collections.<String>emptySet());
        for(String channel:getResources().getStringArray(R.array.categorieschannels)){

            mixpanel.getPeople().set(channel,selections.contains(channel));
        }
        // Give the TabLayout the ViewPager
        SlidingTabLayout tabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setDistributeEvenly(true);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.Orange);
            }
        });

        tabLayout.setViewPager(viewPager);


        //tabLayout.setTabMode(TabLayout.MODE_FIXED);
        //tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        if(getIntent().hasExtra("navigateto")){
            mpagerAdapter.setSelectedFragment(getIntent().getStringExtra("navigateto"));
        }
        //
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.profile) {
            selectItem("My Preferences");
            // Handle the camera action
        }  else if (id == R.id.messages) {
            postProduct();
        }  else if (id == R.id.nav_share) {
            Functions.sendIntent(Actionactivity.this, "MkulimaApp", "Check out this " +
                    "application connecting young farmers https://goo" +
                    ".gl/ojunxn "
                    + "");

        } else if (id == R.id.nav_send) {
            selectItem("Feedback");
        }else if (id == R.id.account) {
            startActivity(new Intent(this,ProfileView.class));
        }else if (id == R.id.seller) {
            startActivity(new Intent(this,Listings.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public boolean onSuggestionSelect(int position) {
        return false;
    }

	/**
	 * @param myoption
	 */
	private void selectItem(String myoption) {

        mDrawerLayout.closeDrawer(GravityCompat.START);
		// update the main content by replacing fragments
		//getSupportActionBar().setTitle(myoption);
		if (myoption.equalsIgnoreCase("My Preferences")) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            startActivity(new Intent(this, SettingsActivity.class));


		}

		else if (myoption.equalsIgnoreCase("My Produce")) {

                mDrawerLayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, Listings.class));


		} else if (myoption.equalsIgnoreCase("Post new product")) {

				mDrawerLayout.closeDrawer(GravityCompat.START);
				startActivity(new Intent(this, Advertise.class));
		}  else if (myoption.equalsIgnoreCase("Feedback")) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            startActivity(new Intent(this, Feedback.class));
		} else {

			//do nothing or
            mDrawerLayout.closeDrawer(GravityCompat.START);
            showPager();

		}

	}

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);


        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
        MenuItem search = menu.findItem(R.id.action_search);
        ComponentName cn = new ComponentName(this, MkulimaSearchable.class);
//
        searchView = (SearchView) MenuItemCompat.getActionView(search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));
        searchView.setOnSuggestionListener(this);


		return super.onCreateOptionsMenu(menu);

	}

    @Override
    public boolean onSuggestionClick(int position) {
        int suggestion = getSuggestion(position);
        searchView.setQuery("", true); // submit query now
        Functions.showSnackBar(searchView, "From ActionAct " + suggestion);
//        mcentral_ton.selectedItem = realm.where(GlobalItem.class).equalTo("item_id",
//                suggestion)
//                .findFirst();
        Functions.showSnackBar(searchView, "From ActionAct " + suggestion);
        startActivity(new Intent(Actionactivity.this, ItemView.class));

        return true; // replace default search manager behaviour
    }

    private int getSuggestion(int position) {
        Cursor cursor = (Cursor) searchView.getSuggestionsAdapter().getItem(
                position);
        int suggest1 = cursor.getInt(cursor
                .getColumnIndex(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID));
        return suggest1;
    }

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		// If the nav drawer is open, hide action items related to the content
		// view
		//boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		//menu.findItem(R.id.refresh).setVisible(!drawerOpen);
        login=menu.findItem(R.id.login);
        logout=menu.findItem(R.id.logout);
        setUserStatus();


		return super.onPrepareOptionsMenu(menu);
	}

	/*
     * (non-Javadoc)
	 *
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.

		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action buttons
		switch (item.getItemId()) {

		case R.id.login:
            startActivity(new Intent(this, UserAuthenticationActvity.class));
			return true;
		case R.id.logout:
			logoutUser();
			return true;
            case R.id.refresh:
                MyApp.startSync(this);
                showPager();
                 return true;
		case R.id.post:
			postProduct();
            return true;
            case R.id.action_search:
                showSearchView(item);

			return true;
//            case R.id.ask:
//                    if(realm.where(GlobalUser.class).findAll().size()>0)
//                startActivity(new Intent(this, QuizMaker.class));
//                else startActivity(new Intent(this, UserAuthenticationActvity.class));
//                return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

    /**
     *
     */
    private void showSearchView(MenuItem searchItem) {
        onSearchRequested();
    }

    private void logoutUser() {
          realm.beginTransaction();
        realm.clear(User.class);
        realm.commitTransaction();
            setUserStatus();


    }

    /**
     * @return
     */

    void setUserStatus() {
        realm=Configs.getRealmInstance(this);
        int valid = realm.where(User.class).findAll().size();
        login.setVisible(valid < 1);
        logout.setVisible(valid > 0);
        invalidateOptionsMenu();

    }

    protected void postProduct() {
        startActivity(new Intent(this, Advertise.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }





    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // startActivity(new Intent(Actionactivity.this, Actionactivity.class));
            //selectItem(position);
        }

    }

    private class MkulimaFragmentsAdapter extends FragmentPagerAdapter implements ActionBar.TabListener{
        final int PAGE_COUNT = 3;
        private String tabTitles[] = new String[] { "Soko","Questions"};
        private Context context;
        private int[] imageResId = {
                R.drawable.basket,
                R.drawable.askme,
                android.R.drawable.ic_media_next,
                android.R.drawable.ic_media_next
        };


        public MkulimaFragmentsAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public Fragment getItem(int i) {

            if(tabTitles[i].equalsIgnoreCase("Soko")){
                return new SokoFragment();
            }else if(tabTitles[i].equalsIgnoreCase("Resources")){
                return InsideFarm.newInstance(Actionactivity.this.getSupportFragmentManager());
            }else
                return InsideFarm.newInstance(Actionactivity.this.getSupportFragmentManager());
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            //return tabTitles[position];
            Drawable image = context.getResources().getDrawable(imageResId[position]);
            image.setBounds(0, 0, 40, 40);
            // Replace blank spaces with image icon
            SpannableString sb = new SpannableString("   " + tabTitles[position]);
//            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
//            sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return sb;
        }
        public void setSelectedFragment(String switchto){
            int i=0;
           manloop: for(String tabname:tabTitles){
            if(tabname.equalsIgnoreCase(switchto)){
            viewPager.setCurrentItem(i);
                break manloop;
            }

            i++;
            }

        }


        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
            viewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        }

        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        mixpanel.flush();
    }
}
// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.mkulimayoung;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import it.gmariotti.cardslib.library.internal.Card;
import models.Product;


public class ProductCard extends Card
{

    private String imageUrl;
    protected ImageView imageView;
    protected TextView mSecondaryTitle;
    protected TextView mTitle,parentName;

    private String price,parentnme;
    private String stock;

    public int getBatchId() {
        return batchId;
    }

    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }

    private int batchId;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    private Product product;


    public PopupMenu getPopupMenu() {
        return popupMenu;
    }

    public void setPopupMenu(PopupMenu popupMenu) {
        this.popupMenu = popupMenu;
    }

    private PopupMenu popupMenu;

    public ProductCard(Context context)
    {
        this(context, R.layout.inside_card);
    }

    public ProductCard(Context context, int i)
    {
        super(context, i);
        init();
    }

    public ProductCard(Context context, String price, String location, String s1,String parentName)
    {
        this(context, R.layout.inside_card);
        this.price = price;
        stock = location;
        imageUrl = s1;
        parentnme=parentName;
    }

    private void init()
    {
        setOnClickListener(new OnCardClickListener() {


            public void onClick(Card card, View view) {
            }


        });
    }

    public void setupInnerViewElements(ViewGroup viewgroup, View view)
    {
        mTitle = (TextView)viewgroup.findViewById(R.id.price);
        mSecondaryTitle = (TextView)viewgroup.findViewById(R.id.stocklevel);
        imageView = (ImageView)viewgroup.findViewById(R.id.colorBorder);
        parentName=(TextView)viewgroup.findViewById(R.id.title);
        if (price != null)
        {
            mTitle.setText(this.price);
        }
        if (stock != null)
        {
            mSecondaryTitle.setText((new StringBuilder()).append(stock));
        }
        if (imageUrl != null)
        {
            ImageLoader imageLoader=new ImageLoader(getContext());
            imageLoader.DisplayImage(SokoFragment.IMAGE_PATH+imageUrl,imageView);
//            imageLoader.DisplayImage("http://www.mkulimayoung.co.ke/soko_/"+imageUrl,imageView);
//            LoadImage.setImage(imageView, );
        }
        if(parentnme!=null)
            parentName.setText(parentnme);
    }


}

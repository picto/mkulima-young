package com.mkulimayoung;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.mkulimayoung.mkulimaApp.R;

import java.util.HashMap;


/**
 * Created by robertnyangate on 27/08/16.
 */
public class ReviewsAdapter extends BaseAdapter {
    private static final String TAG ="BambaAdapter" ;
    private HashMap<String,Object> products;
    private Context context;
    private LayoutInflater layoutInflater;
    public ReviewsAdapter(HashMap<String,Object> products, Context context){
        try {
            this.context=context;
            this.products=products;
            layoutInflater=LayoutInflater.from(context);
        }catch (Exception e){
            throw new ClassCastException("Activity must implement VariantClickCallBack.");
        }

    }
    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int i) {
        int ii=0;
        for(String key:products.keySet())
        {
            if(ii==i)
                return products.get(key);
            else
                ii++;
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        final ViewHolderItem viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.cart_item, parent, false);
            viewHolder = new ViewHolderItem();
            viewHolder.productimage = (ImageView) convertView.findViewById(R.id.productimage);
            viewHolder.productname = (TextView) convertView.findViewById(R.id.title);
            viewHolder.qty = (TextView) convertView.findViewById(R.id.quantity);
            viewHolder.prsn = (TextView) convertView.findViewById(R.id.amount);
            viewHolder.variantSize = (TextView) convertView.findViewById(R.id.variants);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        HashMap<String,Object> events = (HashMap<String, Object>) getItem(i);
        if(events==null)
            return convertView;

        viewHolder.productname.setText("rating: "+events.get("rating"));
        viewHolder.variantSize.setHint(""+events.get("comment"));
        viewHolder.qty.setHint(""+events.get("date"));
        viewHolder.prsn.setHint(""+events.get("user"));







        return convertView;
    }

    static class ViewHolderItem {
        Toolbar toolbar;
        TextView productname,qty,prsn;
        TextView variantSize;
//        CardView cardView;
        ImageView productimage;
    }
}

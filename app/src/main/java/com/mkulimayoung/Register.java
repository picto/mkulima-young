package com.mkulimayoung;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SignUpEvent;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.mkulimayoung.mkulimaApp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.Set;

import database.Database;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import models.Farmers;
import models.User;
import models.UserDevice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import server.ServiceGenerator;
import server.SokoContext;
import server.SokoUsers;
import tools.Configs;
import tools.EmailS;
import tools.PhoneNumber;

public class Register extends AppCompatActivity {

	

	private FragmentManager fmanager;
	private android.support.v4.app.FragmentTransaction ft;
    private Realm realm;
    TextView message;
    private EditText email,phone,fullname,password;
    private AppCompatButton register;
    Toolbar toolbar;
    MixpanelAPI mixpanel;


    @Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        realm = Configs.getRealmInstance(this);
        mixpanel=Configs.getMixpanel(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Signup");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        email= (EditText) findViewById(R.id.input_email);
        message= (TextView) findViewById(R.id.message);
        phone= (EditText) findViewById(R.id.input_phone);
        password= (EditText) findViewById(R.id.input_password);
        fullname= (EditText) findViewById(R.id.input_fullname);

        register=(AppCompatButton)findViewById(R.id.btn_register);

        register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    showProgress();
                    User user=new User();
                    user.setEmail(email.getText().toString());
                    user.setMobile(phone.getText()
                            .toString());

                    user.setName(fullname.getText().toString());
                    user.setUsername(user.getName().split(" ")[0]);
                    user.setPasscode(Functions.md5(password.getText().toString()));
                    user.setRegisterDate(Database.properFormat.format(new Date()));

                    if(realm.where(Farmers.class).equalTo("email",user.getEmail()).findAll().size
                            ()<1){
                        if(realm.where(Farmers.class).equalTo("mobile",user.getMobile()).findAll()
                                .size
                                ()<1){
                        registerUser(user);
                        }else{
                            dismissMyDialog();
                            message.setText("Phone already in use");
                        }
                    }else {
                        dismissMyDialog();
                        message.setText("Email already in use");
                    }



                }
            }
        });




	}
    Dialog pDialog;
    private void showProgress() {
        pDialog = new MaterialDialog.Builder(this)
                .title("Creating account")
                .content("Creating your account..")
                .progress(true,100).build();
        pDialog.setCancelable(false);
        pDialog.show();


    }

    private void registerUser(final User user) {
        SokoUsers.NewMember client = ServiceGenerator.createService(SokoUsers.NewMember.class);
        final Call<List<User>> productjson = client.profile(user);
        SokoContext.echo("sending signup data "+new Gson().toJson(user));

        productjson.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                try {
                    SokoContext.echo("received data "+response.body());
                    dismissMyDialog();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(response.body());

                    realm.commitTransaction();
                    message.setText("Account registered");

                    showToast("Account created");

                        trackSignupEvent(email.getText().toString(), phone.getText().toString());
                        Functions.showToast("Account created",Register.this);
                        showProfile();


                } catch (Exception e) {
                    e.printStackTrace();
                    showToast("Error processing account");
                    dismissMyDialog();
                    message.setText("Error during account verification. Please try to login");

                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                dismissMyDialog();
                if(TestInternet.isOnline(Register.this)){
                    t.printStackTrace();
                    message.setText("Could not create your account. Request assistance");
                }else
                {  t.printStackTrace();
                    message.setText("No internet connection");}
            }
        });


    }

    private void dismissMyDialog() {
        if(pDialog!=null&&pDialog.isShowing())
            pDialog.dismiss();
    }

    private void showToast(String message)
    {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    private void showProfile() {

            mixpanel.getPeople().identify(realm.allObjects(User.class).first().getEmail());

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        final Set<String> selections = sharedPrefs.getStringSet("channels", null);

        mixpanel.getPeople().initPushHandling(Actionactivity.getGcmKey());
       final String email=realm.allObjects(User.class).first().getEmail();
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                Functions.sendEmail("Mkulima Young","Welcome,\n to mkulima young online " +
                                "marketplace. Your account is ready for posting new products.\n " +
                                " Our marketplace has only one rule. Do unto the market as you " +
                                "want it to do for you!\n Cheers!!" +
                                "",email
                );
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Functions.showToast("Welcome",
                        Register
                                .this);
            }
        }
                .execute("");
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(SokoContext.getFarmer(realm.allObjects(User.class).first()));
        realm.commitTransaction();
        finish();



        startActivity(new Intent(Register.this, SettingsActivity.class));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        mixpanel.flush();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public boolean isValid() {
        if(TextUtils.isEmpty(email.getText())){
            email.setError("invalid email");
            return false;
        }
        if(TextUtils.isEmpty(phone.getText())){
            phone.setError("missing contacts");
            return false;
        }
        if(TextUtils.isEmpty(password.getText())){
            password.setError("invalid password");
            return false;
        }
        if(password.getText().length()<6){
            password.setError("provide more than 6 characters");
            return false;
        }
        if(TextUtils.isEmpty(fullname.getText())){
            fullname.setError("invalid name");
            return false;
        }
        if(!PhoneNumber.validCellPhone(phone.getText().toString())){
            phone.setError("invalid phone no.");
            return false;
        } if(!PhoneNumberUtils.isGlobalPhoneNumber(phone.getText().toString())){
            phone.setError("invalid phone no");
            return false;
        }
        if(!EmailS.validEmail(email.getText().toString())){
            email.setError("invalid email address");
            return false;
        }
        Database database=new Database(this);
        database.open();
        if(database.isPhoneInUse(phone.getText().toString()))
        {
            phone.setError("Phone number exists");
            Functions.showToast("Phone no. already used",this);
            database.close();
            return false;
        }
        if(database.isEmailInUse(email.getText().toString()))
        {
            email.setError("Email address exists");
            Functions.showToast("Email already used", this);
            database.close();
            return false;
        }
        return true;


    }
    private void trackSignupEvent(String email,String phone){
        try {
            JSONObject props = new JSONObject();
            props.put("email", email);
            props.put("phone", phone);
            props.put("time", new Date().toString());
            mixpanel.track("SignupEvent", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
    }

}
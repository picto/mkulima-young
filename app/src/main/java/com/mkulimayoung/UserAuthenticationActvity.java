package com.mkulimayoung;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.digits.sdk.android.DigitsAuthButton;
import com.digits.sdk.android.DigitsSession;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.mkulimayoung.mkulimaApp.R;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import database.Database;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import models.Farmers;
import models.Product;
import models.User;
import models.UserDevice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import server.ServiceGenerator;
import server.SokoClient;
import server.SokoContext;
import server.SokoUsers;
import tools.Configs;
import tools.EmailS;

public class UserAuthenticationActvity extends AppCompatActivity{
    public static final int LOGIN_CODE = 100;
    /* Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 0;
    private static final String TWITTER_KEY = "LJeoFlIr7x9JgwF1RT0DWdGuz";
    private static final String TWITTER_SECRET = "5u5wwQfc6Hq8aMlD6S5AKmrZYebY0k3cHHRLm2CuTdeUILOK9e";
    private final int REQ_SIGNUP = 1;
    EditText farmer_email_Et;
    EditText farmer_password_Et;
    AppCompatButton submit;
    Context context;
    DigitsAuthButton digitsButton;
//    Toolbar toolbar;
    CallbackManager callbackManager;
    LoginButton facebookloginButton;
    private DigitsSession session;
    private TwitterLoginButton twitter_loginButton;
    /* Is there a ConnectionResult resolution in progress? */
    private boolean mIsResolving = false;
    private Realm realm;
	 /* Should we automatically resolve ConnectionResults when possible? */
	 private boolean mShouldResolve = false;
	    /* Client used to interact with Google APIs. */
	    private GoogleApiClient mGoogleApiClient;
    private TextView messageError,changePassword,message;
    MixpanelAPI mixpanel;
//    MaterialDialog.Builder dialog;
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		context=this;
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Crashlytics());
        realm = Configs.getRealmInstance(this);
        mixpanel=Configs.getMixpanel(this);
//        dialog=new MaterialDialog.Builder(this);
        changePassword=(TextView)findViewById(R.id.link_forgot);
        message=(TextView)findViewById(R.id.message);
        SokoContext.setUsers(this);
        changePassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(EmailS.validEmail(farmer_email_Et.getText().toString())){
                    final String email=farmer_email_Et
                            .getText().toString
                                    ();

                   if(realm.where(Farmers.class).equalTo("email",email).findAll().size()>0){
                       Farmers farmer=realm.where(Farmers.class).equalTo("email",email).findFirst();
                       trackForgotEvent(email,farmer.getMobile());
                       final String deviceId=new Date().getTime()+"";
                       showProgress();
                       resetPassword(deviceId,email,farmer.getMobile());
                   }else {
                       Toast.makeText(UserAuthenticationActvity.this, "invalid email/password", Toast.LENGTH_SHORT).show();
                       message.setText("No such email in MkulimaYoung registry");
                   }

                }else farmer_email_Et.setError("invalid email");
            }
        });

		//setSupportActionBar(toolbar);

        findViewById(R.id.link_signup).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                newAccountCreate();
            }
        });


		farmer_email_Et = (EditText) findViewById(R.id.input_email);
		farmer_password_Et = (EditText) findViewById(R.id.input_password);
		farmer_password_Et
				.setOnEditorActionListener(new OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE) {

							proceed(farmer_email_Et.getText().toString(), farmer_password_Et
                                            .getText().toString(),
                                    context);
							return true;
						}
						return false;
					}
				});
		submit = (AppCompatButton)findViewById(R.id.btn_login);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				proceed(farmer_email_Et.getText().toString(), farmer_password_Et.getText().toString(),
                        context);
			}
		});

	}

    private void resetPassword(final String deviceId,final String email,final String phone) {
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                Functions.sendEmail("Password Reset","Hello,\nYour new password has been reset to" +
                        " "+deviceId+". You can " +
                        "login and" +
                        " change to your desirable password",email
                );
               try {
                   // database.updatePassword(email,Functions.md5(deviceId));
                   SmsManager smsManager = SmsManager.getDefault();
                   String message = "Copy this Mkulima " +
                           "generated " +
                           "password and paste it on Mkulima App to access your account." +
                           " Password: " +
                           " " +
                           "" + deviceId;
                   smsManager.sendTextMessage(phone, null, message
                           ,
                           null, null);
               }catch (Exception e){
                   e.printStackTrace();
               }
                return null;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                dismissMyDialog();
                realm.beginTransaction();
                Farmers farmer=realm.where(Farmers.class).equalTo("email",email).findFirst();
                farmer.setPasscode(Functions.md5(deviceId));
                realm.commitTransaction();
                Toast.makeText(UserAuthenticationActvity.this, "details have been sent to your " +
                        "email", Toast.LENGTH_SHORT).show();

                message.setText("A new password has been sent to "+email+" and phone "+phone+"" +
                       ".\nCopy and paste it as your password");
                message.setTextColor(getResources().getColor(R.color.appColor));
                SokoContext.updateUser(SokoContext.getUser(farmer));
            }
        }.execute("");

    }

    /**
     *
     */
    protected void proceed(final String email,
			final String password, final Context context) {
        if(realm.allObjects(Farmers.class).size()<1){
            MyApp.startSync(this);
            login(email,password);
        }else
            login(email,password);



    }

    private void login(String email, String password) {
        int count=realm.where(Farmers.class).equalTo("email",email).endsWith("passcode",Functions
                .md5(password)).findAll().size();
        if(count>0){
            message.setText("Account login approved");
            realm.beginTransaction();
            realm.allObjects(User.class).deleteAllFromRealm();
            User user=SokoContext.getUser(realm.where(Farmers.class).equalTo("email",email).endsWith("passcode",Functions
                    .md5(password)).findFirst());
            realm.copyToRealmOrUpdate(user);
            realm.commitTransaction();
            finish();
        }else {
            if(realm.where(Farmers.class).equalTo("email",email).count()>0){
                message.setText("Your password is incorrect.");
                Toast.makeText(context, "invalid email/password", Toast.LENGTH_SHORT).show();

            }
            else{
                message.setText("Your email is not registered in MkulimaYoung");
                Toast.makeText(context, "email in not registered", Toast.LENGTH_SHORT).show();
            }

        }

    }
    Dialog pDialog;
    private void showProgress() {
        pDialog = new MaterialDialog.Builder(this)
                .title("Resetting password")
                .content("Generating and sending new password..")
                .progress(true,100).build();
        pDialog.setCancelable(false);
        pDialog.show();


    }
    private void dismissMyDialog() {
        if(pDialog!=null&&pDialog.isShowing())
            pDialog.dismiss();
    }
    private void trackLoginEvent(String email,String phone){
        try {
            JSONObject props = new JSONObject();
            props.put("email", email);
            props.put("phone", phone);
            props.put("time", new Date().toString());
            mixpanel.track("LoginEvent", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
    }

    private void trackForgotEvent(String email,String phone){
        try {
            JSONObject props = new JSONObject();
            props.put("email", email);
            props.put("phone", phone);
            props.put("time", new Date().toString());
            mixpanel.track("ForgotPassword", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.loginmenu, menu);

		// searchView.setIconifiedByDefault(false);

		return super.onCreateOptionsMenu(menu);
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==R.id.register){
			newAccountCreate();
			return true;
		}else
		return super.onOptionsItemSelected(item);
	}

    private void newAccountCreate() {
        finish();
        startActivity(new Intent(this, Register.class));
	}



    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        mixpanel.flush();
    }
}

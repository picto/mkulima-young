package com.mkulimayoung;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

public class CustomGridViewAdapter extends
		ArrayAdapter<HashMap<String, Object>> {

	Context context;
	int layoutResourceId;
	ImageLoader imageLoader;
	ArrayList<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();

	public CustomGridViewAdapter(Context context, int layoutResourceId,
			ArrayList<HashMap<String, Object>> data) {

		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;

		this.data = data;
		imageLoader = new ImageLoader(context);

	}

	@Override
	public HashMap<String, Object> getItem(int position) {

		return super.getItem(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		HashMap<String, Object> item = data.get(position);
		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			holder = new RecordHolder();
			holder.txtTitle = (TextView) row.findViewById(R.id.grditemtxt);
			holder.txtPrice = (TextView) row.findViewById(R.id.grditemtxtprice);
            holder.category = (TextView) row.findViewById(R.id.grditemlocation);
			holder.imageItem = (ImageView) row.findViewById(R.id.grditemimg);

			// holder.myRelaLay= (RelativeLayout)
			// row.findViewById(R.id.paenthold);
			// holder.badge = new BadgeView((Activity)
			// context,holder.moreholder);
			// holder.badge.setText("More");
			// holder.badge.setBackgroundColor(Color.parseColor("#5882FA"));
			// holder. badge.show();

			row.setTag(holder);
		}

		else {
			holder = (RecordHolder) row.getTag();
		}

		holder.txtTitle.setHint(item.get("itemtxt").toString());
		holder.txtPrice.setHint(" " + item.get("itemprice").toString()+" \n "+item.get("itemtown").toString().toUpperCase());

//		if ((Integer) item.get("hasvariant") == 1) {
//			// holder.badge.hide();
//
//		}
		//
		imageLoader.DisplayImage(item.get("itemimg").toString(),
				holder.imageItem);
		return row;
	}

	static class RecordHolder {
		TextView txtTitle;
		TextView txtPrice;
		TextView category;
		ImageView imageItem;
		RelativeLayout myRelaLay;
		BadgeView badge;
	}
}

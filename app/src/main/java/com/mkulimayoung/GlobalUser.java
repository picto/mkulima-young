/**
 * 
 */
package com.mkulimayoung;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * @author robb
 * 
 */
public class GlobalUser extends RealmObject{

	private String user_email;
	private String username;
    @Index
	private String mobile;
    @PrimaryKey
	private String userid;
	private String origin;
    @Index
    private String fullname;
    private String accessToken;
    //facebook,google,twitter or mkulima
    private String profileType;
    private int user_status;
    private String userip;

    public int getUser_status() {
        return user_status;
    }

    public void setUser_status(int user_status) {
        this.user_status = user_status;
    }

    public String getUserip() {
        return userip;
    }

    public void setUserip(String userip) {
        this.userip = userip;
    }

    public int getUser_confirm() {
        return user_confirm;
    }

    public void setUser_confirm(int user_confirm) {
        this.user_confirm = user_confirm;
    }

    public String getUser_lastlogin() {
        return user_lastlogin;
    }

    public void setUser_lastlogin(String user_lastlogin) {
        this.user_lastlogin = user_lastlogin;
    }

    public String getUser_device() {
        return user_device;
    }

    public void setUser_device(String user_device) {
        this.user_device = user_device;
    }

    private int user_confirm;
    private String user_lastlogin;

    public String getUser_confirmdate() {
        return user_confirmdate;
    }

    public void setUser_confirmdate(String user_confirmdate) {
        this.user_confirmdate = user_confirmdate;
    }

    private String user_confirmdate;
    private String user_device;

	public GlobalUser() {

	}

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }




    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
}

/**
 * 
 */
package com.mkulimayoung;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author robb
 * 
 */
public class Questions extends RealmObject{

	/**
	 * @return the bid
	 */
	public String getBid() {
		return bid;
	}

	/**
	 * @param bid
	 *            the bid to set
	 */
	public void setBid(String bid) {
		this.bid = bid;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the county
	 */
	public String getCounty() {
		return county;
	}

	/**
	 * @param county
	 *            the county to set
	 */
	public void setCounty(String county) {
		this.county = county;
	}

	/**
	 * @return the town
	 */
	public String getTown() {
		return town;
	}

	/**
	 * @param town
	 *            the town to set
	 */
	public void setTown(String town) {
		this.town = town;
	}

	/**
	 * @return the clint_ip
	 */
	public String getClint_ip() {
		return clint_ip;
	}

	/**
	 * @param clint_ip
	 *            the clint_ip to set
	 */
	public void setClint_ip(String clint_ip) {
		this.clint_ip = clint_ip;
	}

	/**
	 * @return the user_id
	 */
	public String getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id
	 *            the user_id to set
	 */
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return the published
	 */
	public String getPublished() {
		return published;
	}

	/**
	 * @param published
	 *            the published to set
	 */
	public void setPublished(String published) {
		this.published = published;
	}

	/**
	 * @return the publicationdate
	 */
	public String getPublicationdate() {
		return publicationdate;
	}

	/**
	 * @param publicationdate
	 *            the publicationdate to set
	 */
	public void setPublicationdate(String publicationdate) {
		this.publicationdate = publicationdate;
	}

	/**
	 * @return the expirydate
	 */
	public String getExpirydate() {
		return expirydate;
	}

	/**
	 * @param expirydate
	 *            the expirydate to set
	 */
	public void setExpirydate(String expirydate) {
		this.expirydate = expirydate;
	}

	/**
	 * @return the estate
	 */
	public String getEstate() {
		return estate;
	}

	/**
	 * @param estate
	 *            the estate to set
	 */
	public void setEstate(String estate) {
		this.estate = estate;
	}

	/**
	 * @return the usr_name
	 */
	public String getUsr_name() {
		return usr_name;
	}

	/**
	 * @param usr_name
	 *            the usr_name to set
	 */
	public void setUsr_name(String usr_name) {
		this.usr_name = usr_name;
	}
	private String bid;
	private String comment;
	private String category;
	private String county;
	private String town;
	private String clint_ip;
	private String user_id;
	private String published;
	private String publicationdate;
	private String expirydate;
	private String estate;
    private int answers;
	private String usr_name;

    public int getAnswers() {
        return answers;
    }

    public void setAnswers(int answers) {
        this.answers = answers;
    }
}

/**
 * 
 */
package com.mkulimayoung;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import models.Product;

/**
 * @author robb
 * 
 */
public class mcentral_ton {
	public static final String MPESA = "MPESA";
	public static final String VISA = "VISA";
	public static final String OTHER = "OTHER";
	public static JSONArray list = new JSONArray();
	public static HashMap<String, Object> map = new HashMap<String, Object>();
	public static HashMap<Integer, GlobalItem> updates = new HashMap<Integer, GlobalItem>();
	public static String object = "";
	public static boolean isCached = false;
	public static String CURRENCY = "KES ";
	public static double totalOrder = 0;
	public static boolean isRegistered = false;
	public static final String ITEMS_URL = MyApp.SERVER+"/mkulimaApp/soko.php";
    public static final String ITEMS_SYNC_URL = MyApp.SERVER+"/mkulimaApp/syncitems" +
            ".php";
	public static final String EVENTS_URL = MyApp.SERVER+"/mkulimaApp/events.php";
	public static final String LOGIN_URL = MyApp.SERVER+"/mkulimaApp/login" +
            ".php";
    public static final String NEWPASS_URL = MyApp.SERVER+"/mkulimaApp/userchange.php";
    public static final String NEWPHONE = MyApp.SERVER+"/mkulimaApp/get_mobile.php";
	public static final String VIEWS_URL = MyApp.SERVER+"/mkulimaApp/updateProduct.php";
	public static final String RESOURCES_URL = MyApp.SERVER+"/mkulimaApp/resources.php";
    public static final String RESOURCES_SYNC_URL = MyApp.SERVER+"/mkulimaApp/sync_resources.php";
	public static final String REGISTER_USER = MyApp.SERVER+"/mkulimaApp/regUser.php";
	public static final String UPDATE_USER = MyApp.SERVER+"/mkulimaApp/update_profile.php";
	public static final String UPLOAD_PRODUCT = MyApp.SERVER+"/mkulimaApp/newProduct.php";
	public static final String PHOTOURLONLINE = MyApp.SERVER+"/uploaded.php";
	public static final String QuizURl = MyApp.SERVER+"/mkulimaApp/questions.php";
	public static final String PostQuiz = MyApp.SERVER+"/mkulimaApp/newquestions.php";
	public static JSONObject products = null;
	public static JSONObject resources_json = null;
	public static JSONArray productsArray = new JSONArray();
	public static JSONArray resourcesArray = new JSONArray();
	public static JSONObject eventsjson = null;
	public static JSONArray eventsjArr = new JSONArray();
	public static ArrayList<GlobalItem> Globalproducts = new ArrayList<GlobalItem>();
	public static ArrayList<GlobalItem> Searchedproducts = new ArrayList<GlobalItem>();
	public static ArrayList<GlobalResource> resources = new ArrayList<GlobalResource>();
	public static HashMap<String, ArrayList<GlobalResource>> resource_categories = new HashMap<String, ArrayList<GlobalResource>>();
	public static HashMap<String, ArrayList<GlobalItem>> categories = new HashMap<String, ArrayList<GlobalItem>>();
	public static ArrayList<EventsBean> events = new ArrayList<EventsBean>();
	public static HashMap<Functions.MonthYear, ArrayList<EventsBean>> common_events = new
            HashMap<Functions.MonthYear,
            ArrayList<EventsBean>>();
	protected static String photoname = null;
	protected static String photoURl = null;
	public static ArrayList<Questions> questions = new ArrayList<Questions>();
	public static ArrayList<Questions> searchedQuizes = new ArrayList<Questions>();
	public static boolean sync = true;
	protected static String username = "none";
	protected static String defusername = "mkulima";
	public static String Defauthtoken = "mkulima2015";
	public static String usernamee = "none";
	public static Product selectedItem = null;
}

/**
 *
 */
package com.mkulimayoung;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.LikeView;
import com.facebook.share.widget.LikeView.ObjectType;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.FirebaseException;
import com.firebase.client.ValueEventListener;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.mkulimayoung.mkulimaApp.R;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URI;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import database.Database;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.Priority;
import io.realm.Realm;
import models.Farmers;
import models.Product;
import models.User;
import models.UserDevice;
import server.ServiceGenerator;
import tools.Configs;

/**
 * @author robb
 */
public class ItemView extends AppCompatActivity {
    final static int CLICKS = 1;
    final static int CALLS = 2;
    final static int SMS = 3;
    public static String IMAGE_PATH = "http://www.mkulimayoung.co.ke/soko_/";
    public JSONObject obj = new JSONObject();
    ImageLoader imageLoader;
    LikeView fblikeView;
    Realm realm;
    Product itemOnView;
    Toolbar toolbar;
    private RatingBar ratingBar;
    private FragmentManager fmanager;
    private android.support.v4.app.FragmentTransaction ft;
    TextView tops;
    TextView downs;
    Firebase ref= null;
    TextView overall;
    private MaterialDialog pDialog;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.item_view);
        Fabric.with(this, new Crashlytics());

        imageLoader = new ImageLoader(this);
        realm = Configs.getRealmInstance(this);
        mixpanel = Configs.getMixpanel(this);
        itemOnView = mcentral_ton.selectedItem;
        if (getIntent().hasExtra("notif")) {
            Database database = new Database(this);
            database.open();
            Log.d("MY", "Product id " + getIntent().getIntExtra("notif", 1));
            itemOnView = database.getProduct(getIntent().getIntExtra("notif", 1));
            database.close();
        }

        if (itemOnView == null) {
            onBackPressed();
        } else {
            ImageView img = (ImageView) findViewById(R.id.logo);
            TextView name = (TextView) findViewById(R.id.title);
            TextView whoami = (TextView) findViewById(R.id.whoami);
            overall = (TextView) findViewById(R.id.overall);
            fblikeView = (LikeView) findViewById(R.id.fblike);
            ratingBar = (RatingBar) findViewById(R.id.ratingBar);
            final String reviews_url= ServiceGenerator.API_BASE_URL+"/reviews.php";
            overall.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pDialog = new MaterialDialog.Builder(ItemView.this)
                            .title("Reviews")
                            .content("Please wait...")
                            .progress(true, 100).build();
                    pDialog.setCancelable(true);
                    pDialog.getWindow()
                            .setSoftInputMode(
                                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    pDialog.show();
                    JSONObject jsonObject=new JSONObject();
                    try {
                        jsonObject.accumulate("pid",itemOnView.getPid());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                            reviews_url, jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try{
                                pDialog.dismiss();
                                HashMap<String,Object>allreviews=new HashMap<String, Object>();
                                for(Iterator iterator = response.keys(); iterator.hasNext();) {
                                    String key = (String) iterator.next();
                                   JSONObject jsonObject1= response.getJSONObject(key);
                                    Log.d("RESPONSERATES",jsonObject1.toString());
                                    HashMap<String,Object>reviews=new HashMap<String, Object>();
                                    reviews.put("comment",""+jsonObject1.get("comment"));
                                    reviews.put("rating",""+jsonObject1.get("rating"));
                                    reviews.put("date",""+jsonObject1.get("pubdate"));
                                    reviews.put("user",""+realm.where(Farmers.class).equalTo
                                            ("id",jsonObject1.getInt("userid")).findFirst()
                                            .getName());
                                    allreviews.put(""+reviews.get("user"),reviews);
                                }
                                new MaterialDialog.Builder(ItemView.this)
                                        .title(itemOnView.getPdct_title())
                                        // second parameter is an optional layout manager. Must be a LinearLayoutManager or GridLayoutManager.
                                        .adapter(new ReviewsAdapter(allreviews,ItemView.this), null)
                                        .show();

                            }catch (Exception e){
                                e.printStackTrace();
                            }finally {
                                try{
                                if(pDialog!=null&&pDialog.isShowing())
                                    pDialog.dismiss();
                                }catch (Exception e){

                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            showToast("could not get ratings");
                             {
                                try{
                                    if(pDialog!=null&&pDialog.isShowing())
                                        pDialog.dismiss();
                                }catch (Exception e){

                                }
                            }
                        }
                    });
                    MyApp.getInstance().addToRequestQueue(jsonObjReq, jsonObject.toString());


                }
            });
            if(realm.where(User.class).findAll().size()<1){
                whoami.setVisibility(View.GONE);

            }else{
//                whoami.setVisibility(View.VISIBLE);
                whoami.setText(realm.where(User.class).findFirst().getName()+",\n give this a " +
                        "rating" +
                        " ");

            }
            setRating();

            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, final float rating, boolean fromUser) {
                    if(fromUser){
                        if(realm.where(User.class).findAll().size()<1){
                            showToast("please login to share feedback");
                            startActivityForResult(new Intent(ItemView.this,UserAuthenticationActvity
                                    .class),10001);
                            return;
                        }

                    new MaterialDialog.Builder(ItemView.this)
                            .title("Leave your feedback")
                            .content("Here you can give credit or report incidents")
                            .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES)
                            .input("feedback", "no comment", new MaterialDialog
                                    .InputCallback() {
                                @Override
                                public void onInput(MaterialDialog dialog, CharSequence input) {
                                    applyRating(rating,input.toString());
                                }
                            }).show();



                }

                }
            });
            final ShareButton shareButton = (ShareButton) findViewById(R.id.fbpost);
            TextView desc = (TextView) findViewById(R.id.description);
            tops = (TextView) findViewById(R.id.tops);
            downs = (TextView) findViewById(R.id.down);
            TextView location = (TextView) findViewById(R.id.location);
            TextView quantity = (TextView) findViewById(R.id.stock_level);
            TextView link = (TextView) findViewById(R.id.link);
            TextView call = (TextView) findViewById(R.id.contact_tv);
            TextView sms = (TextView) findViewById(R.id.con);
            final Button tweet = (Button) findViewById(R.id.tweet);
            initializeRates();
            trackPersonVisit();


            tops.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

//                Database database=new Database(ItemView.this);
//                database.open();
//                if(database.getMyEvents(itemOnView.getPid(),realm.allObjects(UserDevice.class).first()
//                        .getDeviceId(),Database.THUMBS_UP)<1)
//                database.thumbUp(itemOnView, realm.allObjects(UserDevice.class).first()
//                        .getDeviceId());
//                database.close();
//                initializeRates();
                }
            });
            downs.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

//                Database database=new Database(ItemView.this);
//                database.open();
//                if(database.getMyEvents(itemOnView.getPid(),realm.allObjects(UserDevice.class).first()
//                        .getDeviceId(),Database.THUMBS_DOWN)<1)
//                database.thumbDown(itemOnView, realm.allObjects(UserDevice.class).first().getDeviceId());
//
//
//                database.close();
//                initializeRates();

                }
            });
            tweet.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {
                        //File myImageFile = new File(item.get("itemimg").toString());

                        FileCache fileCache = new FileCache(ItemView.this);
                        File f = fileCache.getFile(SokoFragment.IMAGE_PATH + itemOnView.getPdct_photo().toString
                                ());
                        Uri myImageUri = Uri.fromFile(f);
                        TweetComposer.Builder builder = new TweetComposer.Builder(ItemView.this)
                                .text(itemOnView.getPdct_title().toString() + " sold at "
                                        + itemOnView.getPdct_unitpce().toString() + " - "
                                        + itemOnView.getPdct_town().toString() + " via #MkulimaYoungApp  " +
                                        "https://goo" +
                                        ".gl/ojunxn "
                                        + "")
                                .image(myImageUri);
                        builder.createIntent().putExtra("id", itemOnView.getPid()).setAction("com" +
                                ".mkulimayoung.mkulimaApp.ACTION_ITEM_VIEW").toURI();

                        builder.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            call.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    makeContactCall();
                }
            });
            sms.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    makeContactSms();
                }
            });
            link.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    String url = getString(R.string.terms_url);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            });
            TextView seller_name = (TextView) findViewById(R.id.seller_tv);
            TextView contact = (TextView) findViewById(R.id.contact_tv);
            findViewById(R.id.comment).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
//                getInput();
                }
            });

            name.setHint(itemOnView.getPdct_title().toString().toString());
            BadgeView badgeView = new BadgeView(this, img);
            badgeView.setText("onsale");
            if (itemOnView.getPdct_status().equalsIgnoreCase("" + 2))
                badgeView.setText("Sold");
            badgeView.show();
            contact.setHint(itemOnView.getUser_cell() == null ? "" : itemOnView.getUser_cell()
                    .toString());
            ArrayList<String> countries = new ArrayList<>(Arrays.asList(getResources().getStringArray(R
                    .array
                    .countries_array)));

            DecimalFormat df=new DecimalFormat("#,###.##");
            String price = ((itemOnView.getPdct_google_location() != null && countries.contains(itemOnView.getPdct_google_location())
                    ? getResources().getStringArray(R.array
                    .countrycurrency)[countries.indexOf(itemOnView.getPdct_google_location())] + " " + df.format(Double.valueOf(itemOnView
                    .getPdct_unitpce())) : "KES " + df.format(Double.valueOf(itemOnView
                    .getPdct_unitpce()))));

            quantity.setText(price);
            if(realm.where(Product.class).equalTo("pdct_ownr",itemOnView.getPdct_ownr()).findAll
                    ().size()>1) {
                seller_name.setHint("view other products by " + (itemOnView.getFull_name()
                        == null ? "" : itemOnView
                        .getFull_name().toString() + "  "));
            }else{
                seller_name.setHint("" + (itemOnView.getFull_name()
                        == null ? "" : itemOnView
                        .getFull_name().toString() + ""));
            }
            seller_name.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent farmer = new Intent(ItemView.this, FarmerProducts.class);
                    farmer.putExtra("farmerid", itemOnView.getPdct_ownr());
                    startActivity(farmer);
                }
            });
            location.setHint(itemOnView.getPdct_google_location() + "\n"
                    + itemOnView.getPdct_town().toString());
            desc.setHint(itemOnView.getPdct_desc().toString());
            fblikeView.setObjectIdAndType(
                    "http://www.mkulimayoung.co.ke/article/soko_pg/"
                            + itemOnView.getPid() + "".toString(), ObjectType.OPEN_GRAPH);
            fblikeView.setLikeViewStyle(LikeView.Style.BOX_COUNT);

            try {
                imageLoader.DisplayImage(SokoFragment.IMAGE_PATH + itemOnView.getPdct_photo().toString(),
                        img);
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }

            findViewById(R.id.similar).setVisibility(View.GONE);

//            FileCache fileCache = new FileCache(ItemView.this);
//            File f = fileCache.getFile(SokoFragment.IMAGE_PATH + itemOnView.getPdct_photo().toString());
            try{
            Uri myImageUri = Uri.parse(URI.create(SokoFragment.IMAGE_PATH + itemOnView.getPdct_photo()
                    .toString()).toString());
            final ShareLinkContent content = new ShareLinkContent.Builder()
                    .setImageUrl(myImageUri)
                    .setContentDescription(itemOnView.getPdct_desc().toString())
                    .setContentTitle(
                            itemOnView.getPdct_title().toString() + " @ "
                                    + itemOnView.getPdct_unitpce().toString() + " - "

                                    + itemOnView.getPdct_town())
//                    .setRef(itemOnView.getPid() + " ".toString())
                    .build();

            shareButton.setShareContent(content);
           shareButton.setOnClickListener(new OnClickListener() {
               @Override
               public void onClick(View v) {
                   ShareDialog.show(ItemView.this, content);
               }
           });
            }catch (Exception e){
                e.printStackTrace();
            }
            final TextView back = (TextView) findViewById(R.id.on);
            final TextView date = (TextView) findViewById(R.id.date);


            back.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // getFragmentManager().beginTransaction()
                    // .replace(R.id.detail, new SummaryGrid()).commit();
                }
            });
        }
    }
    String rate_url= ServiceGenerator.API_BASE_URL+"/rate.php";
    private void applyRating(float rating,String comment) {
       JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.accumulate("rating",rating);
            jsonObject.accumulate("pid",itemOnView.getPid());
            jsonObject.accumulate("comment",comment);
            jsonObject.accumulate("userid",realm.where(User.class).findFirst().getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                rate_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    JSONObject jsonObject1=response.getJSONObject("0");
                    if(jsonObject1.get("rating")!=null){
                        ratingBar.setRating(Float.parseFloat(""+jsonObject1.get("rating")));
                        overall.setText("see user reviews");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApp.getInstance().addToRequestQueue(jsonObjReq, jsonObject.toString());
    }

    private void initializeRates() {
        Database database = new Database(ItemView.this);
        database.open();

        //add view
//        database.itsAView(itemOnView,realm.allObjects(UserDevice.class)
//                .first()
//                .getDeviceId());
        //initialize ratings
//        tops.setText(""+database.getAllEvents(itemOnView.getPid(), realm.allObjects(UserDevice.class)
//                .first()
//                .getDeviceId(), Database.THUMBS_UP));
//        downs.setText(""+database.getAllEvents(itemOnView.getPid(), realm.allObjects(UserDevice.class)
//                .first()
//                .getDeviceId(), Database.THUMBS_DOWN));
        database.close();
    }

    /**
     *
     */


    private String getStatus(int status) {
        switch (status) {
            case 0:
                return "Suspended";

            case 2:
                return "Booked";
            default:
                return "On Sale";
        }
    }

    /**
     * @param localizedMessage
     */
    private void showToast(String localizedMessage) {
        Toast.makeText(ItemView.this, localizedMessage, Toast.LENGTH_LONG)
                .show();
    }

    /**
     *
     */
    private void makeContactSms() {
        Answers.getInstance().logCustom(new CustomEvent("SMS").putCustomAttribute("To",
                "to->" + itemOnView.getUser_cell()).putCustomAttribute("about", itemOnView.getPdct_title()));
        trackSmsEvent();
        Intent sms = new Intent(Intent.ACTION_SENDTO);
        Uri data = Uri.parse("sms:" //$NON-NLS-1$
                + itemOnView.getUser_cell());
        sms.setData(data);
        startActivity(sms);
    }

    /**
     *
     */
    private void makeContactCall() {
        Answers.getInstance().logCustom(new CustomEvent("CALL").putCustomAttribute("To",
                "to->" + itemOnView.getUser_cell()).putCustomAttribute("about", itemOnView.getPdct_title()));
        trackCallEvent();
        Intent call = new Intent(Intent.ACTION_CALL);
        Uri data = Uri.parse("tel:" + itemOnView.getUser_cell());
        call.setData(data);


        try {
            startActivity(call);
        } catch (SecurityException e) {
            e.getLocalizedMessage();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        mixpanel.flush();
    }

    MixpanelAPI mixpanel;

    private void trackCallEvent() {
        try {
            JSONObject props = new JSONObject();
            props.put("callfrom", mixpanel.getPeople().getDistinctId());
            props.put("senderdevice", realm.allObjects(UserDevice.class).first().getDeviceId());
            props.put("time", new Date().toString());
            props.put("callto", itemOnView.getUser_cell());
            props.put("item", itemOnView.getPdct_title());
            props.put("itemid", itemOnView.getPid());
            mixpanel.track("ItemCallEvents", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
    }

    private void trackSmsEvent() {
        try {
            JSONObject props = new JSONObject();
            props.put("smsfrom", mixpanel.getPeople().getDistinctId());
            props.put("senderdevice", realm.allObjects(UserDevice.class).first().getDeviceId());
            props.put("time", new Date().toString());
            props.put("smsto", itemOnView.getUser_cell());
            props.put("item", itemOnView.getPdct_title());
            props.put("itemid", itemOnView.getPid());
            mixpanel.track("ItemSmsEvents", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
    }

    private void trackPersonVisit() {
        try {
            JSONObject props = new JSONObject();
            props.put("viewid", mixpanel.getPeople().getDistinctId());
//            props.put("senderdevice", realm.allObjects(UserDevice.class).first().getDeviceId());
            props.put("time", new Date().toString());
            props.put("item", itemOnView.getPdct_title());
            props.put("itemid", itemOnView.getPid());
            mixpanel.track("ItemViewEvents", props);
        } catch (Exception e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (itemOnView == null)
            finish();
    }
    String rating_url= ServiceGenerator.API_BASE_URL+"/ratings.php";
    public void setRating() {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.accumulate("pid",itemOnView.getPid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                rating_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    JSONObject jsonObject1=response.getJSONObject("0");
                    if(jsonObject1.get("rating")!=null){
                      ratingBar.setRating(Float.parseFloat(""+jsonObject1.get("rating")));
                      overall.setText("see user reviews");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApp.getInstance().addToRequestQueue(jsonObjReq, jsonObject.toString());
    }
//    public void setRating() {
//        Configs.getAllProductRatingsFireBase(itemOnView.getPid()).addValueEventListener(new ValueEventListener() {
//
//            @Override
//            public void onDataChange(DataSnapshot snapshot) {
//                System.out.println(snapshot.getValue());
//                int users=0;
//                float ratings=0;
//                try{
//                    if(snapshot.getValue()==null)
//                        return;
//                    HashMap<String,Object>rates= (HashMap<String, Object>) snapshot.getValue();
//                    for(String userid:rates.keySet()){
//                        users++;
//                        HashMap<Integer,Object> rr= (HashMap<Integer,Object>) rates.get(userid);
//                        ratings+=Double.valueOf(""+rr.get("rating"));
//                    }
//
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//                ratingBar.setRating(ratings/users);
//                 overall.setText("see  "+users+" user "+ (users>1?"reviews":"review"));
//            }
//
//            @Override public void onCancelled(FirebaseError error) { }
//
//        });
//    }
}

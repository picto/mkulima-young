package com.mkulimayoung;

/**
 * Created by robertnyangate on 11/12/15.
 */
public class IntentConstants {

    public static final String ACTION_MAIN = "com.mkulimayoung.mkulimaApp.MAIN";
    public static final String ACTION_ITEMVIEW = "com.mkulimayoung.mkulimaApp.ACTION_ITEM_VIEW";
    public static final String ACTION_QUIZVIEW = "com.mkulimayoung.mkulimaApp.ACTION_QUESTION_VIEW";
    public static final String ACTION_MESSAGEVIEW = "com.mkulimayoung.mkulimaApp" +
            ".ACTION_MESSAGEVIEW";
    public static final String ACTION_HANDLE_TIMEOUT = "com.mkulimayoung.mkulimaApp.ACTION_HANDLE_TIMEOUT";
}

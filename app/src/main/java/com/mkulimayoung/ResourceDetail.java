/**
 * 
 */
package com.mkulimayoung;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * @author robb
 * 
 */
public class ResourceDetail extends AppCompatActivity {
    FragmentManager fmn;
    GlobalResource detail;

    Realm realm;

    @Override
    public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        // Open the default realm for the UI thread.
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();

        // Clear the realm from last time
//        Realm.deleteRealm(realmConfiguration);

        // Create a new empty instance of Realm
        realm = Realm.getInstance(realmConfiguration);

        detail = realm.where(GlobalResource.class).equalTo("id", getIntent().getStringExtra("id"))
                .findFirst();

        TextView date = (TextView) findViewById(R.id.date);
        final TextView title = (TextView) findViewById(R.id.title);
        final TextView body = (TextView) findViewById(R.id.body);
        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO put G+ and twitter buttons here when the content improves
                Functions.sendIntent(ResourceDetail.this, title.getHint().toString(), body.getHint()
                        .toString().substring(0,body.getHint().length()/4)+" via #MkulimaYoungApp" +
                        " https://goo.gl/ojunxn");
            }
        });


        date.setHint("Updated "
                + Functions.getHumanDate(detail.getDateUpdated()));
        title.setHint(detail.getTitle());
        SpannableString ss = new SpannableString(Html.fromHtml(detail.getBody()));
        final ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(ss.));
//                startActivity(i);
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        //ss.setSpan(clickableSpan, 22, 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //body.setHint(Html.fromHtml(detail.getBody()));
        body.setText(ss);
        body.setMovementMethod(LinkMovementMethod.getInstance());
        body.setHighlightColor(Color.TRANSPARENT);


    }



}

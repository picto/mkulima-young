package com.mkulimayoung;

import java.util.Random;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

public class ValidationClass {

	public ValidationClass() {

	}

	public boolean ValidEmail(String EMAIL_ADDRESS) {
		Pattern rfc2822 = Pattern
				.compile("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
		if (rfc2822.matcher(EMAIL_ADDRESS).matches()) {
			// Well formed email
			return true;
		}
		return false;

	}

	public String randomNumbers(int len) {
		Random rnd = new Random();
		String AB = "ABCDEFGHIJKLMNOPQRST0123456789";
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		}

		return sb.toString();

	}

	public boolean isJSONValid(String test) {
		try {
			new JSONObject(test);
			return true;
		} catch (JSONException ex) {
			return false;
		}
	}

	public int getInteger(String input) {
		try {
			return Integer.parseInt(input);

		}

		catch (Exception ex) {
			return 0;
		}
	}

}

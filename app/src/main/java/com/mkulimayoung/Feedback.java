/**
 * 
 */
package com.mkulimayoung;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mkulimayoung.mkulimaApp.R;

/**
 * @author robb
 * 
 */
public class Feedback extends AppCompatActivity {

	private FragmentManager fmanager;
	private android.support.v4.app.FragmentTransaction ft;

	public static Feedback newInstance(FragmentManager fm) {
		Feedback sp = new Feedback();
		sp.fmanager = fm;
		return sp;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.question);

        final EditText namefeed = (EditText) findViewById(R.id.namefeed);
        // final EditText emailfeed = (EditText) v.findViewById(R.id.emailfeed);
        final EditText message = (EditText) findViewById(R.id.messagefeed);
        findViewById(R.id.sendfeed).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // if(TextUtils.isEmpty(emailfeed.getText()))
                // emailfeed.setError("Invalid email");
                if (TextUtils.isEmpty(message.getText()))
                    message.setError("missing message");
                else
                    sendFeedback(message.getText().toString(), namefeed
                            .getText().toString());
            }
        });
	}



	protected void sendFeedback(String message, String name) {
		Intent intentEmail = new Intent(Intent.ACTION_SEND);
		intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[] {
				"jmacharia86@gmail.com", "oigorobert@gmail.com" });
		intentEmail.putExtra(Intent.EXTRA_SUBJECT, "AppFeedback");
		intentEmail.putExtra(Intent.EXTRA_TEXT, message);
		intentEmail.setType("message/rfc822");
		startActivity(Intent.createChooser(intentEmail,
				"Choose an email provider :"));

	}

}

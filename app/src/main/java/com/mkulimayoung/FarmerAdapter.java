package com.mkulimayoung;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import java.util.ArrayList;

import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;
import models.Product;

/**
 * Created by robertnyangate on 10/19/15.
 */
public class FarmerAdapter extends BaseAdapter {

    ImageLoader loader;
    ArrayList<Product>items;
    LayoutInflater inflater;
    public FarmerAdapter(Context context,ArrayList<Product>itemss) {
        loader=new ImageLoader(context);
        items=itemss;
        inflater= ((Activity)context).getLayoutInflater();
    }


    private static class ViewHolder {
        TextView timestamp,place,price;
        ImageView imageView;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Integer.valueOf(items.get(position).getPid());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.productline, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.timestamp = (TextView) convertView.findViewById(R.id.name);
            viewHolder.place=(TextView) convertView.findViewById(R.id.location);
            viewHolder.price=(TextView) convertView.findViewById(R.id.price);
            viewHolder.imageView=(ImageView) convertView.findViewById(R.id.logo);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Product item = items.get(position);
        viewHolder.timestamp.setText(item.getPdct_title());
        viewHolder.place.setText(item.getPdct_town());
        viewHolder.price.setText(item.getPdct_unitpce());
        loader.DisplayImage(SokoFragment.IMAGE_PATH+item.getPdct_photo(),viewHolder.imageView);
        return convertView;
    }

}

package com.mkulimayoung;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class TestInternet {

	public TestInternet() {

	}

	public static boolean isOnline(Context mActivity) {
		ConnectivityManager cm = (ConnectivityManager) mActivity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public boolean isInteger(String input) {
		try {
			Integer.parseInt(input);
			return true;
		}

		catch (Exception ex) {
			return false;
		}
	}

}

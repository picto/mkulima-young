package com.mkulimayoung;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.mkulimayoung.mkulimaApp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by robertnyangate on 9/27/15.
 */
public class ValidateUser extends AppCompatActivity {
    Realm realm;
    private TextView messageError;
    private EditText codeEnter;
    private int mProgressStatus = 0;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userverify);

        final RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();

        // Clear the realm from last time
//        Realm.deleteRealm(realmConfiguration);

        // Create a new empty instance of Realm
        realm = Realm.getInstance(realmConfiguration);
        //  toolbar = (Toolbar) findViewById(R.id.toolbar);


        messageError = (TextView) findViewById(R.id.messageError);
        codeEnter = (EditText) findViewById(R.id.code);
        //setSupportActionBar(toolbar);
        findViewById(R.id.reverse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ValidateUser.this, ProfileView.class));
                finish();
            }
        });
        if (realm.where(GlobalUser.class).findAll().size() > 0) {
            ((TextView) findViewById(R.id.head)).setHint("A confirmation code was sent to the " +
                    "email: " + realm.where(GlobalUser.class).findFirst().getUser_email() + " " +
                    ".\n\nPlease enter the code below to activate your account");

        } else {
            ((TextView) findViewById(R.id.head)).setHint("A confirmation code was sent to the " +
                    "email/phone provided " +
                    ".\n\nPlease confirm the code below then enter a new password");
            findViewById(R.id.verifybutton).setVisibility(View.GONE);
            findViewById(R.id.passchanger).setVisibility(View.VISIBLE);
            findViewById(R.id.codeconfirm).setVisibility(View.GONE);
            findViewById(R.id.reverse).setVisibility(View.GONE);
            final EditText newpassword=(EditText)findViewById(R.id.newpassword);
            findViewById(R.id.changePassButton).setOnClickListener(new View.OnClickListener() {
                                                                       @Override
                                                                       public void onClick(View v) {

                                                                           if (realm.where(ValidCodes.class).equalTo("code", codeEnter.getText().toString())
                                                                                   .equalTo("used", 0).findAll().size() > 0) {

                                                                               changePassword
                                                                                       (newpassword.getText().toString(),realm.where(ValidCodes.class).findFirst().getUserid());
                                                                           }else{
                                                                               messageError.setHint("Invalid code");
                                                                           }

                                                                       }
                                                                   }

            );

        }
        findViewById(R.id.verifybutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyUser();
            }
        });
        findViewById(R.id.resend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.sendCodeToUser(realm,ValidateUser.this);
                Functions.showToast("Sent", ValidateUser.this);
            }
        });
        findViewById(R.id.later).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(ValidateUser.this, Actionactivity.class);
                backIntent.putExtra("ignore", true);
                startActivity(backIntent);

                finish();
            }
        });


    }

    private void changePassword(final String password,final String email) {
        new AsyncTask<String,String,String>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Functions.showDialog("Checking for email..",ValidateUser.this);
            }

            @Override
            protected String doInBackground(String... params) {
                ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
                pairs.add(new BasicNameValuePair("email", email));
                pairs.add(new BasicNameValuePair("password", password));

                return Functions.getJSONFromUrl(pairs,
                        mcentral_ton
                                .NEWPASS_URL);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Functions.dismissDialog();
                if(s==null)
                    Functions.showToast("Network failure!!",ValidateUser.this);
                else
                if(s.contains("failed")){
                    Functions.showToast(s,ValidateUser.this);
                    Answers.getInstance().logCustom(new CustomEvent("ForgotPassword")
                            .putCustomAttribute("password reset" +
                                    "", "failed"));
                }else{

                    Answers.getInstance().logCustom(new CustomEvent("ForgotPassword")
                            .putCustomAttribute("password reset" +
                                    "", "succeeded"));
                    realm.beginTransaction();
                    realm.allObjects(ValidCodes.class).clear();
                    realm.commitTransaction();
                    Functions.showToast("Updated",ValidateUser.this);
                    Functions.makeUserLocal(Realm.getInstance(ValidateUser.this), s);
                    startActivity(new Intent(ValidateUser.this, Actionactivity.class));
                    finish();
                }
            }
        }.execute("");


    }

    private void verifyUser() {
        if (realm.where(ValidCodes.class).equalTo("code", codeEnter.getText().toString())
                .equalTo("used", 0).findAll().size() > 0) {
            Functions.showToast("Account verified", ValidateUser.this);
            realm.beginTransaction();
            realm.allObjects(ValidCodes.class).clear();
            realm.commitTransaction();
//            if(updatepassword){
//                Functions.updateUserPassowrd(newPassword, email);
//            }else{
            realm.beginTransaction();
            GlobalUser userlocal = realm.allObjects(GlobalUser.class).first();
            userlocal.setUser_confirm(1);
            userlocal.setUser_confirmdate(Functions.getDate(new Date().getTime()));
            realm.copyToRealmOrUpdate(userlocal);
            realm.commitTransaction();


        Intent backIntent = new Intent(ValidateUser.this, Actionactivity.class);
        backIntent.putExtra("pushup", true);
        startActivity(backIntent);

        finish();

    }

    else

    {
        messageError.setHint("Invalid code");
    }

}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}

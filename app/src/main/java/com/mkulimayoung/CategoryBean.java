package com.mkulimayoung;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by robertnyangate on 9/24/15.
 */
public class CategoryBean extends RealmObject {
    @PrimaryKey
    private String category;
   /* 0=products
    1=resources,
    2=questions;
    3=comments;*/
    private int categoryType;


public CategoryBean(){

}
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(int categoryType) {
        this.categoryType = categoryType;
    }

}

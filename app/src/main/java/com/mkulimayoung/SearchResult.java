/**
 * 
 */
package com.mkulimayoung;

import android.app.SearchManager;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SearchEvent;
import com.mkulimayoung.mkulimaApp.R;

import java.util.Date;

import database.Database;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import models.Product;

/**
 * @author robb
 * 
 */
public class SearchResult extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

Realm realm;
    DatabaseTable db = new DatabaseTable(this);
    ListView mLVCountries;
    SimpleCursorAdapter mCursorAdapter;
    private TextView txtQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();

        // Clear the realm from last time
//        Realm.deleteRealm(realmConfiguration);

        // Create a new empty instance of Realm
        realm = Realm.getInstance(realmConfiguration);
        mLVCountries = (ListView)findViewById(R.id.lv_countries);

        // get the action bar
        // ActionBar actionBar = getActionBar();

        // Enabling Back navigation on Action Bar icon
        // actionBar.setDisplayHomeAsUpEnabled(true);

        txtQuery = (TextView) findViewById(R.id.txtQuery);
        if (savedInstanceState == null)
            handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {

        setIntent(intent);
        handleIntent(intent);
    }

    /**
     * Handling intent data
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
                String query = intent.getStringExtra(SearchManager.QUERY);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    MkulimaContentProvider.AUTHORITY, SearchRecentSuggestionsProvider.DATABASE_MODE_2LINES);
            suggestions.saveRecentQuery(query, null);
                Cursor c = db.getWordMatches(query, null);
                //process Cursor and display results
            mCursorAdapter = new SimpleCursorAdapter(getBaseContext(),
                    R.layout.searchcats,
                    c,
                    new String[]{"Name",
                            "Price",
                            "Location", "_id","Imagename"}, new int[]{R.id.category, R.id.product,

                    R.id.price, R.id.idd,R.id.productImage}, 0);

            // Setting the cursor adapter for the country listview
            mLVCountries.setAdapter(mCursorAdapter);
            mLVCountries.setAdapter(new SearchResultAdapter(getBaseContext(),c));
            mLVCountries.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mcentral_ton.selectedItem = realm.where(Product.class).equalTo("pid",Integer.valueOf(((TextView) view.findViewById(R.id.idd)).getText()
                            .toString())).findFirst();

                    startActivity(new Intent(SearchResult.this, ItemView.class));
                }
            });


        } else if (intent.getAction().equals(Intent.ACTION_VIEW)) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d("ANSWERS", "today>>>>>>>>" + new Answers().getVersion());
            Answers.getInstance().logSearch(new SearchEvent()
                            .putQuery(query)
                            .putCustomAttribute("time", new Date().toString())
            );
            Cursor c = db.getWordMatches(query, null);
            //process Cursor and display results
            mCursorAdapter = new SimpleCursorAdapter(getBaseContext(),
                    R.layout.searchcats,
                    c,
                    new String[]{"Name",
                            "Price",
                            "Location", "_id","Imagename"}, new int[]{R.id.category, R.id.product,

                    R.id.price, R.id.idd,R.id.productImage}, 0);

            // Setting the cursor adapter for the country listview
            mLVCountries.setAdapter(new SearchResultAdapter(getBaseContext(),c));
            mLVCountries.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mcentral_ton.selectedItem = realm.where(Product.class).equalTo("pid",Integer.valueOf(((TextView) view.findViewById(R.id.idd)).getText()
                            .toString())).findFirst();

                    startActivity(new Intent(SearchResult.this, ItemView.class));
                }
            });


        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = MkulimaContentProvider.CONTENT_URI;
        return new CursorLoader(getBaseContext(), uri, null, null, new String[]{args
                .getString("query")}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}

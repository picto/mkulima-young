package com.mkulimayoung;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import io.realm.RealmResults;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.internal.base.BaseCard;
import it.gmariotti.cardslib.library.view.CardViewNative;
import models.Question;
import models.QuizCard;

/**
 * Created by robertnyangate on 4/1/16.
 */
public class QuizAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<Question> data;//key=>user and value=>payment option
    private static LayoutInflater inflater = null;
    //public ImageLoader imageLoader;
    String mime = "text/html";
    String encoding = "utf-8";
    Date datethis;

    public QuizAdapter(Activity a,ArrayList<Question> d) {
        activity = a;
        data = d;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //imageLoader = new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }




    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.quiz_adapter, null);

        final CardViewNative category = (CardViewNative) vi.findViewById(R.id.quiz);

//        imageSwitcher.setImageDrawable(activity.getResources().getDrawable(R.drawable.logosmall));


        //ImageView tuserImage = (ImageView) vi.findViewById(R.id.listforum_image);

        final Question  question = data.get(position);
        StringBuffer answerSum=new StringBuffer();
        try {
            JSONArray answers=new JSONArray(question.getAnswers_json());
            int k=0;
          myfor:  for(int i=0;i<answers.length();i++){
                JSONObject ans=answers.getJSONObject(i);
                if(k<3)
                answerSum.append(ans.getString("username")+" - "+ans.getString("answer")+"\n");
                if(k>3){
                    answerSum.append("see more...");
                break myfor;
                }
                k++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        answerSum.ensureCapacity(3);
         QuizCard livecard = new QuizCard(activity,question.getTitle(),answerSum
                .toString().length()>3?answerSum.toString():"no answers yet"
                ,question.getId(),activity
                .getResources().getDrawable(R.drawable.hoho));
        livecard.setCatid(question.getId());
        final CardHeader liveheader = new CardHeader(activity);

        liveheader.setPopupMenuPrepareListener(new CardHeader
                .OnPrepareCardHeaderPopupMenuListener() {
            public boolean onPreparePopupMenu(BaseCard basecard, PopupMenu popupmenu) {
                try {

                        popupmenu.getMenu().add(12,1,2,"Answer");
                        ((QuizCard) basecard).setPopupMenu(popupmenu);
                        popupmenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                            if(1==item.getItemId()){
                                Intent intent=new Intent(activity,ChatRoom.class);
                                intent.putExtra("question",question.getTitle());
                                intent.putExtra("owner",question.getUsername());
                                intent.putExtra("channel",question.getChannel());
                                intent.putExtra("quizid",question.getId());
                                intent.putExtra("answers",question.getAnswers_json());
                                activity.startActivity(intent);
                            }
                                return false;

                            }
                        });


                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }


        });
        //overflow menu if more than one variant
        liveheader.setButtonOverflowVisible(true);
//        liveheader.setTitle(question.getTitle());
        livecard.addCardHeader(liveheader);
        livecard.setOnClickListener(new Card.OnCardClickListener() {


            public void onClick(Card card, View view) {
                try {
                    Intent intent=new Intent(activity,ChatRoom.class);
                    intent.putExtra("question",question.getTitle());
                    intent.putExtra("owner",question.getUsername());
                    intent.putExtra("quizid",question.getId());
                    intent.putExtra("channel",question.getChannel());
                    intent.putExtra("answers",question.getAnswers_json());
                    activity.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        });

        category.setCard(livecard);

        return vi;
    }

}

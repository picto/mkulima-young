package com.mkulimayoung;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.mkulimayoung.mkulimaApp.R;

import tools.Configs;

/**
 * Created by robertnyangate on 3/30/16.
 */
public class MkulimaPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    MixpanelAPI mixpanel;

    @Override
    public void onCreate(final Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
//        String syncConnPref = sharedPref.getString(SettingsActivity.KEY_PREF_SYNC_CONN, "");
        mixpanel= Configs.getMixpanel(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
//        updatePreference("pref_key_message");
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
//        updatePreference(key);
        if(key.equalsIgnoreCase("buyer")){
            mixpanel.getPeople().set("Buyer",sharedPreferences.getBoolean("buyer",false));
        }
        if(key.equalsIgnoreCase("supply")){
            mixpanel.getPeople().set("Seller",sharedPreferences.getBoolean("supply",false));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mixpanel.flush();
    }
}

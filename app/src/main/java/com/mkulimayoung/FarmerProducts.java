package com.mkulimayoung;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import java.util.ArrayList;
import java.util.List;

import database.Database;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import models.Product;
import tools.Configs;

/**
 * Created by robertnyangate on 10/18/15.
 */
public class FarmerProducts extends AppCompatActivity {
    Realm realm;
ArrayList<Product> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.farmer_products);

        realm = Configs.getRealmInstance(this);
        if(getIntent().hasExtra("farmerid")){
            items= new ArrayList<>(realm.where(Product.class).equalTo("pdct_ownr", getIntent()
                    .getStringExtra("farmerid")).findAll());
        }

        ListView listView = (ListView)
                findViewById(android.R.id.list);

        if(!items.isEmpty())
        ( (TextView)findViewById(R.id.name)).setText(items.get(0).getFull_name());


        FarmerAdapter adapter = new FarmerAdapter(this,
                items);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mcentral_ton.selectedItem = items.get(position);
                startActivity(new Intent(FarmerProducts.this, ItemView.class));
                finish();
            }
        });


    }



}

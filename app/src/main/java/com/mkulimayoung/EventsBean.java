/**
 * 
 */
package com.mkulimayoung;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * @author robb
 * 
 */
public class EventsBean extends RealmObject {
    @PrimaryKey
	private int eventid;
    @Index
	private String title;
	private String brief;

	private String startDate;
	private String endDate;
	private String startTime;
	private String endTime;
	private String county;
    @Index
	private String town;
	private String venue;
	private String vipStandCost;
	private String standCost;
	private String vipTicketCost;
	private String visitorTicketCost;
	private String userId;

	/**
	 * 
	 */
	public EventsBean() {
	}



	/**
	 * @return the visitorTicketCost
	 */
	public String getVisitorTicketCost() {
		return visitorTicketCost;
	}

	/**
	 * @param visitorTicketCost
	 *            the visitorTicketCost to set
	 */
	public void setVisitorTicketCost(String visitorTicketCost) {
		this.visitorTicketCost = visitorTicketCost;
	}

	/**
	 * @return the vipTicketCost
	 */
	public String getVipTicketCost() {
		return vipTicketCost;
	}

	/**
	 * @param vipTicketCost
	 *            the vipTicketCost to set
	 */
	public void setVipTicketCost(String vipTicketCost) {
		this.vipTicketCost = vipTicketCost;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the brief
	 */
	public String getBrief() {
		return brief;
	}

	/**
	 * @param brief
	 *            the brief to set
	 */
	public void setBrief(String brief) {
		this.brief = brief;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the eventid
	 */
	public int getEventid() {
		return eventid;
	}

	/**
	 * @param eventid
	 *            the eventid to set
	 */
	public void setEventid(int eventid) {
		this.eventid = eventid;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the town
	 */
	public String getTown() {
		return town;
	}

	/**
	 * @param town
	 *            the town to set
	 */
	public void setTown(String town) {
		this.town = town;
	}

	/**
	 * @return the county
	 */
	public String getCounty() {
		return county;
	}

	/**
	 * @param county
	 *            the county to set
	 */
	public void setCounty(String county) {
		this.county = county;
	}

	/**
	 * @return the venue
	 */
	public String getVenue() {
		return venue;
	}

	/**
	 * @param venue
	 *            the venue to set
	 */
	public void setVenue(String venue) {
		this.venue = venue;
	}

	/**
	 * @return the vipStandCost
	 */
	public String getVipStandCost() {
		return vipStandCost;
	}

	/**
	 * @param vipStandCost
	 *            the vipStandCost to set
	 */
	public void setVipStandCost(String vipStandCost) {
		this.vipStandCost = vipStandCost;
	}

	/**
	 * @return the standCost
	 */
	public String getStandCost() {
		return standCost;
	}

	/**
	 * @param standCost
	 *            the standCost to set
	 */
	public void setStandCost(String standCost) {
		this.standCost = standCost;
	}


}

package com.mkulimayoung;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by robertnyangate on 10/3/15.
 */
public class CommentsRoom extends AppCompatActivity{
    private Bundle savedInstance;

    ListView lv;
    TextView chatroom;
    Toolbar toolbar;
    EditText chatText;
    // Create a handler which can run code periodically
    private Handler handler = new Handler();
    ImageLoader loader;
    ImageView imagevew;

    Realm realm;
    ProgressBar progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_comments);
        loader=new ImageLoader(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();

        // Clear the realm from last time
//        Realm.deleteRealm(realmConfiguration);

        // Create a new empty instance of Realm
        realm = Realm.getInstance(realmConfiguration);


        lv=(ListView)findViewById(android.R.id.list);
        chatroom=(TextView)findViewById(R.id.itemview);
        progress=(ProgressBar)findViewById(R.id.progress);


        chatroom.setHint(getIntent().getStringExtra("item"));
        imagevew=(ImageView)findViewById(R.id.imageview);


        savedInstance=savedInstanceState;
        loader.DisplayImage(getIntent().getStringExtra("imagename"),imagevew);
        // getComments();

    }







    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }
}

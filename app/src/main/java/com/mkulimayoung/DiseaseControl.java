package com.mkulimayoung;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.mkulimayoung.mkulimaApp.R;

import org.joda.time.DateTime;

import java.util.ArrayList;

import database.Database;
import io.realm.Realm;
import models.Question;
import models.User;
import models.UserDevice;
import tools.Configs;

/**
 * Created by robertnyangate on 4/11/16.
 */
public class DiseaseControl extends AppCompatActivity {
    Realm realm;
    LinearLayout loader;
    private FragmentManager fmanager;
    private android.support.v4.app.FragmentTransaction ft;
    private Bundle savedInstance;
    Toolbar toolbar;
    private DateTime lastcheck;
//    Spinner catspinner;
    int selection=0;
    private Handler handler = new Handler();
    public static ArrayList<Question> questions;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        savedInstance=savedInstanceState;
        setContentView(R.layout.pestquizlist);
        realm = Configs.getRealmInstance(this);
        userId= realm.allObjects(UserDevice.class).first()
                .getDeviceId();
        userName= "User-" + userId.substring(0, 5);
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
        loader=(LinearLayout)findViewById(R.id.loader);
        listView=(ListView)findViewById(android.R.id.list);
//        catspinner=(Spinner)findViewById(R.id.catspinner);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String[] categ=getResources().getStringArray(R.array.categoriesnew);



        showQuestions();

    }

    private void   showQuestions() {
        Database database=new Database(this);
        database.open();
        questions=database.getAllPestQuestions();

        QuizAdapter quizAdapter=new QuizAdapter(this,questions);
        listView.setAdapter(quizAdapter);

        database.close();

        findViewById(R.id.titlealert).setVisibility(View.VISIBLE);



    }

    // Join a messaging channel.
    private static final int REQUEST_SENDBIRD_CHAT_ACTIVITY = 100;
    private static final int REQUEST_SENDBIRD_CHANNEL_LIST_ACTIVITY = 101;
    private static final int REQUEST_SENDBIRD_MESSAGING_ACTIVITY = 200;
    private static final int REQUEST_SENDBIRD_MESSAGING_CHANNEL_LIST_ACTIVITY = 201;
    private static final int REQUEST_SENDBIRD_USER_LIST_ACTIVITY = 300;
    public static final String appId = "43128A44-C88D-4BA3-BEC6-942652A6BEBC"; /* Sample SendBird
    Application */
    static String userId =null;
    static  String userName = null; /* Generate User Nickname */

    ListView listView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_menu, menu);

        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.postquiz)
            startDialog();
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    Dialog quizDialog;
    private void startDialog() {
        if(realm.allObjects(User.class).size()<1){
            startActivity(new Intent(this,UserAuthenticationActvity.class));

        }else{
            CreateQuestionDialog();}
    }

    private void CreateQuestionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.ask_layout, null);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(v);
        // Add action buttons
        AppCompatButton postQuiz=(AppCompatButton)v.findViewById(R.id.postit);
        final AppCompatSpinner spinner=(AppCompatSpinner)v.findViewById(R.id.spinnercats);
        final EditText quizit=(EditText)v.findViewById(R.id.quizit);


        builder.setTitle("New Question: ");
        final AlertDialog dialog = builder.create();
        postQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(quizit.getText().length()<4){
                    Functions.showToast("Invalid quiz length",DiseaseControl.this);
                    quizit.setError("Invalid length");
                }else {
                    dialog.dismiss();
                    addQuestion(quizit.getText().toString(), spinner.getSelectedItem().toString
                            ());

                }
            }
        });
        dialog.show();
    }

    private void addQuestion(String question,String channel) {
        Database database=new Database(this);
        database.open();
        database.createPestQuestion(question, channel, realm.allObjects(User.class).first().getId
                ());
        database.close();
        showQuestions();
    }
}

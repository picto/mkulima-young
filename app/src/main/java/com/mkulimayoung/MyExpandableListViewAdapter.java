package com.mkulimayoung;

import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by robertnyangate on 9/23/15.
 */

public class MyExpandableListViewAdapter extends BaseExpandableListAdapter {

    private String[] groups = {
            "Test1",
            "Test2",
            "Test3"
    };

    private String[][] children = {

            {
                    "foo1",
                    "foo2",
                    "foo3"
            },

            {
                    "foo1",
                    "foo2",
                    "foo3"
            },

            {
                    "foo1",
                    "foo2",
                    "foo3"
            }
    };

    //Add these lines to your code
    private Context context;
    HashMap<String,List<EventsBean>> expandableListDetail;
    private ArrayList<String>parents;
    private ArrayList<String>kids;


    public MyExpandableListViewAdapter(Context context,HashMap<String,List<EventsBean>>
            expandableListItems) {
        this.context = context;
        this.expandableListDetail=expandableListItems;
        parents=new ArrayList<>(expandableListItems.keySet());
    }

    public EventsBean getChild(int groupPosition, int childPosition) {
        return new ArrayList<>(expandableListDetail.get(parents.get(groupPosition))).get
                (childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public int getChildrenCount(int groupPosition) {
        return new ArrayList<>(expandableListDetail.get(parents.get(groupPosition))).size();
    }

    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        final TextView textView = (TextView) convertView.findViewById(R.id.expandedListItem);
        textView.setHint(getChild(groupPosition, childPosition).getTitle());
        addEvent=(Button)convertView.findViewById(R.id.calendar);
        addEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent calIntent = new Intent(Intent.ACTION_INSERT);
                calIntent.setType("vnd.android.cursor.item/event");
                calIntent.putExtra(CalendarContract.Events.TITLE, textView.getHint().toString());
                calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, getChild(groupPosition, childPosition).getVenue());
                calIntent.putExtra(CalendarContract.Events.DESCRIPTION, getChild(groupPosition, childPosition).getBrief());

                //calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                        Functions.getDateFromHumanTimed(Functions.getHumanDate(getChild(groupPosition, childPosition).getStartDate()
                        ) + " "
                                + Functions.getTimeFromUnixTime(getChild(groupPosition, childPosition)
                                .getStartTime())).getTime());
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                        Functions.getDateFromHumanTimed(Functions.getHumanDate(getChild(groupPosition, childPosition).getEndDate()
                        ) + " "
                                + Functions.getTimeFromUnixTime(getChild(groupPosition, childPosition)
                                .getEndTime())).getTime());

                context.startActivity(calIntent);
            }
        });
        convertView.findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.sendIntent(context, textView.getHint().toString(), "Venue: " + getChild
                        (groupPosition, childPosition)
                        .getVenue()
                        + " " + Functions.getHumanDate(getChild(groupPosition, childPosition).getEndDate()
                ) + " "
                        + Functions.getTimeFromUnixTime(getChild(groupPosition, childPosition)
                        .getEndTime()) + "\nVia MkulimaYoungApp https://goo.gl/ojunxn");


            }
        });


        venue=(TextView)convertView.findViewById(R.id.venue);
        time=(TextView)convertView.findViewById(R.id.schedule);
        detail=(TextView)convertView.findViewById(R.id.details);

        detail.setText(getChild(groupPosition, childPosition).getBrief());
        detail.setMovementMethod(LinkMovementMethod.getInstance());
        venue.setHint(getChild(groupPosition, childPosition).getVenue());
        time.setHint(Functions.getHumanDate(getChild(groupPosition, childPosition).getStartDate()
        ) + " "
                + Functions.getTimeFromUnixTime(getChild(groupPosition, childPosition)
                .getStartTime()) + "\n-"
                +Functions.getHumanDate(getChild(groupPosition, childPosition).getEndDate()) + " " +
                "" +Functions.getTimeFromUnixTime(getChild(groupPosition, childPosition)
                .getEndTime()));
        return convertView;
    }

    public Object getGroup(int groupPosition) {
        return parents.get(groupPosition);
    }

    public int getGroupCount() {
        return parents.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
    private TextView title,venue,time,detail;
    private Button addEvent;
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.listTitle);
        textView.setText(getGroup(groupPosition).toString());

        return convertView;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public boolean hasStableIds() {
        return true;
    }
}

/**
 * 
 */
package com.mkulimayoung;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * @author robb
 * 
 */
public class SearchParam extends AppCompatActivity {
    Toolbar toolbar;
    CheckBox notifyme;
    private FragmentManager fmanager;
    private android.support.v4.app.FragmentTransaction ft;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_fragment);

// Open the default realm for the UI thread.
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();

        // Clear the realm from last time
//        Realm.deleteRealm(realmConfiguration);

        // Create a new empty instance of Realm
        realm = Realm.getInstance(realmConfiguration);
        Button okay = (Button) findViewById(R.id.searchItem);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final EditText search_name = (EditText)
                findViewById(R.id.search_name);
        // final EditText searchtown = (EditText) findViewById(R.id.county);
        final Spinner county = (Spinner) findViewById(R.id.county_spinner);
        final RelativeLayout gview = (RelativeLayout)
                findViewById(R.id.parent);
        notifyme=(CheckBox)findViewById(R.id.notifyme);
        notifyme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });


        okay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int count = realm.where(GlobalItem.class).contains("itemname", search_name.getText()
                        .toString())
                        .findAll().size();
                if (count > 0) {
                    //see whether they searched county


                    Intent i = new Intent(SearchParam.this, Actionactivity.class);
                    i.putExtra("item", search_name.getText().toString());
//                        i.putExtra("town",county
//                                .getSelectedItem().toString());
                    startActivity(i);


                } else {
                    ((TextView) findViewById(R.id.found)).setHint("No such item on the market!");
                    notifyme.setVisibility(View.VISIBLE);

                }

            }

        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}

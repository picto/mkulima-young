package com.mkulimayoung;
/**
 * Created by robertnyangate on 9/24/15.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;
import com.readystatesoftware.viewbadger.*;

import java.util.ArrayList;
import java.util.Arrays;

import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;
import models.Product;

public class GridAdapter extends RealmBaseAdapter<Product> {

    ImageLoader imageLoader;
    Context contx;
    private LayoutInflater inflater;
    private RealmResults<Product> cities = null;
    public GridAdapter(Context context,RealmResults<Product>items) {
        super(context,items,true);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.cities=items;
        imageLoader = new ImageLoader(context);
        contx = context;
    }

    public void setData(RealmResults<Product> details) {
        this.cities = details;
    }

    @Override
    public int getCount() {
        if (cities == null) {
            return 0;
        }
        return cities.size();
    }

    @Override
    public Product getItem(int position) {
        if (cities == null || cities.get(position) == null) {
            return null;
        }
        return cities.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View currentView, ViewGroup parent) {
        if (currentView == null) {
            currentView = inflater.inflate(R.layout.gridviewitem, parent, false);
        }

        Product city = cities.get(position);

        if (city != null) {
            TextView location = ((TextView) currentView.findViewById(R.id.grditemlocation));
            ((TextView) currentView.findViewById(R.id.grditemtxt)).setText(city.getPdct_title());
            ((TextView) currentView.findViewById(R.id.grditemtxtprice)).setHint((city
                    .getPdct_unitpce()));
            if(city.getPdct_town()!=null)
                location.setText((city
                        .getPdct_town()));
            else
                location.setText((city
                        .getPdct_google_location()));
           ImageView image=((ImageView) currentView.findViewById(R.id.grditemimg));
            ImageView countryflag = (ImageView) currentView.findViewById(R.id.countryflag);
            ArrayList<String> countries = new ArrayList<>(Arrays.asList(contx.getResources().getStringArray(R
                    .array
                    .countries_array)));
            boolean isChecked = false;



//            LabelView label = new LabelView(contx);
//            label.setText((isChecked ? contx.getResources().getStringArray(R.array
//                    .countrycurrency)[countries.indexOf(city.getItem_origin())] + " " + city
//                    .getRetail_price() : "KES " + city.getRetail_price()));
//            badge.setText((isChecked ? contx.getResources().getStringArray(R.array
//                    .countrycurrency)[countries.indexOf(city.getPrice())] + " " + city
//                    .getPrice() : "KES " + city.getPrice()));

//            label.setText(city
//                    .getRetail_price());
           TextView price=(TextView)(currentView.findViewById(R.id.grditemtxtprice));
//            BadgeView badge = new BadgeView(contx, price);
//            badge.setText("" + city.getPrice());
            price.setText(""+city.getPdct_unitpce());
//            badge.show();
//            LoadImage.setImage(image,city.getimage_name());
            imageLoader.DisplayImage(SokoFragment.IMAGE_PATH + city.getPdct_photo(), image);

        }

        return currentView;
    }

}
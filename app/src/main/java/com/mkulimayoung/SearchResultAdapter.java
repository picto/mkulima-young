package com.mkulimayoung;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

import java.util.ArrayList;
import java.util.Arrays;

import database.Database;
import io.realm.Realm;
import io.realm.RealmResults;
import models.Product;

/**
 * Created by robertnyangate on 1/9/16.
 */
public class SearchResultAdapter extends BaseAdapter {

    ImageLoader imageLoader;
    Context contx;
    private LayoutInflater inflater;
    private final Cursor cursor;
    ArrayList<Product>cities;
    Database database;
    public SearchResultAdapter(Context context,Cursor c) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(context);
        contx = context;
        cursor=c;
        database=new Database(context);
        database.open();
        cities=new ArrayList<>();
        if(null!=cursor){
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Product   product=new Product();
            product.setPdct_unitpce("" + cursor.getDouble(cursor.getColumnIndex("Price")));
            product.setPid(cursor.getInt(cursor.getColumnIndex("_id")));
            product.setPdct_photo(cursor.getString(cursor.getColumnIndex("Imagename")));
            product.setPdct_title(cursor.getString(cursor.getColumnIndex("Name")));
            product.setPdct_town(cursor.getString(cursor.getColumnIndex("Location")));
            cities.add(product);
            cursor.moveToNext();
        }
        }else{
            //todo create an ask around logic
        }
    }



    @Override
    public int getCount() {

        return cities.size();
    }

    @Override
    public Object getItem(int position) {
      return   cities.get(position);


    }

    @Override
    public long getItemId(int position) {
        return Integer.valueOf(cities.get(position).getPid());
    }


    @Override
    public View getView(int position, View currentView, ViewGroup parent) {
        if (currentView == null) {
            currentView = inflater.inflate(R.layout.searchcats, parent, false);
        }

        Product city = cities.get(position);

        if (city != null) {
            TextView category = ((TextView) currentView.findViewById(R.id.category));
            ((TextView) currentView.findViewById(R.id.price)).setHint(city.getPdct_town());
            ((TextView) currentView.findViewById(R.id.idd)).setText(""+city.getPid());
            ((TextView) currentView.findViewById(R.id.product)).setHint("@ "+(city
                    .getPdct_unitpce()));
            category.setText(city.getPdct_title());
            ImageView image=((ImageView) currentView.findViewById(R.id.productImage));
            imageLoader.DisplayImage(SokoFragment.IMAGE_PATH+city.getPdct_photo(),image);
        }

        return currentView;
    }

}
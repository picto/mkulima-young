package com.mkulimayoung;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.mkulimayoung.mkulimaApp.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import database.Database;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import models.FAnswer;
import models.FQuestioin;
import models.Farmers;
import models.FirebaseListAdapter;
import models.User;
import tools.Configs;

/**
 * Created by robertnyangate on 10/3/15.
 */
public class ChatRoom extends AppCompatActivity {
    private static final int CHATROOM_RCODE = 19;
    private static final int CHATROOM_RETURNCODE = 100;
    private static final int REQCODE = 1;
    ListView lv;
    TextView chatroom;
    EditText chatText;
    TextView uploadmedia;
    ProgressBar progressBar;
    ImageLoader loader;
    Realm realm;
    Toolbar toolbar;
    private Bundle savedInstance;
    // Create a handler which can run code periodically
    private Handler handler = new Handler();
    private String channelTag;
    // Defines a runnable which is run every 100ms

    private Bitmap mybitmap = null;
    private boolean isEditing;
    private int myanswerid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.commentlist);

        realm = Configs.getRealmInstance(this);
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
        loader = new ImageLoader(this);

        lv = (ListView) findViewById(android.R.id.list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Open Discussions");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        chatroom = (TextView) findViewById(R.id.comment);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        uploadmedia = (TextView) findViewById(R.id.uploadstuff);
        uploadmedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectMedia();
            }
        });
// Run the runnable object defined every 100ms
//        progressBar.setVisibility(View.VISIBLE);

        findViewById(R.id.buttonSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (realm.where(User.class).findAll().size() < 1) {
                    Functions.showToast("Please login", ChatRoom.this);
                    Intent intent = (new Intent(ChatRoom.this, UserAuthenticationActvity.class));
                    startActivityForResult(intent, CHATROOM_RCODE);
                } else if (chatText.getText().toString().length() > 0) {
                    postMessage();
                    chatText.setText("");


                }
            }
        });
        chatText = (EditText) findViewById(R.id.chatText);
        chatroom.setText(getIntent().getStringExtra("question")+"?\n\n"+getIntent()
                .getStringExtra("channel"));
//        channelTag = getIntent().getStringExtra("channel");
//        getSupportActionBar().setTitle(chatroom.getHint());
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

        savedInstance = savedInstanceState;
        showAnswers();
    }

    private void postMessage() {
        final String message = chatText.getText().toString();
        FAnswer answer=new FAnswer(message,realm.allObjects(User
                .class).first().getUsername(),Database.properFormat.format(new Date()),getIntent
                ().getStringExtra("quizid"), UUID.randomUUID().toString(),realm.allObjects(User
                .class).first().getEmail());
        Firebase ref=Configs.getAnswerssFireBase(getIntent().getStringExtra("quizid"));
        ref.push().setValue(answer);
        answers.put(answer.getEmail(),answer);
        if(!answers.isEmpty()){
            for(final String email:answers.keySet()){
                final String replyfrom=answers.get(email).getUser();
                if(!email.equalsIgnoreCase(realm.allObjects(User
                        .class).first().getEmail())){
                    new AsyncTask<String, String, String>() {
                        @Override
                        protected String doInBackground(String... params) {
                            try{
                            Functions.sendEmail("MkulimaYoung - #"+getIntent().getStringExtra
                                    ("question"),message+" \n\n Replied by "+replyfrom+"\n\n " +
                                    "please " +
                                    "visit" +
                                    " MkulimaApp to discuss further.\nDo not reply here!!!",
                                    email);
                            }catch (Exception e){
                                e.printStackTrace();
                                Log.d("MkulimaMail","email not sent to >>>"+email);
                            }
                            return null;
                        }
                    }.execute("");
                }
            }
        }

        showAnswers();


    }
    Dialog pDialog;

    private void showProgress(String message) {
        pDialog = new MaterialDialog.Builder(this)
                .title("Answers")
                .content(message)
                .progress(true, 100).build();
        pDialog.show();


    }

    private void dismissMyDialog() {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }
    public static HashMap<String,FAnswer>answers=new HashMap<>();
    private void showAnswers() {
        Firebase mFirebaseRef=Configs.getAnswerssFireBase(getIntent().getStringExtra("quizid"));
        Query queryRef = mFirebaseRef.orderByChild("date");
        showProgress("Loading comments");
        queryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()==null){
                    Toast.makeText(ChatRoom.this, "No comments", Toast.LENGTH_SHORT).show();
                    dismissMyDialog();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        FirebaseListAdapter<FAnswer> quizAdapter=new FirebaseListAdapter<FAnswer>(queryRef,
                FAnswer.class,
                R.layout.chatroomsg, this) {
            @Override
            protected void populateView(View v, FAnswer model) {

                dismissMyDialog();
                if(model.getEmail()!=null){
                    answers.put(model.getEmail(),model);
                }
                ((TextView)v.findViewById(R.id.title)).setText(model.getAnswer());
                ((TextView)v.findViewById(R.id.user)).setText(model.getUser()+"  "+model.getDate
                        ());
                ((TextView)v.findViewById(R.id.quizid)).setText(model.getAnswerid());
            }


        };
        lv.setAdapter(quizAdapter);


    }




    private void selectMedia() {
        selectPhoto();
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHATROOM_RCODE) {
            if (resultCode == CHATROOM_RETURNCODE) {
                postMessage();
            } else {
                Functions.showToast("You need to login first", this);
            }
        }
        if (requestCode == REQCODE)
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImage = data.getData();
                try {
                    String filePath = getPath(selectedImage);
                    String file_extn = filePath.substring(filePath
                            .lastIndexOf(".") + 1); //$NON-NLS-1$

                    if (file_extn.equals("img") || file_extn.equals("jpg") //$NON-NLS-1$ //$NON-NLS-2$
                            || file_extn.equals("png") //$NON-NLS-1$
                            || file_extn.equals("jpeg") //$NON-NLS-1$
                            || file_extn.equals("gif")) { //$NON-NLS-1$
                        if (Functions.isValidSize(filePath)) {
                            mcentral_ton.photoURl = filePath;
                            mcentral_ton.photoname = filePath
                                    .substring(filePath.lastIndexOf("/") + 1);
                            Functions.showToast("Nice Photo "
                                    + mcentral_ton.photoname, this);
                            setPhoto();

                        } else {
                            Functions.showToast("Invalid photo size", this);
                        }

                    } else {

                    }

                } catch (Exception ep) {
                    Log.d("s", ">>>>>>>Error " + ep.getLocalizedMessage());
                }
            }
    }

    // method to start activity for selecting photo to upload.
    public void selectPhoto() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*"); //$NON-NLS-1$
        startActivityForResult(photoPickerIntent, REQCODE);
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void setPhoto() {

        File imgFile = new File(mcentral_ton.photoURl);

        if (imgFile.exists()) {

            mybitmap = BitmapFactory.decodeFile(imgFile
                    .getAbsolutePath());
            ImageView myImage = (ImageView) findViewById(R.id.upload);
            myImage.setVisibility(View.VISIBLE);
            myImage.setImageBitmap(mybitmap);

        }

    }

    private String getName(Uri uri) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chatroom,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        if(item.getItemId()==R.id.sync){
            showAnswers();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        finish();
    }


}

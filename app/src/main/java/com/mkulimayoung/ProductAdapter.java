package com.mkulimayoung;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.mkulimayoung.mkulimaApp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;

import io.realm.RealmResults;
import models.Product;
import server.ServiceGenerator;
import tools.Configs;

/**
 * Created by robertnyangate on 27/08/16.
 */
public class ProductAdapter extends BaseAdapter {
    private static final String TAG ="BambaAdapter" ;
    private RealmResults<Product> products;
    private Context context;
    private LayoutInflater layoutInflater;
    private  VariantClickCallBack variantClickCallBack;
    public ProductAdapter(RealmResults<Product> products, Context context){
        try {
//            this.variantClickCallBack = ((VariantClickCallBack) context);
        this.context=context;
        this.products=products;
        layoutInflater= LayoutInflater.from(context);
        }catch (Exception e){
            throw new ClassCastException("Activity must implement VariantClickCallBack.");
        }

    }
    @Override
    public int getCount() {

        return products.size()>20?20:products.size();
    }

    @Override
    public Object getItem(int i) {
        return products.get(i);
    }

    @Override
    public long getItemId(int i) {
        return products.get(i).getPid();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        final ViewHolderItem viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.product_card, parent, false);
            viewHolder = new ViewHolderItem();
            viewHolder.toolbar = (Toolbar) convertView.findViewById(R.id.toolbar);
            viewHolder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
            viewHolder.cardView = (CardView) convertView.findViewById(R.id.card_view);
            viewHolder.productimage = (ImageView) convertView.findViewById(R.id.productimage);
            viewHolder.productname = (TextView) convertView.findViewById(R.id.title);
            viewHolder.rating = (TextView) convertView.findViewById(R.id.rating);
            viewHolder.variantSize = (TextView) convertView.findViewById(R.id.variants);
            viewHolder.location = (TextView) convertView.findViewById(R.id.location);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        Product events = (Product) getItem(i);

        DecimalFormat df=new DecimalFormat("#,###.##");
        viewHolder.productname.setText(events.getPdct_title());
        viewHolder.location.setText(events.getPdct_town());
//        Log.d("MK",">>> "+events.getPdct_unitpce());
        viewHolder.variantSize.setHint(df.format(Double.valueOf(events.getPdct_unitpce())));
        ImageLoader imageLoader=new ImageLoader(context);
        imageLoader.DisplayImage(SokoFragment.IMAGE_PATH+events.getPdct_photo(),viewHolder.productimage);
        viewHolder.cardView.setCardElevation(10);
        setRating(events.getPid(),viewHolder.rating);








        return convertView;
    }
    public static interface VariantClickCallBack {
        void onVariantClickCallBack(int pid);
    }
    String rating_url= ServiceGenerator.API_BASE_URL+"/ratings.php";
    public void setRating(int pid,final TextView ratingTV) {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.accumulate("pid",pid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                rating_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    JSONObject jsonObject1=response.getJSONObject("0");
                    if(jsonObject1.get("rating")instanceof Double ||jsonObject1.get("rating")
                            instanceof Integer || jsonObject1.get("rating")
                            instanceof String){
                        ratingTV.setText("" +jsonObject1
                                .get("rating"));
                    }else{
                        ratingTV.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApp.getInstance().addToRequestQueue(jsonObjReq, jsonObject.toString());
    }

    static class ViewHolderItem {
        Toolbar toolbar;
        TextView productname,location;
        TextView variantSize,rating;
        CardView cardView;
        ImageView productimage;
        RatingBar ratingBar;
    }
}

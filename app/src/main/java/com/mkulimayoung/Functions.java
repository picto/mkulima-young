/**
 * 
 */
package com.mkulimayoung;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.mkulimayoung.mkulimaApp.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import database.Database;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import models.User;
import server.SokoContext;

/**
 * @author robb
 * 
 */
public class Functions {
    public static final String FULL_PATTERN = "EEE MMM dd yyyy ";
    public static final String FULL_PATTERNTIMES = "EEE MMM dd yyyy HH:mm";
    private final static SimpleDateFormat sdft = new SimpleDateFormat(
            "yyyy-MM-dd");
	private final static SimpleDateFormat sdftm = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
    private static final String TAG ="MKULIMA" ;
    public static String IMAGE_PATH = "http://www.mkulimayoung.co.ke/soko_/";
    static InputStream is = null;
	static JSONObject jObj = null;
	static JSONArray jArray = null;
	static String json = "";
    static MaterialDialog loader;
    private static Dialog d;

	public static String getAmount(double amount, String currency) {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf)
				.getDecimalFormatSymbols();
		decimalFormatSymbols.setCurrencySymbol(currency + " ");
		((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);

		String value = nf.format(amount);
		return value;

	}

public static void showSnackBar(View v,String text){
    Snackbar snack = Snackbar.make(v, text,
            Snackbar.LENGTH_LONG);
    View view = snack.getView();
    FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
    params.gravity = Gravity.TOP;
    view.setLayoutParams(params);
    snack.show();
}

	public static long getTime(String date) {
		try {
			Date d = sdft.parse(date + " "
					+ new GregorianCalendar().get(Calendar.HOUR_OF_DAY) + ":"
					+ (new GregorianCalendar().get(Calendar.MINUTE)));
			return d.getTime();
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return 0;
	}

    public static byte[] getBitmapBytes(Bitmap bmp){

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

	@SuppressLint("SimpleDateFormat")
	public static String getHumanDate(String date) {
		try {
			Date d = sdft.parse(date);
			SimpleDateFormat df = new SimpleDateFormat(FULL_PATTERN);
			return df.format(d);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return date;
	}

    public static String getTimeFromUnixTime(String longvalue){
        Date d = new Date(Long.parseLong(longvalue));
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        return df.format(d);
    }

	@SuppressLint("SimpleDateFormat")
	public static String getHumanDateTime(String date) {
		try {
			Date d = sdftm.parse(date);
			SimpleDateFormat df = new SimpleDateFormat(FULL_PATTERN);
			return df.format(d);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return date;
	}

	@SuppressLint("SimpleDateFormat")
	public static Date getDateFromHuman(String datehuman) {
		try {

			SimpleDateFormat df = new SimpleDateFormat(FULL_PATTERN);
			return df.parse(datehuman);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return new Date();
	}

    @SuppressLint("SimpleDateFormat")
    public static Date getDateFromHumanTimed(String datehuman) {
        try {

            SimpleDateFormat df = new SimpleDateFormat(FULL_PATTERNTIMES);
            return df.parse(datehuman);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        return new Date();
    }

	@SuppressLint("SimpleDateFormat")
	public static String getHumanMonth(String date) {
		try {
			Date d = sdft.parse(date);
			SimpleDateFormat df = new SimpleDateFormat("MMM yyyy");
			return df.format(d);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return date;
	}

	public static String formatTodayDate() {
		return sdft.format(new Date());
	}

	public static Date getDate(String date) {
		try {
			Date d = sdft.parse(date);
			return d;
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return new Date();
	}

	public static String getDate(long time) {
		String date = "";
		Date d = new Date(time);
		date = sdft.format(d);
		return date;
	}

	public static void showToast(String string, Context c) {
        Toast.makeText(c, string, Toast.LENGTH_LONG).show();



	}

    public static void showToastHere(View view,String message){
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction("DONE", null).show();
    }

	public static void showDialog(String string, Context c) {

		if (loader!=null&&loader.isShowing())
			 loader.dismiss();

        loader = new MaterialDialog.Builder(c).content("Please wait...").iconRes(R.drawable.mylogo)
                .progress(true, 0).show();
//		loader = new Dialog(c, R.style.CustomDialog);
//		loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		LayoutInflater li = LayoutInflater.from(c);
//		View content = li.inflate(R.layout.loading_toast, null);
//		((TextView) content.findViewById(R.id.message)).setText(string);
//		loader.setContentView(content);
//		// loader.setMessage(string);
//		// loader.setIcon(c.getResources().getDrawable(R.drawable.ic_launcher));
//		loader.show();

	}

	// public static boolean checkLogin(String brand, String f_id, Context c) {
	//
	// ArrayList<NameValuePair> pairs = new ArrayList<>();
	// pairs.add(new BasicNameValuePair("farmer_id", f_id));
	// pairs.add(new BasicNameValuePair("farmer_brand", brand));
	// // try {
	// // String response = CustomHttpClient.executeHttpPost(LOGINURL, pairs);
	// // String res = response.toString();
	// // Log.d("RESPONSE", ">>>>>>>>>>>>>>" + res);
	// //
	// // return res.contains("success");
	// //
	// // } catch (Exception e) {
	// // // TODO Auto-generated catch block
	// // e.printStackTrace();
	// // showToast("Error " + e.getLocalizedMessage(), c);
	// // Log.d("Error", ">>>>>>>>>>>>>>" + e.getLocalizedMessage());
	// //
	// // }
	// //return getJSONFromUrl(pairs, LOGINURL);
	// }

    public static void dismissDialog() {
        if (loader != null && loader.isShowing())
            loader.dismiss();
    }

	public static String getJSONFromUrl(List<NameValuePair> params, String url) {
        ConnectionManager conn=new ConnectionManager(url);

        try {
            conn.addAllParams(new ArrayList<>(params));
            return conn.sendRequest();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

	public static String getCategoryItems(String url,ArrayList<NameValuePair>params) {
		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}
		Log.d(">>", ">>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<\n" + json);
		return json;
	}

	/**
	 * @param name
	 * @param county
	 * @return
	 */
	public static ArrayList<GlobalItem> filterProducts(String name,
			String county) {
		ArrayList<GlobalItem> result = new ArrayList<GlobalItem>();
		for (GlobalItem product : mcentral_ton.Globalproducts) {
			if (county != null) {
				if (product.getitemname().toLowerCase()
						.contains(name.toLowerCase())
						&& product.getItem_origin().contains(county))
					result.add(product);
			} else {
				if (product.getitemname().contains(name))
					result.add(product);
			}

		}

		return result;
	}

	public static HashMap<String, ArrayList<GlobalItem>> getCategorisedItems() {
		HashMap<String, ArrayList<GlobalItem>> categories = new HashMap<String, ArrayList<GlobalItem>>();
		HashMap<String, String> mapped = new HashMap<String, String>();
		categories.put("ALL", mcentral_ton.Globalproducts);
		for (GlobalItem product : mcentral_ton.Globalproducts) {
			mapped.put(product.getCategory(), "");

		}
		for (String category : mapped.keySet()) {
			categories.put(category,
					getGroup(category, mcentral_ton.Globalproducts));
		}
		return categories;
	}

	public static HashMap<MonthYear, ArrayList<EventsBean>> getCategorisedEvents(Realm realm) {
		HashMap<MonthYear, ArrayList<EventsBean>> categories = new HashMap<MonthYear,
                ArrayList<EventsBean>>();
		HashMap<String, String> mapped = new HashMap<String, String>();

        for (EventsBean product : realm.allObjects(EventsBean.class)) {

			mapped.put(product.getStartDate(), "");

		}
		for (String date : mapped.keySet()) {
			categories.put(new MonthYear(getHumanMonth(date)),
					getDateGroup(date, realm.allObjects(EventsBean.class)));


		}
        Collections.sort(new ArrayList<MonthYear>(categories.keySet()));


		return categories;
	}

    public static String getRandomString(){
        Random numb=new Random();

        String code=numb.nextInt()+"";
        return new DateTime().getMillisOfSecond()+"_"+code;
    }

    public static void sendCodeToUser(Realm realm,Context context) {
        final String emailTo=realm.where
                (GlobalUser
                        .class).findFirst().getUser_email();
        final String fullname=realm.where
                (GlobalUser
                        .class).findFirst().getFullname();
        Random numb=new Random();

        String code=numb.nextLong()+"";
      final  String codeToSend=code.substring(code.length() - 4, code.length());
        new Thread(new Runnable() {
            public void run() {


                Functions.sendEmail("MkulimaApp Activation", "Hello "+fullname
                                +",\nPlease confirm the account you just created on MkulimaYoung " +
                                "App" +
                                " by using this validation-code to activate: "+codeToSend+"\n\n",
                        emailTo);
            }
        }).start();
        realm.beginTransaction();
        ValidCodes codegen=  realm.createObject(ValidCodes.class);
        codegen.setUserid(realm.where(GlobalUser.class).findFirst().getUserid());
        codegen.setCode(codeToSend);
        codegen.setCodeid(numb.nextInt());
        codegen.setCodeid(Functions.class.hashCode() + numb.nextInt());
        codegen.setUsed(0);
        realm.commitTransaction();
        SmsManager smsManager = SmsManager.getDefault();
        String message="MkulimaYoung" +
                " " +
                "Account Activation Code is: " + codeToSend;
        smsManager.sendTextMessage(getUser(realm).getMobile(), null, message
                ,
                null, null);
        try{
        context.getContentResolver().delete(Uri.parse("content://sms/sent"), "address = ? and " +
                        "body = ?",
                new String[]
                        {getUser(realm).getMobile(), message});
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public static String sendCodeToUsernow(final String email, Realm realm, Context context) {

        Random numb=new Random();

        String code=numb.nextLong()+"";
        final  String codeToSend=code.substring(code.length() - 4, code.length());
        new Thread(new Runnable() {
            public void run() {
                if(android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())

                Functions.sendEmail("MkulimaApp Activation", "Hello"
                                +",\nYou requested a new password for the account on " +
                                "MkulimaYoung " +
                                "App.\n\n" +
                                " Please use this activation code to reset your password: "+codeToSend+"\n\n",
                        email);
            }
        }).start();
        realm.beginTransaction();
        realm.clear(ValidCodes.class);
        ValidCodes codegen=  realm.createObject(ValidCodes.class);
        codegen.setCode(codeToSend);
        codegen.setCodeid(Functions.class.hashCode() + numb.nextInt());
        codegen.setUsed(0);
        codegen.setUserid(email);
        realm.commitTransaction();

        sendMessageWithCode_ForgotPassword(email, codeToSend, context);
        return code;




    }

    public static GlobalUser getUser(Realm realm) {
    GlobalUser user=null;
    if( realm.where(GlobalUser.class).findAll().size()>0)
        user=realm.where(GlobalUser.class).findFirst();
    return user;
}

    public static void sendMessageWithCode_ForgotPassword(final String email, final String code,
                                                         final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
                pairs.add(new BasicNameValuePair("email", email));
                Realm realm =Realm.getInstance(MyApp.getInstance().getRealmConfiguration());

               String jsonprofile=getJSONFromUrl(pairs, mcentral_ton
                        .NEWPHONE);
                Log.d("DEBUG","getprofile>>>>>>>"+jsonprofile);
                if (jsonprofile != null && jsonprofile.contains("profile")) {
                    makeUserLocal(realm,jsonprofile);
                    try {
                        SmsManager smsManager = SmsManager.getDefault();
                        String message="MkulimaYoung" +
                                " " +
                                "Account Activation Code is: " + code;
                        smsManager.sendTextMessage(getUser(realm).getMobile(), null,
                                message,
                                null, null);
                        try{
                            context.getContentResolver().delete(Uri.parse("content://sms/sent"), "address = ? and " +
                                            "body = ?",
                                    new String[]
                                            {getUser(realm).getMobile(), message});
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    } catch (Exception e) {

                        e.printStackTrace();
                    }


                }
                realm.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void requestInternet(final Context actionactivity, boolean b) {
        new MaterialDialog.Builder(actionactivity)
                .title("Enable Data Connection?")
                .content("Promise: will use minimal data then turn off data for you")
                .positiveText("Turn on data")
                .cancelable(true)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        try {
                            Functions.setMobileDataEnabled(actionactivity,true);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        } catch (NoSuchFieldException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                    }
                })
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                    }
                })
                .show();
    }

	public static HashMap<String, ArrayList<Questions>> getCategorisedQuiz(
			ArrayList<Questions> quizz) {
		HashMap<String, ArrayList<Questions>> categories = new HashMap<String, ArrayList<Questions>>();
		HashMap<String, String> mapped = new HashMap<String, String>();
		for (Questions product : quizz) {

			mapped.put(product.getCategory(), "");

		}
		for (String date : mapped.keySet()) {
			categories.put(date, getGroupQuiz(date, quizz, true));
		}
		return categories;
	}

	public static HashMap<String, ArrayList<Questions>> getCountyQuiz(
			ArrayList<Questions> quizz) {
		HashMap<String, ArrayList<Questions>> categories = new HashMap<String, ArrayList<Questions>>();
		HashMap<String, String> mapped = new HashMap<String, String>();
		for (Questions product : quizz) {

			mapped.put(product.getCounty(), "");

		}
		for (String date : mapped.keySet()) {
			categories.put(date, getGroupQuiz(date, quizz, false));
		}
		return categories;
	}

	public static HashMap<String, ArrayList<GlobalResource>> getCategorisedResources() {
		HashMap<String, ArrayList<GlobalResource>> categories = new HashMap<String, ArrayList<GlobalResource>>();
		HashMap<String, String> mapped = new HashMap<String, String>();
		categories.put("ALL", mcentral_ton.resources);
		for (GlobalResource product : mcentral_ton.resources) {
			mapped.put(product.getCategory(), "");

		}
		for (String category : mapped.keySet()) {
			categories.put(category,
					getGroupResource(category, mcentral_ton.resources));
		}
		return categories;
	}

	public static ArrayList<GlobalItem> getGroup(String category,
			ArrayList<GlobalItem> list) {
		ArrayList<GlobalItem> items = new ArrayList<GlobalItem>();
		for (GlobalItem i : list) {
			if (i.getCategory().equalsIgnoreCase(category))
				items.add(i);

		}
		return items;
	}

	public static ArrayList<Questions> getGroupQuiz(String category,
			ArrayList<Questions> list, boolean iscategory) {
		if (iscategory) {
			ArrayList<Questions> items = new ArrayList<Questions>();
			for (Questions i : list) {
				if (i.getCategory().equalsIgnoreCase(category))
					items.add(i);

			}
			return items;
		} else {
			ArrayList<Questions> items = new ArrayList<Questions>();
			for (Questions i : list) {
				if (i.getCounty().equalsIgnoreCase(category))
					items.add(i);

			}
			return items;
		}
	}

	public static ArrayList<EventsBean> getDateGroup(String date,
			RealmResults<EventsBean> list) {
		ArrayList<EventsBean> items = new ArrayList<EventsBean>();
		for (EventsBean i : list) {
			if (new DateTime(Functions.getDate(i.getStartDate()))
					.getMonthOfYear() == new DateTime(Functions.getDate(date))
					.getMonthOfYear()
					&& (new DateTime(Functions.getDate(i.getStartDate()))
							.getYear() == new DateTime(Functions.getDate(date))
							.getYear())) {

				items.add(i);
			}

		}
		return items;
	}

	public static ArrayList<GlobalResource> getGroupResource(String category,
			ArrayList<GlobalResource> list) {
		ArrayList<GlobalResource> items = new ArrayList<GlobalResource>();
		for (GlobalResource i : list) {
			if (i.getCategory().equalsIgnoreCase(category))
				items.add(i);

		}
		return items;
	}

    public static String GetCountryZipCode(Context contx, int countryno) {
        String CountryID = "";
        String CountryZipCode = "ke";

        //getNetworkCountryIso
        try {
            String[] rl = contx.getResources().getStringArray(R.array.CountryCodes);
//            String[] g = rl[countryno].split(",");
//            Log.d("CountryCheck",">>>>>>>>>>>country no "+g[1]);
            return rl[countryno];
//            if (g[1].trim().equals(CountryID.trim())) {
//               return  g[0];
//            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("CountryError", ">>>>>>>>>>>country error " + e.getMessage());
        }
        return CountryZipCode;
    }

    public static void loadMyGrid(final Context c, final boolean forceupdate) {

		new AsyncTask<String, String, ArrayList<GlobalItem>>() {

			@Override
			protected void onPreExecute() {

				super.onPreExecute();
				// dialog.setMessage("Loading...");
				// dialog.show();
			}

			@Override
			protected ArrayList<GlobalItem> doInBackground(String... params) {


				return new ArrayList<GlobalItem>();

			}

			@Override
			protected void onPostExecute(ArrayList<GlobalItem> result) {

				super.onPostExecute(result);
				// dialog.dismiss();

			}
		}.execute("");

	}

	/*
	 * @param email
	 * 
	 * @param password
	 * 
	 * @param activity
	 * 
	 * @return
	 */
	public static User checkLoginAdmin(String email, String password,Context context) {
        Database database=new Database(context);
        database.open();
        User user=database.getUser(email,password);
        database.close();
        return user;


	}
    public static String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Log.d("DEBUG",">>>>>>>"+e.getLocalizedMessage());
        }
        return "";
    }

	public static String getJsonResponse(String url, String json) {
		InputStream inputStream = null;
		String result = "";
		HttpClient httpclient = new DefaultHttpClient();

		HttpPost httpPost = new HttpPost(url);

		StringEntity se;
		try {
			se = new StringEntity(json);
			httpPost.setEntity(se);
		} catch (UnsupportedEncodingException e1) {

			e1.printStackTrace();
		}

		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");

		System.out.println(" >>>>the post " + httpPost.toString() + " URL ");

		HttpResponse httpResponse;
		try {
			httpResponse = httpclient.execute(httpPost);

			inputStream = httpResponse.getEntity().getContent();

			if (inputStream != null) {
				result = convertInputStreamToString(inputStream);
				Log.d("MK", "Mkulima>>>>>>>>>>>Response Json " + result);

			} else {
				result = "9999";
			}
		} catch (ClientProtocolException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		// Log.d("result", result);
		System.out.println("result" + result);
		// Log.i("ded"," >>>> "+result);
		// 11. return result
		return result;
	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}

	/**
	 * @param fullname
	 * @param email
	 * @param cellphone
	 * @param password
	 * @param county
	 * @param country
	 * @param username
	 * @return
	 */
	public static String registerUser(String fullname, String email,
			String cellphone, String password, String county, String country,
			String username, Context context, int newsletter) {

		ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("full_name", fullname));
		pairs.add(new BasicNameValuePair("user_name", username));
		pairs.add(new BasicNameValuePair("user_email", email));
		pairs.add(new BasicNameValuePair("user_cell", cellphone));
		pairs.add(new BasicNameValuePair("user_county", county));
		pairs.add(new BasicNameValuePair("user_country", country));
		pairs.add(new BasicNameValuePair("user_password", password));
		pairs.add(new BasicNameValuePair("newsletter", "" + newsletter));
        pairs.add(new BasicNameValuePair("user_creationdate", "" + Functions.getDate(new Date()
                .getTime())));


		String json = getJSONFromUrl(pairs, mcentral_ton.REGISTER_USER);

		return json;

	}
public static boolean makeUserLocal(Realm realm,String jsonprofile){
    if (jsonprofile != null && jsonprofile.contains("profile")) {
        try {
            JSONObject ob = new JSONObject(jsonprofile);
            JSONArray arr = ob.getJSONArray("profile");

            GlobalUser user = new GlobalUser();
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);

//								user = new Gson().fromJson(obj.toString(),
//										GlobalUser.class);
                //save local
                realm.beginTransaction();
                //clear any local instances
                realm.allObjects(GlobalUser.class).clear();
                realm.createObjectFromJson(GlobalUser.class, obj.toString());
                realm.commitTransaction();


            }

            return true;

        } catch (JSONException e) {

            e.printStackTrace();
            return false;

        }

    }else {
        return false;
    }
}
	public static GlobalUser updateUser(GlobalUser user,
			Context context) {

		ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("full_name", user.getFullname()));
		pairs.add(new BasicNameValuePair("user_name", user.getUsername()));
		pairs.add(new BasicNameValuePair("user_email", user.getUser_email()));
		pairs.add(new BasicNameValuePair("user_cell", user.getMobile()));
		pairs.add(new BasicNameValuePair("user_county", user.getOrigin()));
		pairs.add(new BasicNameValuePair("user_id", user.getUserid()+""));
        pairs.add(new BasicNameValuePair("user_confirm", user.getUser_confirm()+""));
        pairs.add(new BasicNameValuePair("user_confirmdate", user.getUser_confirmdate()+""));
        pairs.add(new BasicNameValuePair("status", user.getUser_status()+""));

		String json = getJSONFromUrl(pairs, mcentral_ton.UPDATE_USER);
//		if (json.contains("profile")) {
//			try {
//				JSONObject ob = new JSONObject(json);
//				JSONArray arr = ob.getJSONArray("profile");
//
//				for (int i = 0; i < arr.length(); i++) {
//					JSONObject obj = arr.getJSONObject(i);
//					return new Gson()
//							.fromJson(obj.toString(), GlobalUser.class);
//				}
//			} catch (Exception e) {
//				Log.d("my", ">>>>>>>>>>>>>>" + e.getLocalizedMessage());
//			}
//		}
		return null;

	}

	/**
	 * @param product
	 * @return
	 */
	public static String uploadProduct(ProductUpload product) {

		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(mcentral_ton.UPLOAD_PRODUCT);
		ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();

		pairs.add(new BasicNameValuePair("newproduct", new Gson().toJson(
				product, ProductUpload.class)));

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(pairs));
			HttpResponse response = httpClient.execute(httpPost);

			String responseString = EntityUtils.toString(response.getEntity());
			Log.d("MK", "Uploading product Response>>>>>>>>" + responseString);

			return responseString;
		} catch (Exception e) {
			e.printStackTrace();
			return "error getting data";
		}

	}

	public static boolean isValidSize(String filepath) {
		String value = null;
		long Filesize = getFolderSize(new File(filepath)) / 1024;// call
																	// function
																	// and
																	// convert
																	// bytes
																	// into Kb
        return Filesize < 300000;
    }

	public static long getFolderSize(File f) {
		long size = 0;
		if (f.isDirectory()) {
			for (File file : f.listFiles()) {
				size += getFolderSize(file);
			}
		} else {
			size = f.length();
		}
		return size;
	}

//	@SuppressWarnings("deprecation")
//	public static String uploadFile(String sourceFileUri) {
//		File imgFile = new File(sourceFileUri);
//		Bitmap old_bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//		Bitmap resizedImage = Bitmap.createScaledBitmap(old_bitmap, 120, 120,
//				true);
//		FileOutputStream out = null;
//
//		try {
//			out = new FileOutputStream(imgFile);
//			resizedImage.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp
//																		// is
//																		// your
//																		// Bitmap
//																		// instance
//			// PNG is a lossless format, the compression factor (100) is ignored
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (out != null) {
//					out.close();
//				}
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//
//		StringBuilder s = new StringBuilder();
//		HttpClient httpClient = new DefaultHttpClient();
//		HttpPost postRequest = new HttpPost(mcentral_ton.PHOTOURLONLINE);
//		File file = new File(sourceFileUri);
//		FileBody bin = new FileBody(file);
//
//		@SuppressWarnings("deprecation")
//        MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();
//				//HttpMultipartMode.BROWSER_COMPATIBLE);
//
//		reqEntity.addPart("fileToUpload", bin); //$NON-NLS-1$
//
//		try {
//			//reqEntity.addPart("name", new StringBody(mcentral_ton.photoname)); //$NON-NLS-1$
//			postRequest.setEntity(reqEntity.build());
//			HttpResponse response = httpClient.execute(postRequest);
//			BufferedReader reader = new BufferedReader(new InputStreamReader(
//					response.getEntity().getContent(), "UTF-8")); //$NON-NLS-1$
//			String sResponse;
//
//			while ((sResponse = reader.readLine()) != null) {
//				s = s.append(sResponse);
//			}
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IllegalStateException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return s.toString();
//
//	}

	/**
	 * Logout user
	 */
	public static void logoutUser(Context c) {


	}

	public static void loginUser(final FragmentManager fm,
			final Context context, final Fragment frag) {


    }

    /**
	 * @param quizurl
	 * @return
	 */
	public static ArrayList<Questions> getQuestions(String quizurl,
			Context ctx, boolean forceupdate) {
		ArrayList<Questions> quiz = new ArrayList<Questions>();



		return quiz;
	}

	public static String postQuestion(Questions quiz) {

		ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("newquestion", new Gson().toJson(quiz,
                Questions.class)));

		return getJSONFromUrl(pairs, mcentral_ton.PostQuiz);
	}

	public static void updateQuizes(final Context contex,
			final boolean showToast) {
		new AsyncTask<String, String, ArrayList<Questions>>() {
			ProgressDialog dialog;

			@Override
			protected void onPreExecute() {
				dialog = new ProgressDialog(contex);

				dialog.setMessage("Please wait..Refreshing...");
				if (showToast)
					dialog.show();

            }

            @Override
            protected ArrayList<Questions> doInBackground(String... params) {

				return getQuestions(mcentral_ton.QuizURl, contex, true);
			}

			@Override
			protected void onPostExecute(ArrayList<Questions> result) {

				super.onPostExecute(result);
				if (showToast)
					dialog.dismiss();
				mcentral_ton.questions = result;

			}
		}.execute("");
	}

    public static RealmList<GlobalItem> extractProducts(String json) {
    RealmList<GlobalItem>productlist=new RealmList<>();
    try {
        //check if there are any items

        HashMap<CategoryBean,String> categories=new HashMap<CategoryBean, String>();
        JSONObject products = new JSONObject(json);
        JSONArray productsArray = products
                .getJSONArray("products");

        for (int i = 0; i < productsArray.length(); i++) {
            JSONObject obj = productsArray
                    .getJSONObject(i);

            GlobalItem item = new GlobalItem();
            item.setCategory(obj.getString("pdct_category"));
            CategoryBean c=new CategoryBean();
            c.setCategory(item.getCategory());
            categories.put(c, "");
            if (obj.has("pdct_pub_date"))
                item.setPubdate(Functions.getHumanDateTime(obj
                        .getString("pdct_pub_date")));
            item.setitemname(obj.getString("pdct_title"));
            item.setItemdescription(obj.getString("pdct_desc"));
            //lets add currency. remove hard codes
            log(obj.getString("unitprice"));
            item.setRetail_price(
                    obj.getString("unitprice"));
            item.setitem_id(obj.getInt("pid"));

            item.setItem_origin(obj.getString("country"));
            item.setItem_town(obj.getString("pdct_town"));
            item.setItem_mobile(obj.getString("contact"));
            item.setItem_owner(obj.getString("pdct_ownwename"));
            item.setimage_name(obj.getString("imagename"));

            try {
                item.setImageidname(item.getimage_name().split(
                        IMAGE_PATH)[1]);
            } catch (Exception ep) {

            }
            item.setMeasurement(obj.getString("pdct_ttl_selling"));
            item.setUserid(obj.getInt("userid"));
            productlist.add(item);
            SokoContext.echo("product size: "+productlist.size());
        }
    } catch (JSONException e) {

        e.printStackTrace();
    }
    return productlist;

}

    public static RealmList<CategoryBean> extractProducCategories(String json){
        RealmList<CategoryBean>productlist=new RealmList<>();
        try {
            //check if there are any items

            HashMap<CategoryBean,String> categories=new HashMap<CategoryBean, String>();
            JSONObject products = new JSONObject(json);
            JSONArray productsArray = products
                    .getJSONArray("products");

            for (int i = 0; i < productsArray.length(); i++) {
                JSONObject obj = productsArray
                        .getJSONObject(i);

                GlobalItem item = new GlobalItem();
                item.setCategory(obj.getString("pdct_category"));
                CategoryBean c=new CategoryBean();
                c.setCategory(item.getCategory());

                categories.put(c, "");



            }
            for(CategoryBean categoryBean: categories.keySet()) {

                CategoryBean cat= new CategoryBean();
                cat.setCategory(categoryBean.getCategory());
                cat.setCategoryType(0);
                productlist.add(cat);
            }



        } catch (JSONException e) {

            e.printStackTrace();
        }
        return productlist;

    }

    public static RealmList<GlobalResource> extractResources(String json) {
    RealmList<GlobalResource> items = new RealmList<GlobalResource>();
    try {
    JSONObject resources_json = new JSONObject(
            json);

    JSONArray resourcesArray = resources_json
            .getJSONArray("resources");




    HashMap<CategoryBean,String> categories=new HashMap<CategoryBean, String>();
    for (int i = 0; i < resourcesArray.length(); i++) {
        JSONObject obj = resourcesArray
                .getJSONObject(i);
        GlobalResource item = new GlobalResource();
        item.setCategory(obj.getString("rscategory"));
        item.setTitle(obj.getString("rstitle"));
        item.setBody(obj.getString("rstext"));
        item.setDateUpdated(obj.getString("rslastupdated"));
        item.setSlug(obj.getString("pdct_ttl_selling"));
        item.setId(obj.getString("bid"));
        CategoryBean c=new CategoryBean();
        c.setCategory(item.getCategory());
        categories.put(c, "");

        items.add(item);
    }
    }catch (Exception e){
        e.printStackTrace();
    }
    return items;
}

    public static RealmList<CategoryBean> extractResourceTypes(String json) {
       RealmList<CategoryBean> items = new RealmList<CategoryBean>();
       HashMap<CategoryBean, String> categories = new HashMap<CategoryBean, String>();
       try {
           JSONObject resources_json = new JSONObject(
                   json);

           JSONArray resourcesArray = resources_json
                   .getJSONArray("resources");


           for (int i = 0; i < resourcesArray.length(); i++) {
               JSONObject obj = resourcesArray
                       .getJSONObject(i);
               GlobalResource item = new GlobalResource();
               item.setCategory(obj.getString("rscategory"));
                CategoryBean c=new CategoryBean();
               c.setCategory(item.getCategory());
               categories.put(c, "");


           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       for (CategoryBean categoryBean : categories.keySet()) {

           CategoryBean cat = new CategoryBean();
           cat.setCategory(categoryBean.getCategory());
           cat.setCategoryType(1);
           items.add(cat);
       }
       return items;
   }

    public static RealmList<EventsBean> extractEvents(String json){
        {
            RealmList<EventsBean> items = new RealmList<EventsBean>();
            try {



                JSONObject eventsjson = new JSONObject(
                        json);

                JSONArray eventsjArr =  eventsjson
                        .getJSONArray("events");

                for (int i = 0; i < eventsjArr.length(); i++) {

                    JSONObject obj = eventsjArr
                            .getJSONObject(i);
                    EventsBean item = new EventsBean();
                    item.setEventid(obj.getInt("evid"));
                    item.setTitle(obj.getString("evtitle"));
                    item.setBrief(obj.getString("evbrief"));
                    item.setStartDate(obj.getString("evstartdate"));
                    item.setEndDate(obj.getString("evenddate"));
                    item.setStartTime(obj.getString("evstarttime"));
                    item.setEndTime(obj.getString("evendtime"));
                    item.setCounty(obj.getString("evcounty"));
                    item.setTown(obj.getString("evtown"));
                    item.setVenue(obj.getString("evvenue"));
                    item.setVipStandCost(obj.getString("evcostvipstand"));
                    item.setStandCost(obj.getString("evcoststand"));
                    item.setVipTicketCost(obj.getString("evticketvvip"));
                    item.setVisitorTicketCost(obj
                            .getString("evticketvisitor"));
                    item.setUserId(obj.getString("usrid"));
                   items.add(item);

                }


            } catch (JSONException e) {

                Log.d("Error ","erro in events >>>"+e.getLocalizedMessage());
            }

            return items;

        }
    }

    public static void log(String message) {
        Log.d("DEBUG", ">>>>" + message);
    }
    public static RealmList<Questions> extractQuizes(String json) {
    RealmList<Questions> items = new RealmList<Questions>();
    return items;
}

    public static void setMobileDataEnabled(Context context, boolean enabled) throws
            ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {

        tryVersionDAta(context);
        final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
        connectivityManagerField.setAccessible(true);
        final Object connectivityManager = connectivityManagerField.get(conman);
        final Class connectivityManagerClass =  Class.forName(connectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        try {
            setMobileDataEnabledMethod.invoke(connectivityManager, enabled);
        } catch (InvocationTargetException e) {
            e.printStackTrace();

        }
    }

    private static void tryVersionDAta(Context c) {
        ConnectivityManager dataManager;
        dataManager  = (ConnectivityManager)c.getSystemService(Context.CONNECTIVITY_SERVICE);
        Method dataMtd = null;
        try {
            dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        dataMtd.setAccessible(true);
        try {
            dataMtd.invoke(dataManager, true);
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static boolean setMobileConnectionEnabled(Context context, boolean enabled)
    {
        try{
            // Requires: android.permission.CHANGE_NETWORK_STATE
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD){
                // pre-Gingerbread sucks!
                final TelephonyManager telMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
                final Method getITelephony = telMgr.getClass().getDeclaredMethod("getITelephony");
                getITelephony.setAccessible(true);
                final Object objITelephony = getITelephony.invoke(telMgr);
                final Method toggleDataConnectivity = objITelephony.getClass()
                        .getDeclaredMethod(enabled ? "enableDataConnectivity" : "disableDataConnectivity");
                toggleDataConnectivity.setAccessible(true);
                toggleDataConnectivity.invoke(objITelephony);
            }
            // Requires: android.permission.CHANGE_NETWORK_STATE
            else if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
                final ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
                // Gingerbread to KitKat inclusive
                final Field serviceField = connMgr.getClass().getDeclaredField("mService");
                serviceField.setAccessible(true);
                final Object connService = serviceField.get(connMgr);
                try{
                    final Method setMobileDataEnabled = connService.getClass()
                            .getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                    setMobileDataEnabled.setAccessible(true);
                    setMobileDataEnabled.invoke(connService, Boolean.valueOf(enabled));
                }
                catch(NoSuchMethodException e){
                    // Support for CyanogenMod 11+
                    final Method setMobileDataEnabled = connService.getClass()
                            .getDeclaredMethod("setMobileDataEnabled", String.class, Boolean.TYPE);
                    setMobileDataEnabled.setAccessible(true);
                    setMobileDataEnabled.invoke(connService, context.getPackageName(), Boolean.valueOf(enabled));
                }
            }
            // Requires: android.permission.MODIFY_PHONE_STATE (System only, here for completions sake)
            else{
                // Lollipop and into the Future!
                final TelephonyManager telMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
                final Method setDataEnabled = telMgr.getClass().getDeclaredMethod("setDataEnabled", Boolean.TYPE);
                setDataEnabled.setAccessible(true);
                setDataEnabled.invoke(telMgr, Boolean.valueOf(enabled));
            }
            return true;
        }
        catch(NoSuchFieldException e){
            Log.e(TAG, "setMobileConnectionEnabled", e);
        }
        catch(IllegalAccessException e){
            Log.e(TAG, "setMobileConnectionEnabled", e);
        }
        catch(IllegalArgumentException e){
            Log.e(TAG, "setMobileConnectionEnabled", e);
        }
        catch(NoSuchMethodException e){
            Log.e(TAG, "setMobileConnectionEnabled", e);
        }
        catch(InvocationTargetException e){
            Log.e(TAG, "setMobileConnectionEnabled", e);
        }
        return false;
    }



    public static void sendIntent(Context context,String subject,String message){
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

// Add data to the intent, the receiving app will decide what to do with it.
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        context.startActivity(Intent.createChooser(intent, "Share Via?"));

    }

    public static void sendEmail(String subject,String message,String receiver){

        //Get the session object
        Properties props = new Properties();
        props.put("mail.smtp.host", "gator3163.hostgator.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                        return new javax.mail.PasswordAuthentication("noreply@wakulimayoung.com" +
                                "", "123456@macharia");

                    }
                });

        //compose
        try {
            MimeMessage mime = new MimeMessage(session);
            mime.setFrom(new InternetAddress("noreply@wakulimayoung.com"));//change accordingly
            mime.addRecipient(MimeMessage.RecipientType.TO,new InternetAddress(receiver));
            mime.setSubject(subject);
            mime.setText(message+getSignature());

            //send message
            Transport.send(mime);

            System.out.println(">>> message sent successfully");

        } catch (Exception e) {
            Log.d("Degub","Error>>>> "+e.getLocalizedMessage());
        }

    }

    private static String getSignature() {
        //toDO get a standard signature for mkulima young
        return "\n\n\nBest Regards,\nMkulimaYoung Team";
    }

    public static ArrayList<String> myListingIds(Realm realm){
        ArrayList<String> mylist=new ArrayList<>();
        if(realm.where(GlobalItem.class).findAll().size()>0)
        for(GlobalItem item:realm.where(GlobalItem.class).equalTo("userid",Integer.valueOf(realm.where
                (GlobalUser
                .class).findFirst().getUserid())).findAll())
            mylist.add(item.getitem_id()+"");
        return mylist;
    }

    public static class MonthYear implements Comparable<MonthYear>, Comparator<MonthYear> {

        private String monthYear;

        public MonthYear(String monthy) {
            this.monthYear = monthy;
        }

        public String getMonthYear() {
            return monthYear;
        }

        public void setMonthYear(String monthYear) {
            this.monthYear = monthYear;
        }

        @Override
        public String toString() {
            return monthYear.toString();
        }

        @Override
        public int compareTo(MonthYear another) {
            return compare(this, another);
        }

        @Override
        public int compare(MonthYear lhs, MonthYear rhs) {
            SimpleDateFormat df = new SimpleDateFormat("MMM yyyy");
            try {
                if (new DateTime(df.parse(lhs.getMonthYear())).isEqual(new DateTime(df.parse(rhs
                        .getMonthYear()))))
                    return 1;
                else return 0;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

}

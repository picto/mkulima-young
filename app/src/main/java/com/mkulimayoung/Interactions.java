/**
 * 
 */
package com.mkulimayoung;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author robb
 * 
 */
public class Interactions extends RealmObject {
    @PrimaryKey
	private int pid;
	private int sms;
    private int rates;
    private RealmList<Comments> comments;

	/**
	 * @return the sms
	 */
	public int getSms() {
		return sms;
	}

	/**
	 * @param sms
	 *            the sms to set
	 */
	public void setSms(int sms) {
		this.sms = sms;
	}

	/**
	 * @return the calls
	 */
	public int getCalls() {
		return calls;
	}

	/**
	 * @param calls
	 *            the calls to set
	 */
	public void setCalls(int calls) {
		this.calls = calls;
	}

	/**
	 * @return the clicks
	 */
	public int getClicks() {
		return clicks;
	}

	/**
	 * @param clicks
	 *            the clicks to set
	 */
	public void setClicks(int clicks) {
		this.clicks = clicks;
	}

	private int calls;
	private int clicks;

	/**
	 * @return the pid
	 */
	public int getPid() {
		return pid;
	}

	/**
	 * @param pid
	 *            the pid to set
	 */
	public void setPid(int pid) {
		this.pid = pid;
	}

    public int getRates() {
        return rates;
    }

    public void setRates(int rates) {
        this.rates = rates;
    }

    public RealmList<Comments> getComments() {
        return comments;
    }

    public void setComments(RealmList<Comments> comments) {
        this.comments = comments;
    }
}

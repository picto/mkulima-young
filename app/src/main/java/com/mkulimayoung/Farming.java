/**
 * 
 */
package com.mkulimayoung;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mkulimayoung.mkulimaApp.R;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * @author robb
 * 
 */
public class Farming extends AppCompatActivity {

    Realm realm;
    ListView listView;
    // protected static final String LIVETSOCK = "animals";
    // protected static final String CROPS = "crops";
	// private String filter = "crops";d
	private android.support.v4.app.FragmentManager fm;
	private FragmentTransaction ft;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.howlist);
        // Open the default realm for the UI thread.
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();

        // Clear the realm from last time
//        Realm.deleteRealm(realmConfiguration);

        // Create a new empty instance of Realm
        realm = Realm.getInstance(realmConfiguration);
        listView = (ListView) findViewById(android.R.id.list);

    }


}

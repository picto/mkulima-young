/**
 * 
 */
package com.mkulimayoung;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.mkulimayoung.mkulimaApp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import database.Database;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.internal.base.BaseCard;
import it.gmariotti.cardslib.library.view.CardViewNative;
import models.Product;
import models.Question;
import models.QuizCard;
import models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import server.ServiceGenerator;
import server.SokoContext;
import server.SokoUsers;
import tools.Configs;

/**
 * @author robb
 * 
 */
public class Listings extends AppCompatActivity {
    private static final int LISTING_REQUESTCODE =900 ;
    public static String IMAGE_PATH = "http://www.mkulimayoung.co.ke/soko_/";
    MarrayAdapter adapter;
    Toolbar toolbar;
    ListView listView;
    private FragmentManager fmanager;
    private android.support.v4.app.FragmentTransaction ft;
    private Realm realm;
    private RealmResults<Product>myitems=null;


    /**
     * @param fmanager
     * @return
     */
    public static Listings newInstance(FragmentManager fmanager) {
        Listings login = new Listings();
        login.fmanager = fmanager;
        return login;
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

    setContentView(R.layout.mlisting);
        realm = Configs.getRealmInstance(this);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Produce");
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (realm.where(User.class).findAll().size()<1) {
            startActivityForResult(new Intent(this, UserAuthenticationActvity.class),LISTING_REQUESTCODE);


        }else{
            updateFields();
        }


	}

    private void updateFields() {
        listView=(ListView)findViewById(android.R.id.list);

        if (realm.allObjects(User.class).size()<1) {
            //not logged in
            Intent t = new Intent(this, UserAuthenticationActvity.class);
            t.putExtra("stage", Actionactivity.MYPRODUCTS);
            startActivity(t);
            finish();
        } else {
            //farmer is a producer. can get alerts when a buyer is looking for products

            adapter = new MarrayAdapter(this, new ArrayList<>(realm.where(Product.class).equalTo
                    ("pdct_ownr",""+realm
                            .allObjects(User.class).first().getId()).findAll()));
            listView.setAdapter(adapter);
            if (adapter.getCount()<1) {
                Functions.showToast("You have no items uploaded",Listings.this);
                finish();
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LISTING_REQUESTCODE) {
            if (resultCode == UserAuthenticationActvity.LOGIN_CODE) {
                updateFields();
            } else {
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

	static class MarrayAdapter extends BaseAdapter {

        private Activity activity;
        private ArrayList<Product> data;//key=>user and value=>payment option
        private static LayoutInflater inflater = null;
        //public ImageLoader imageLoader;
        String mime = "text/html";
        String encoding = "utf-8";
        Date datethis;

        public MarrayAdapter(Activity a,ArrayList<Product> d) {
            activity = a;
            data = d;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //imageLoader = new ImageLoader(activity.getApplicationContext());
        }

        public int getCount() {
            return data.size();
        }

        public Product getItem(int position) {
            return data.get(position);
        }

        public long getItemId(int position) {
            return position;
        }




        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.mylisting_adapter, null);

//            final CardViewNative category = (CardViewNative) vi.findViewById(R.id.quiz);

//        imageSwitcher.setImageDrawable(activity.getResources().getDrawable(R.drawable.logosmall));


            //ImageView tuserImage = (ImageView) vi.findViewById(R.id.listforum_image);

            final Product  question = data.get(position);
            StringBuffer answerSum=new StringBuffer();
           answerSum.append(question.getPdct_town()+"\n");
           answerSum.append(question.getPdct_desc()+"\n");
           answerSum.append(question.getPdct_pub_date()+"\n");

            answerSum.ensureCapacity(3);
            CardViewNative cardViewNative= (CardViewNative) vi.findViewById(R.id.mycard);
            QuizCard livecard = new QuizCard(activity,question.getPdct_title(),answerSum
                    .toString()
                    ,Integer.valueOf(question.getPid()),SokoFragment.IMAGE_PATH+""+question
                    .getPdct_photo
                    ());
            livecard.setCatid(Integer.valueOf(question.getPid()));
            final CardHeader liveheader = new CardHeader(activity);

            liveheader.setPopupMenuPrepareListener(new CardHeader
                    .OnPrepareCardHeaderPopupMenuListener() {
                public boolean onPreparePopupMenu(final BaseCard basecard, PopupMenu popupmenu) {
                    try {

                        popupmenu.getMenu().add(12,2,2,"Sold");
                        popupmenu.getMenu().add(12,1,2,"On Sale");
                        popupmenu.getMenu().add(12,0,2,"Hide");
                        ((QuizCard) basecard).setPopupMenu(popupmenu);

                        popupmenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                Realm realm=Configs.getRealmInstance(activity);
                                realm.beginTransaction();
                                Product product= realm.where(Product.class).equalTo("pid",(
                                       (QuizCard)
                                        basecard)
                                        .getCatid()).findFirst();
                                product.setPdct_status(item.getItemId()+"");
                                realm.commitTransaction();
                                SokoContext.echo("Updating "+product.getPdct_title()+" "+product
                                        .getPid());
                                updateProductText(product,activity);
                                return false;

                            }
                        });


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return true;
                }


            });
            //overflow menu if more than one variant
            liveheader.setButtonOverflowVisible(true);
//        liveheader.setTitle(question.getTitle());
            livecard.addCardHeader(liveheader);
            livecard.setOnClickListener(new Card.OnCardClickListener() {


                public void onClick(Card card, View view) {
                    try {
                        Intent intent=new Intent(activity,Advertise.class);
                        intent.putExtra("update",true);
                        activity.startActivity(intent);
                        mcentral_ton.selectedItem=getItem(position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            });

            cardViewNative.setCard(livecard);

            return vi;
        }

    }
    private static void updateProductText(Product product, final Activity activity) {
        SokoUsers.UpdateProduct client = ServiceGenerator.createService(SokoUsers.UpdateProduct.class);
        //dissociate from realm
        Product product1=new Product();
        product1.setPdct_category(product.getPdct_category());
        product1.setPdct_desc(product.getPdct_desc());
        product1.setPid(product.getPid());
        product1.setPdct_ownr(product.getPdct_ownr());
        product1.setPdct_google_location(product.getPdct_google_location());
        product1.setPdct_town(product.getPdct_town());
        product1.setPdct_photo(product.getPdct_photo());
        product1.setPdct_status(product.getPdct_status());
        product1.setPdct_unitpce(product.getPdct_unitpce());
        product1.setPdct_title(product.getPdct_title());

        final Call<List<Product>> productjson = client.postProductUpdate(product1);
        showProgress("Posting content",activity);
        productjson.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                try {
                    SokoContext.echo("updated product " + response.body());
                    Realm realm=Configs.getRealmInstance(activity);
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(response.body());
                    realm.commitTransaction();
                    dismissMyDialog();
                    showToast("Content saved",activity);


                } catch (Exception e) {
                    dismissMyDialog();
                    e.printStackTrace();
                    showToast("Error processing response " + e.getLocalizedMessage(),activity);
                    SokoContext.echoError("error in server response " + e.getLocalizedMessage
                            ());
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                dismissMyDialog();
                if(TestInternet.isOnline(activity))
                showToast("Error posting content. No internet connection",activity);
                else showToast("Error posting content. try again",activity);
                SokoContext.echoError("Error posting product " + t.getLocalizedMessage());
            }
        });
    }
   static Dialog pDialog;

    private static void showProgress(String message,Activity activity) {
        pDialog = new MaterialDialog.Builder(activity)
                .title("Product Status")
                .content(message)
                .progress(true, 100).build();
        pDialog.setCancelable(false);
        pDialog.show();


    }

    private static void dismissMyDialog() {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }

    private static void showToast(String message,Activity activity) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

}

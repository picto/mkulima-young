/**
 * 
 */
package com.mkulimayoung;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * @author robb
 * 
 */
public class GlobalItem extends RealmObject  {
    @Index
	private String itemname;
    @Index
	private String category;
	private String subcategory;
	private String item_number;
	private String imageidname;
	private String item_origin;
	private String measurement;
    @Index
	private String item_town;
	private String item_owner;
	private String item_mobile;
	private int rating;
	private String pubdate;
	private String itemdescription;// item description
	private String retail_price;// item cost
	private double quantity;// Unit of measure- quantity
	private String image_name;// image name for product
    @PrimaryKey
	private int item_id;
	private int trans_id;
    @Index
	private int userid;
	private int clicks;
	private String views;

	/**
	 * @return the calls
	 */
	public int getCalls() {
		return calls;
	}

	/**
	 * @param calls
	 *            the calls to set
	 */
	public void setCalls(int calls) {
		this.calls = calls;
	}

	/**
	 * @return the messages
	 */
	public int getMessages() {
		return messages;
	}

	/**
	 * @param messages
	 *            the messages to set
	 */
	public void setMessages(int messages) {
		this.messages = messages;
	}

	/**
	 * @return the reports
	 */
	public int getReports() {
		return reports;
	}

	/**
	 * @param reports
	 *            the reports to set
	 */
	public void setReports(int reports) {
		this.reports = reports;
	}

	private int calls;
	private int messages;
	private int reports;
	private int trans_amount;// amount payable to intermediary
	private int status;// available or out-of-stock or awaiting

	public GlobalItem() {

	}

	public String getitemname() {

		return this.itemname;
	}

	public void setitemname(String value) {

		this.itemname = value;
	}

	public int getitem_id() {

		return this.item_id;
	}

	public void setitem_id(int value) {

		this.item_id = value;
	}

	public int gettrans_id() {

		return this.trans_id;
	}

	public void settrans_id(int value) {

		this.trans_id = value;
	}

	public int gettrans_amount() {

		return this.trans_amount;
	}

	public void settrans_amount(int value) {

		this.trans_amount = value;
	}

	public String getimage_name() {

		return this.image_name;
	}

	public void setimage_name(String value) {

		this.image_name = value;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the subcategory
	 */
	public String getSubcategory() {
		return subcategory;
	}

	/**
	 * @param subcategory
	 *            the subcategory to set
	 */
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	/**
	 * @return the item_number
	 */
	public String getItem_number() {
		return item_number;
	}

	/**
	 * @param item_number
	 *            the item_number to set
	 */
	public void setItem_number(String item_number) {
		this.item_number = item_number;
	}

	/**
	 * @return the quantity
	 */
	public double getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the itemdescription
	 */
	public String getItemdescription() {
		return itemdescription;
	}

	/**
	 * @param itemdescription
	 *            the itemdescription to set
	 */
	public void setItemdescription(String itemdescription) {
		this.itemdescription = itemdescription;
	}

	/**
	 * @return the retail_price
	 */
	public String getRetail_price() {
		return retail_price;
	}

	/**
	 * @param retail_price
	 *            the retail_price to set
	 */
	public void setRetail_price(String retail_price) {
		this.retail_price = retail_price;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the item_origin
	 */
	public String getItem_origin() {
		return item_origin;
	}

	/**
	 * @param item_origin
	 *            the item_origin to set
	 */
	public void setItem_origin(String item_origin) {
		this.item_origin = item_origin;
	}

	/**
	 * @return the item_town
	 */
	public String getItem_town() {
		return item_town;
	}

	/**
	 * @param item_town
	 *            the item_town to set
	 */
	public void setItem_town(String item_town) {
		this.item_town = item_town;
	}

	/**
	 * @return the item_owner
	 */
	public String getItem_owner() {
		return item_owner;
	}

	/**
	 * @param item_owner
	 *            the item_owner to set
	 */
	public void setItem_owner(String item_owner) {
		this.item_owner = item_owner;
	}

	/**
	 * @return the item_mobile
	 */
	public String getItem_mobile() {
		return item_mobile;
	}

	/**
	 * @param item_mobile
	 *            the item_mobile to set
	 */
	public void setItem_mobile(String item_mobile) {
		this.item_mobile = item_mobile;
	}

	/**
	 * @return the measurement
	 */
	public String getMeasurement() {
		return measurement;
	}



	/**
	 * @param measurement
	 *            the measurement to set
	 */
	public void setMeasurement(String measurement) {
		this.measurement = measurement;
	}


	/**
	 * @return the userid
	 */
	public int getUserid() {
		return userid;
	}

	/**
	 * @param userid
	 *            the userid to set
	 */
	public void setUserid(int userid) {
		this.userid = userid;
	}

	/**
	 * @return the rating
	 */
	public int getRating() {
		return rating;
	}

	/**
	 * @param rating
	 *            the rating to set
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}

	/**
	 * @return the pubdate
	 */
	public String getPubdate() {
		return pubdate;
	}

	/**
	 * @param pubdate
	 *            the pubdate to set
	 */
	public void setPubdate(String pubdate) {
		this.pubdate = pubdate;
	}

	/**
	 * @return the imageidname
	 */
	public String getImageidname() {
		return imageidname;
	}

	/**
	 * @param imageidname
	 *            the imageidname to set
	 */
	public void setImageidname(String imageidname) {
		this.imageidname = imageidname;
	}

	/**
	 * @return the clicks
	 */
	public int getClicks() {
		return clicks;
	}

	/**
	 * @param clicks
	 *            the clicks to set
	 */
	public void setClicks(int clicks) {
		this.clicks = clicks;
	}

	/**
	 * @return the views
	 */
	public String getViews() {
		return views;
	}

	/**
	 * @param views
	 *            the views to set
	 */
	public void setViews(String views) {
		this.views = views;
	}

}

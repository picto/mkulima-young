package com.mkulimayoung;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.Menu;
import android.view.MenuInflater;

import com.mkulimayoung.mkulimaApp.R;

/**
 * Created by robertnyangate on 9/22/15.
 */
public class MkulimaYoungActs extends AppCompatActivity {
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sokoni);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new MkulimaFragmentsAdapter(getSupportFragmentManager(),
                MkulimaYoungActs.this));

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private class MkulimaFragmentsAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 3;
        private String tabTitles[] = new String[] { "Soko", "Resources", "Events","Forums" };
        private Context context;
        private int[] imageResId = {
                android.R.drawable.ic_menu_recent_history,
                android.R.drawable.ic_menu_recent_history,
                android.R.drawable.ic_menu_recent_history,
                android.R.drawable.ic_menu_recent_history
        };


        public MkulimaFragmentsAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public Fragment getItem(int i) {
           if(tabTitles[i].equalsIgnoreCase("Soko")){
               return SokoFragment.newInstance(MkulimaYoungActs.this.getSupportFragmentManager());
           }else if(tabTitles[i].equalsIgnoreCase("Resources")){
               return InsideFarm.newInstance(MkulimaYoungActs.this.getSupportFragmentManager());
           }else if(tabTitles[i].equalsIgnoreCase("Events")){
               return InsideFarm.newInstance(MkulimaYoungActs.this.getSupportFragmentManager());
           }else return null;
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            //return tabTitles[position];
            Drawable image = context.getResources().getDrawable(imageResId[position]);
            image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
            // Replace blank spaces with image icon
            SpannableString sb = new SpannableString("   " + tabTitles[position]);
            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
            sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return sb;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        // searchView.setIconifiedByDefault(false);

        return super.onCreateOptionsMenu(menu);

    }
}

package com.mkulimayoung;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

public class DrawerAtadApter extends ArrayAdapter<NavDrawerItem> {

	private final Activity activity;
	// private final ArrayList<HashMap<String, String>> data;
	private final ArrayList<NavDrawerItem> data;
	private static LayoutInflater inflater = null;
	public ImageLoaderRound imageLoader;
	String mime = "text/html";
	String encoding = "utf-8";

	// public DrawerAtadApter(Activity a, ArrayList<HashMap<String, String>> d)
	// {
	// activity = a;
	// data = d;
	// inflater = (LayoutInflater) activity
	// .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	// imageLoader = new ImageLoaderRound(activity.getApplicationContext());
	// }
	public DrawerAtadApter(Context context, ArrayList<NavDrawerItem> data) {
		super(context, R.layout.drawer_list_item, data);
		this.activity = (Activity) context;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public NavDrawerItem getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = inflater.inflate(R.layout.drawer_list_item, parent, false);

		TextView titlepost = (TextView) vi.findViewById(R.id.text1); // title
																		// of
		TextView Expltxt = (TextView) vi.findViewById(R.id.artistforum);
		ImageView myImageView = (ImageView) vi.findViewById(R.id.icon);

		// ImageView thumb_image = (ImageView) vi
		// .findViewById(R.id.listforum_image); // thumb image

		NavDrawerItem menuItem = data.get(position);
		myImageView.setImageResource(menuItem.getIcon());
		titlepost.setText(menuItem.getTitle());
		Expltxt.setHint(menuItem.getSubTitle());

		if (titlepost.getText().toString().equalsIgnoreCase("Dashboard")) {
			// imageLoader.DisplayImage("http://192.168.1.46/myshop/bag.jpg",
			// myImageView);
		}

		// vi.setFitsSystemWindows(true);

		return vi;

	}

	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		int targetWidth = 50;
		int targetHeight = 50;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
				Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);
		return targetBitmap;
	}
}
package com.mkulimayoung;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.mkulimayoung.mkulimaApp.R;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import tools.Configs;

public class MainActivity extends AppCompatActivity {

//    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
//    private static final String TWITTER_KEY = "LJeoFlIr7x9JgwF1RT0DWdGuz";
//    private static final String TWITTER_SECRET = "5u5wwQfc6Hq8aMlD6S5AKmrZYebY0k3cHHRLm2CuTdeUILOK9e";


    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "LJeoFlIr7x9JgwF1RT0DWdGuz";
    private static final String TWITTER_SECRET = "5u5wwQfc6Hq8aMlD6S5AKmrZYebY0k3cHHRLm2CuTdeUILOK9e";

	// Note: Your consumer key and secret should be obfuscated in your source code before shipping.
	// Button exploreBtn;
	// // Splash s creen timer
	private static int SPLASH_TIME_OUT = 1000;
	// public static int run_time = 0;
	// private TextView counter;
	// @Override
	// protected void onCreate(Bundle savedInstanceState) {
	// super.onCreate(savedInstanceState);
	// setContentView(R.layout.activity_main);
	// counter=(TextView)findViewById(R.id.counter);
	// new Handler().postDelayed(new Runnable() {
	//
	// /*
	// * Showing splash screen with a timer. This will be useful when you
	// * want to show case your app logo / company
	// */
	//
	// @Override
	// public void run() {
	// // This method will be executed once the timer is over
	// // Start your app main activity
	//
	// }
	//
	// }, SPLASH_TIME_OUT);
	// }
	Handler handler;
	Handler handler2, handler3;
    Realm realm;
    //Realm realm;
    //private ProgressBar mSwitcher;
    private TextView conyounfarms, welcome;
    private ImageView logo;

    @Override
    public void onPostCreate(Bundle savedInstanceState,
                             PersistableBundle persistentState) {

        super.onPostCreate(savedInstanceState, persistentState);

    }
   final static int MESSAGE=1;
   final static int PRODUCT=2;
   final static int NEWS=3;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        int type=getIntent().getIntExtra("mtype",1);
        switch (type){
            case MESSAGE:
                showMDialog();
                break;
            case PRODUCT:
                showProduct();
                break;
            default:
                showMDialog();
        }
	}

    private void showProduct() {
     Intent intent=new Intent(MainActivity.this,ItemView.class);
        intent.putExtra("notif",getIntent().getIntExtra("productid",0));
        startActivity(intent);
        finish();
    }

    private void showMDialog() {
        MaterialDialog.Builder dialog=new MaterialDialog.Builder(this);
        dialog.content(getIntent().getStringExtra("message"));
        dialog.title(getIntent().getStringExtra("title"));
        dialog.positiveText("Okay");
        dialog.show();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}

/**
 *
 */
package com.mkulimayoung;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.mkulimayoung.mkulimaApp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import database.Database;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import models.Product;
import models.User;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit.mime.TypedFile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import server.ServiceGenerator;
import server.SokoContext;
import server.SokoUsers;
import tools.Configs;

/**
 * @author robertnyangate
 */
public class Advertise extends AppCompatActivity {
    // Toolbar toolbar;
    public final static String[] statuses = new String[]{"ON SALE", "SOLD", "BOOKED"};
    private static final int POST_REQUESTCODE = 500;
    private static final int myNotifId = 20001;
    private static final int REQCODE = 1;
    public static String UPDATE_INSTANCE = "update";
    private static int maxWidth = 300;
    private static int maxHeight = 300;
    EditText title;
    EditText description;
    EditText unitprice;
    boolean isUpdate;
    EditText town;
    EditText estate;
    Spinner country;
    Spinner categoryspinner;
    CheckBox status;
    Button submit;
    ImageLoader imageLoader;
    String productname, productprice;
    Bundle savedInst;
    long totalSize = 0;
    boolean uploadedwell;
    MaterialDialog pb = null;
    SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf1 = new SimpleDateFormat("EEE , d MMM , yyyy");
    private FragmentManager fmanager;
    private android.support.v4.app.FragmentTransaction ft;
    private Realm realm;
    private int pid;
    private Toolbar toolbar;
    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;
    private String SELECTED_EXPIRY = null;

    public static Bitmap resize(String path) {
// create the options
        BitmapFactory.Options opts = new BitmapFactory.Options();

//just decode the file
        opts.inJustDecodeBounds = true;
        Bitmap bp = BitmapFactory.decodeFile(path, opts);

//get the original size
        int orignalHeight = opts.outHeight;
        int orignalWidth = opts.outWidth;
//initialization of the scale
        int resizeScale = 2;
//get the good scale

        if (orignalWidth > maxWidth || orignalHeight > maxHeight) {
            final int heightRatio = Math.round((float) orignalHeight / (float) maxHeight);
            final int widthRatio = Math.round((float) orignalWidth / (float) maxWidth);
            resizeScale = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
//put the scale instruction (1 -> scale to (1/1); 8-> scale to 1/8)
        opts.inSampleSize = resizeScale;
        opts.inJustDecodeBounds = false;
//get the futur size of the bitmap
        int bmSize = (orignalWidth / resizeScale) * (orignalHeight / resizeScale) * 4;
//check if it's possible to store into the vm java the picture
        if (Runtime.getRuntime().freeMemory() > bmSize) {
//decode the file
            bp = BitmapFactory.decodeFile(path, opts);
        } else
            return null;
        return bp;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.listing);


        realm = Configs.getRealmInstance(this);
        imageLoader = new ImageLoader(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("New Item");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        savedInst = savedInstanceState;
        mixpanel = Configs.getMixpanel(this);
        if (realm.where(User.class).findAll().size() < 1) {
            startActivityForResult(new Intent(this, UserAuthenticationActvity.class), POST_REQUESTCODE);


        } else {
            updateFields();
        }


    }

    private void updateFields() {
        fmanager = getSupportFragmentManager();
        submit = (Button) findViewById(R.id.listing_ok);
        title = (EditText) findViewById(R.id.product_title);
        description = (EditText) findViewById(R.id.product_desc);
        unitprice = (EditText) findViewById(R.id.product_price);
        town = (EditText) findViewById(R.id.place_list);
        country = (Spinner) findViewById(R.id.country_spinner_lis);
        country.setSelection(10);
        unitprice.setHint("price in KES");
        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                unitprice.setHint("price in " + getResources().getStringArray(R.array
                        .countrycurrency)[country.getSelectedItemPosition()]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                unitprice.setHint("price in " + getResources().getStringArray(R.array
                        .countrycurrency)[10]);
            }
        });
        categoryspinner = (Spinner) findViewById(R.id.category_spinner);
        status = (CheckBox) findViewById(R.id.statusspi);


        status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                status.setChecked(isChecked);
            }
        });

        findViewById(R.id.changephoto).setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        selectPhoto();
                    }
                });
        submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(title.getText().toString()))
                    title.setError("Invalid Title");
                else if (TextUtils.isEmpty(description.getText().toString()))
                    description.setError("Invalid Description");
                else if (TextUtils.isEmpty(unitprice.getText().toString()))
                    unitprice.setError("Invalid UnitPrice");
                else if (TextUtils.isEmpty(town.getText().toString()))
                    town.setError("Invalid Town");
                else if (mcentral_ton.photoname == null)
                    Functions.showSnackBar(description, "No image selected");
                else if (!TestInternet.isOnline(Advertise.this)) {
                    Functions.showSnackBar(description, "No Internet Connection");

                } else
                    proceedUpload();

            }
        });
        if (getIntent().hasExtra(UPDATE_INSTANCE)) {
            populateFields();
            imageLoader.DisplayImage(SokoFragment.IMAGE_PATH + mcentral_ton.selectedItem
                            .getPdct_photo(),
                    ((ImageView) findViewById(R.id.preview)));
            mcentral_ton.photoname = mcentral_ton.selectedItem.getPdct_photo();
            isUpdate = true;
        } else if (mcentral_ton.photoURl == null) {
            selectPhoto();
            isUpdate = false;
        } else {
            setPhoto();
        }
    }

    MixpanelAPI mixpanel;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        mixpanel.flush();
    }

    private boolean photoChanged;

    /**
     *
     */
    private void setPhoto() {

        File imgFile = new File(mcentral_ton.photoURl);

        if (imgFile.exists()) {

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                    .getAbsolutePath());
            ImageView myImage = (ImageView) findViewById(R.id.preview);
            myImage.setImageBitmap(myBitmap);

        }
        if (isUpdate)
            photoChanged = true;

    }

    /**
     *
     */
    private void populateFields() {
        Product product = mcentral_ton.selectedItem;
        status.setChecked(Integer.valueOf(product.getPdct_status()) > 0);
        title.setText(product.getPdct_title());
        title.setEnabled(false);
        unitprice.setText(product.getPdct_unitpce());
        town.setText(product.getPdct_town());
        description.setText(product.getPdct_desc());

    }

    /**
     *
     */
    protected void proceedUpload() {
        if (!isUpdate) {
            //new product
            uploadFile(mcentral_ton.photoURl, mcentral_ton.photoname);
        } else if (isUpdate && photoChanged) {//photo changed
            uploadFile(mcentral_ton.photoURl, mcentral_ton.photoname);
        } else {//just update content

            processContent();
        }

    }

    private int getStatus(String s) {
        if (statuses[0].equalsIgnoreCase(s))
            return 1;
        if (statuses[1].equalsIgnoreCase(s))
            return 2;
        if (statuses[2].equalsIgnoreCase(s))
            return 3;
        else return 0;

    }

    public void selectDate(View view, int dateFieldID) {
        DialogFragment newFragment = new SelectDateFragment(dateFieldID);
        newFragment.show(getSupportFragmentManager(), "DatePicker");
    }

    public void populateSetDate(int year, int month, int day, int dateFieldID) {

        // datethis = (Date) sdf2.parse(day+"-"+month+"-"+year);
        SELECTED_EXPIRY = (year + "-" + month + "-" + day);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQCODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImage = data.getData();
                try {
                    String filePath = getPath(selectedImage);
                    String file_extn = filePath.substring(filePath
                            .lastIndexOf(".") + 1); //$NON-NLS-1$

                    if (file_extn.equals("img") || file_extn.equals("jpg") //$NON-NLS-1$ //$NON-NLS-2$
                            || file_extn.equals("png") //$NON-NLS-1$
                            || file_extn.equals("jpeg") //$NON-NLS-1$
                            || file_extn.equals("gif")) { //$NON-NLS-1$
                        if (Functions.isValidSize(filePath)) {
                            mcentral_ton.photoURl = filePath;
                            mcentral_ton.photoname = filePath
                                    .substring(filePath.lastIndexOf("/") + 1);
//                            Functions.showSnackBar(description, "Nice Photo ");
                            setPhoto();

                        } else {
                            Functions.showSnackBar(description, "Invalid photo size");
                        }

                    } else {

                    }

                } catch (Exception ep) {
                    Log.d("s", ">>>>>>>Error " + ep.getLocalizedMessage());
                }
            }
        } else if (requestCode == POST_REQUESTCODE) {
            if (resultCode == UserAuthenticationActvity.LOGIN_CODE) {

                updateFields();
            } else {
                finish();
            }
        }
    }

    // method to start activity for selecting photo to upload.
    public void selectPhoto() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*"); //$NON-NLS-1$
        startActivityForResult(photoPickerIntent, REQCODE);
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaColumns.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private String getName(Uri uri) {
        String[] projection = {MediaColumns.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaColumns.DISPLAY_NAME);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void cleanUp(String result) {
        Functions.showSnackBar(description, result);
        Intent update = new Intent(Advertise.this, Actionactivity.class);
        update.putExtra("stage", 0);
        update.putExtra("update", true);
        startActivity(update);
        mcentral_ton.photoname = null;
        mcentral_ton.photoURl = null;
        Advertise.this.finish();
    }

    @SuppressLint("ValidFragment")
    public class SelectDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        int dateFieldID;

        public SelectDateFragment(int dateFieldID) {
            this.dateFieldID = dateFieldID;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        @Override
        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd, dateFieldID);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {

            super.onDismiss(dialog);
        }
    }

    private void sendConfirmEmail(final String responseString) {
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                Realm realm = Configs.getRealmInstance(Advertise.this);
                if (responseString.contains("sorry") || responseString.contains("no")) {
                    Functions.sendEmail("MkulimaMarket", "Hello " + realm.where(User.class)
                            .findFirst().getName() + ",\nYou have successfully posted your " +
                            "produce" +
                            " " +
                            "on MkulimaYoung platform. However the product image failed to upload" +
                            ". Please navigate to My Produce drawer-menu-option on MkulimaApp to " +
                            "update your product with an image." +
                            "" +
                            ".\n\nThank you" +
                            " " +
                            "for connecting.", realm.where(User.class).findFirst().getEmail());
                } else {

                    if (!isUpdate)
                        Functions.sendEmail("MkulimaMarket", "Hello " + realm.where(User.class)
                                .findFirst().getName() + ",\nYou have successfully posted your " +
                                "produce" +
                                " " +
                                "on MkulimaYoung platform. We hope you get a buyer soon.\n\nThank you" +
                                " " +
                                "for connecting.", realm.where(User.class).findFirst().getEmail());
                    realm.close();
                }
                return null;
            }
        }.execute("");
    }

    private void trackEvent(String itemname, String town, String country, String price, int
            productid) {
        try {
            JSONObject props = new JSONObject();
            props.put("email", realm.allObjects(User.class).first().getEmail());
            props.put("product", itemname);
            props.put("town", town);
            props.put("productid", productid);
            props.put("country", country);
            props.put("price", price);
            props.put("time", new Date().toString());
            mixpanel.track("SellerEvent", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
        trackRegion(itemname, town, country);
    }

    private void trackRegion(String itemname, String town, String country) {
        try {
            JSONObject props = new JSONObject();
            props.put("product", itemname);
            props.put("town", town);
            props.put("country", country);
            props.put("time", new Date().toString());
            mixpanel.track("RegionBuyerCrops", props);
        } catch (JSONException e) {
            Log.e("MY", "Unable to add properties to JSONObject", e);
        }
    }

    Dialog pDialog;

    private void showProgress(String message) {
        pDialog = new MaterialDialog.Builder(this)
                .title("Product Status")
                .content(message)
                .progress(true, 100).build();
        pDialog.setCancelable(false);
        pDialog.show();


    }

    private void dismissMyDialog() {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private boolean contentUsed;

    private void uploadFile(String filepath, String imagename) {
        // create upload service client
        showProgress("Uploading image");
        SokoUsers.FileUploadService service =
                ServiceGenerator.createUploadService(SokoUsers.FileUploadService.class);
        Bitmap resized = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(filepath), 300,
                300, true);

        // use the FileUtils to get the actual file by uri
        File file = new File(filepath);

        try {
            file.createNewFile();
            FileOutputStream ostream = new FileOutputStream(file);
            resized.compress(Bitmap.CompressFormat.PNG, 100, ostream);
            ostream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("fileToUpload", file.getName(), requestFile);

        // add another part within the multipart request
        String descriptionString = imagename;
        RequestBody description =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), descriptionString);

        // finally, execute the request
        Call<ResponseBody> call = service.upload(description, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                dismissMyDialog();
                try {
                    processContent();
                    Log.d("POSTING_DEBUG"," posted " + response.body().string());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
                dismissMyDialog();
                if(!TestInternet.isOnline(Advertise.this))
                    showToast("Error posting content. No internet connection "+t.getLocalizedMessage());
                else showToast("Error posting content. try again");
            }
        });
    }

    private void processContent() {

        Product product = new Product();
        product.setPdct_category(categoryspinner.getSelectedItem().toString());
        product.setPdct_town(town.getText().toString());
        product.setPdct_google_location(country.getSelectedItem().toString());
        product.setPdct_ownr("" + realm.where(User.class).findFirst
                ().getId());
        product.setPdct_photo(mcentral_ton.photoname);
        product.setPdct_pub_date(sdf3.format(new Date()));
        product.setPdct_title(title.getText().toString());
        product.setPdct_ttl_selling(product.getPdct_title());
        product.setPdct_desc(description.getText().toString());
        product.setPdct_unitpce(unitprice.getText().toString());
        product.setPdct_status("" + (status.isChecked() ? 1 : 0));
        productname = title.getText().toString();
        productprice = unitprice.getText().toString();
        if (!isUpdate) {
            postProductTexts(product);
        } else {

            product.setPid(mcentral_ton.selectedItem.getPid());
            updateProductText(product);
        }
    }

    private void updateProductText(Product product) {
        SokoUsers.UpdateProduct client = ServiceGenerator.createService(SokoUsers.UpdateProduct.class);
        final Call<List<Product>> productjson = client.postProductUpdate(product);
        showProgress("Posting content");
        productjson.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                try {
                    SokoContext.echo("updated product " + response.body());
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(response.body());
                    realm.commitTransaction();
                    dismissMyDialog();
                    showToast("Content saved");


                } catch (Exception e) {
                    dismissMyDialog();
                    e.printStackTrace();
                    showToast("Error processing server response ");
                    SokoContext.echoError("error in server response " + e.getLocalizedMessage
                            ());
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                dismissMyDialog();
                if(!TestInternet.isOnline(Advertise.this))
                    showToast("Error posting content. No internet connection");
                else showToast("Error posting content. try again "+t.getLocalizedMessage());
                SokoContext.echoError("Error posting product " + t.getLocalizedMessage());
            }
        });
    }

    private void postProductTexts(Product product) {
        SokoUsers.PostProduct client = ServiceGenerator.createService(SokoUsers.PostProduct.class);
        final Call<List<Product>> productjson = client.postProduct(product);
        SokoContext.echo("received product data. updating profile");
        showProgress("Posting content..");

        productjson.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                try {

                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(response.body());
                    realm.commitTransaction();
                    contentUsed = true;
                    dismissMyDialog();
                    SokoContext.echo("received upload response " + response.body().get(0).getPdct_title());
                    showToast("Content uploaded");
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    dismissMyDialog();
                    if(TestInternet.isOnline(Advertise.this))
                    showToast("Server Error posting content,try later!");
                    else showToast("Connection timeout. Restart internet connection");
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                SokoContext.echoError("Error posting product " + t.getLocalizedMessage());
                dismissMyDialog();
                if(!TestInternet.isOnline(Advertise.this))
                    showToast("Error posting content. No internet connection");
                else showToast("Error posting content. try again "+t.getLocalizedMessage());
            }
        });
    }
}

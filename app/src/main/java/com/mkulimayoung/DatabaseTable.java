package com.mkulimayoung;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;

import database.Database;
import io.realm.Realm;
import models.Product;
import tools.Configs;

/**
 * Created by robertnyangate on 11/6/15.
 */
public class DatabaseTable {

    //The columns we'll include in the dictionary table
    public static final String COL_ID = "_id";
    public static final String COL_NAME = "Name";
    public static final String COL_PRICE = "Price";
    public static final String COL_LOCATION = "Location";
    public static final String COL_IMAGE = "Imagename";
    public static final String COL_DATA = "loc_and_name";
    private static final String TAG = "ProductsDatabase";
    private static final String DATABASE_NAME = "DICTIONARY";
    private static final String FTS_VIRTUAL_TABLE = "FTS";
    private static final int DATABASE_VERSION = 1;

    private final DatabaseOpenHelper mDatabaseOpenHelper;
    private HashMap<String, String> mAliasMap;

    public DatabaseTable(Context context) {
        mDatabaseOpenHelper = new DatabaseOpenHelper(context);
    }

    public Cursor getWordMatches(String query, String[] columns) {
        String selection = COL_DATA+" MATCH ?";
        String[] selectionArgs = new String[]{query + "*"};

        return query(selection, selectionArgs, columns);
    }

    public Cursor getWordMatches(String[] selectionArgs, String[] columns, DatabaseTable db) {
        String selection = "_ID, Price,_id,Name  as " +
                SearchManager.SUGGEST_COLUMN_TEXT_1 + " and Location as "
                + SearchManager.SUGGEST_COLUMN_TEXT_2 + "  and " + COL_NAME + " MATCH ? ";

        return getWordMatches(selectionArgs[0], new String[]{"_id", "Name","Imagename", "Price", "Location",
                "Name" +
                " as " +
                "" + SearchManager
                .SUGGEST_COLUMN_TEXT_1, "Location || ' ' || Price as " +
                "" + SearchManager
                .SUGGEST_COLUMN_TEXT_2, "Imagename as " + SearchManager.SUGGEST_COLUMN_ICON_1,
                "_id as " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID});
    }

    private Cursor query(String selection, String[] selectionArgs, String[] columns) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(FTS_VIRTUAL_TABLE);

        Cursor cursor = builder.query(mDatabaseOpenHelper.getReadableDatabase(),
                columns, selection, selectionArgs, null, null, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;
    }

    private static class DatabaseOpenHelper extends SQLiteOpenHelper {

        private static final String FTS_TABLE_CREATE =
                "CREATE VIRTUAL TABLE " + FTS_VIRTUAL_TABLE +
                        " USING fts3 (" +
                        COL_NAME + ", " +
                        COL_ID + ", " +
                        COL_LOCATION+ "," +
                        COL_IMAGE + "," +
                        COL_DATA + "," +
                        COL_PRICE + ")";
        private final Context mHelperContext;
        private SQLiteDatabase mDatabase;

        DatabaseOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            mHelperContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            mDatabase = db;
            mDatabase.execSQL(FTS_TABLE_CREATE);
            loadDictionary();
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + FTS_VIRTUAL_TABLE);
            onCreate(db);
        }

        private void loadDictionary() {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        loadWords();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }).start();
        }

        private void loadWords() throws IOException {
            // Create a new empty instance of Realm
            Database database=new Database(mHelperContext);
            database.open();
            Log.d("VIRTUALDEBUG", "adding products to vDB>>>>>");
            Realm realm= Configs.getRealmInstance(mHelperContext);
            for(Product item:realm.allObjects(Product.class)){
                addWord(item.getPdct_title(), item.getPdct_unitpce(), item.getPdct_town(),
                        Integer.valueOf(item
                        .getPid()), item.getPdct_photo(),item.getPdct_desc());
//                Log.d("VIRTUALDEBUG","added>>>>>"+item.getName());
            }

            database.close();
        }

        public long addWord(String name, String price, String location, int pid, String
                imagename,String description) {
            ContentValues initialValues = new ContentValues();
            initialValues.put(COL_NAME, name);
            initialValues.put(COL_PRICE, price);
            initialValues.put(COL_LOCATION, location);
            initialValues.put(COL_ID, pid);
            initialValues.put(COL_DATA,name+" "+location+" "+description);
            initialValues.put(COL_IMAGE,  imagename);

            return mDatabase.insert(FTS_VIRTUAL_TABLE, null, initialValues);
        }
    }
}

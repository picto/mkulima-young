/**
 * 
 */
package com.mkulimayoung;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mkulimayoung.mkulimaApp.R;

/**
 * @author robb
 * 
 */
public class EventsAdapter extends ArrayAdapter<EventsBean> {
	private final LayoutInflater inflator;
	private final Context context;
	EventHolder holder = null;
	private final List<EventsBean> events;

	/**
	 * @param context
	 * @param resource
	 * @param objects
	 */
	public EventsAdapter(Context context, int resource, List<EventsBean> objects) {
		super(context, resource, objects);
		inflator = ((Activity) context).getLayoutInflater();
		this.context = context;
		events = objects;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = inflator.inflate(R.layout.events_layout, parent,
					false);
			holder = new EventHolder();
			holder.brief = (TextView) convertView.findViewById(R.id.brief);
			holder.eventTitle = (TextView) convertView.findViewById(R.id.title);
			holder.startDate = (TextView) convertView
					.findViewById(R.id.start_date);
			holder.via = (TextView) convertView.findViewById(R.id.via);
			holder.endDate = (TextView) convertView.findViewById(R.id.end_date);
			holder.startTime = (TextView) convertView
					.findViewById(R.id.start_time);
			holder.endTime = (TextView) convertView.findViewById(R.id.end_time);
			holder.venue = (TextView) convertView.findViewById(R.id.venue);
			holder.county = (TextView) convertView.findViewById(R.id.county);
			holder.town = (TextView) convertView.findViewById(R.id.town);
			holder.brief = (TextView) convertView.findViewById(R.id.brief);
			holder.brief.setVisibility(View.VISIBLE);
			holder.venue.setVisibility(View.VISIBLE);
			holder.county.setVisibility(View.VISIBLE);

			holder.ly = (LinearLayout) convertView.findViewById(R.id.end);
			convertView.setTag(holder);

		} else {
			holder = (EventHolder) convertView.getTag();

		}
		holder.town.setHint(events.get(position).getTown());
		holder.eventTitle.setHint(events.get(position).getTitle());
		holder.brief.setHint(events.get(position).getBrief());

		holder.venue.setHint(events.get(position).getVenue());
		holder.county.setHint(events.get(position).getCounty());
		if (events.get(position).getStartDate()
				.equalsIgnoreCase(events.get(position).getEndDate())) {
			holder.startDate.setHint(Functions.getHumanDate(events
					.get(position).getStartDate()));
		} else {
			holder.startDate.setHint("From "
					+ Functions.getHumanDate(events.get(position)
							.getStartDate()));
			holder.via.setHint(" TO ");
			holder.endDate.setHint(Functions.getHumanDate(events.get(position)
					.getEndDate()));
		}
		holder.startTime.setHint(events.get(position).getStartTime());
		holder.endTime.setHint(events.get(position).getEndTime());

		return convertView;
	}

	static class EventHolder {
		TextView eventTitle, brief, venue, county, town;
		TextView startDate, endDate, via;
		TextView startTime, endTime;
		LinearLayout ly;

	}

	public void showContent(int pos, AdapterView<?> parent, View v) {
		getView(pos, v, parent);

		if (holder.ly.getVisibility() == View.VISIBLE)
			holder.ly.setVisibility(View.GONE);
		else {
			holder.ly.setVisibility(View.VISIBLE);
			holder.ly.requestFocus();
		}
	}
}

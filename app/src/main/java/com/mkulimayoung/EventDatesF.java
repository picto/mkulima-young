/**
 * 
 */
package com.mkulimayoung;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.mkulimayoung.mkulimaApp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author robb
 * 
 */
public class EventDatesF extends ListFragment {
    ExpandableListView expandableListView;
    MyExpandableListViewAdapter expandableListAdapter;
    List expandableListTitle;
    HashMap<String,List<EventsBean>> expandableListDetail;
    private FragmentManager fmanager;
    private android.support.v4.app.FragmentTransaction ft;
    private int mStackLevel;

    public static EventDatesF newInstance(FragmentManager fm) {
        EventDatesF sp = new EventDatesF();
        sp.fmanager = fm;
        return sp;
    }

	/**
	 * @param string
	 */
	private void showToast(String string) {
		Toast.makeText(getActivity(), string, Toast.LENGTH_LONG).show();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null)
			return null;
		View v = inflater.inflate(R.layout.newhow_list, container, false);
        expandableListView = (ExpandableListView) v.findViewById(R.id.expandableListView);
        expandableListDetail = ExpandableListDataPump.getData();
        expandableListTitle = new ArrayList(expandableListDetail.keySet());
        expandableListAdapter = new MyExpandableListViewAdapter(getContext(),expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {


            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                showDialog(groupPosition, childPosition);
                return false;

            }
        });
		return v;
	}

    void showDialog(int groupPosition,int childPosition) {
        mStackLevel++;

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = Events_Frag.newInstance(fmanager,
                expandableListAdapter.getChild
                        (groupPosition, childPosition));
        newFragment.show(ft, "dialog");
    }

    @Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		//super.onViewCreated(view, savedInstanceState);
		//showDates();
	}

    static class ExpandableListDataPump {
        public static HashMap<String,List<EventsBean>> getData() {
            HashMap<String,List<EventsBean>> expandableListDetail = new HashMap<>();
            for(Functions.MonthYear date:mcentral_ton.common_events.keySet()){
                List dateItem = new ArrayList();
                dateItem.addAll(mcentral_ton.common_events.get(date));
                expandableListDetail.put(date.toString(), dateItem);
            }

            return expandableListDetail;
        }
    }
}

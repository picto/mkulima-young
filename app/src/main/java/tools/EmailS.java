package tools;

import android.util.Patterns;

/**
 * Created by robertnyangate on 3/30/16.
 */
public class EmailS {
    public static boolean validEmail(String email)
    {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}

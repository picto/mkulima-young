package tools;

import android.content.Context;

import com.firebase.client.Firebase;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by robertnyangate on 3/20/16.
 */
public class Configs {
    static String MIXPANEL_PROJECT_TOKEN="828794db8b179593cba55f9aaa03a60b";

    public static Realm getRealmInstance(Context c){
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(c)
                .deleteRealmIfMigrationNeeded().
                        build();

        Realm realm = Realm.getInstance(realmConfiguration);

        return realm;
    }
    public static MixpanelAPI getMixpanel(Context context){
        String projectToken = MIXPANEL_PROJECT_TOKEN; // e.g.: "1ef7e30d2a58d27f4b90c42e31d6d7ad"
      MixpanelAPI  mixpanel = MixpanelAPI.getInstance(context, projectToken);
        return mixpanel;
    }

    public static Firebase getQuestionsFireBase(){
      Firebase f= new Firebase("https://blazing-fire-3105.firebaseio.com/questions");
        f.keepSynced(true);
        return f;

    }
    public static Firebase getRatingsFireBase(int usrdid,int pid){
        Firebase f= new Firebase("https://blazing-fire-3105.firebaseio" +
                ".com/product/ratings/"+pid+"/"+usrdid);
        f.keepSynced(true);
        return f;

    }
    public static Firebase getAllProductRatingsFireBase(int pid){
        Firebase f= new Firebase("https://blazing-fire-3105.firebaseio" +
                ".com/product/ratings/"+pid);
        f.keepSynced(true);
        return f;

    }
    public static Firebase getProductCommentsFireBase(int pid){
        Firebase f= new Firebase("https://blazing-fire-3105.firebaseio.com/product/comments/"+pid);
        f.keepSynced(true);
        return f;

    }
    public static Firebase getOrdersFireBase(){
        Firebase fire=new Firebase("https://blazing-fire-3105.firebaseio.com/buyer_orders");
        fire.keepSynced(true);
        return fire;
    }
    public static Firebase getAnswerssFireBase(String quizId){
      Firebase fire=   new Firebase("https://blazing-fire-3105.firebaseio.com/answers/"+quizId);
        fire.keepSynced(true);
        return fire;
    }
}

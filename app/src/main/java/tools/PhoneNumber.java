package tools;

/**
 * Created by robertnyangate on 3/30/16.
 */
public class PhoneNumber {
    public static boolean validCellPhone(String number)
    {
        return android.util.Patterns.PHONE.matcher(number).matches();
    }
}

package database;

/**
 * Created by robertnyangate on 3/16/16.
 */
public class Tables {

    public static  final String ITEMS_TABLE="wv_soko";

    public static final String CREATE_SOKO_TABLE="CREATE TABLE "+ITEMS_TABLE+" (" +
            "  pid integer primary key autoincrement," +
            "  pdct_title text NOT NULL," +
            "  pdct_desc text," +
            "  pdct_unitpce integer DEFAULT NULL," +
            "  pdct_ttl_selling varchar(200) DEFAULT NULL," +
            "  pdct_photo text," +
            "  pdct_category varchar(200) DEFAULT NULL," +
            "  pdct_county varchar(200) NOT NULL," +
            "  pdct_town varchar(200) NOT NULL," +
            "  pdct_estate varchar(210) DEFAULT NULL," +
            "  pdct_google_location varchar(200) DEFAULT NULL," +
            "  pdct_pub_date text DEFAULT NULL," +
            "  pdct_status integer DEFAULT 1," +
            "  pdct_expdate text DEFAULT NULL," +
            "  pdct_placed_ip varchar(100) DEFAULT NULL," +
            "  pdct_ownr integer DEFAULT NULL," +
            "  views text default null," +
            "  revised_date text not null"+
            ");";
    public static final String ANALYSIS_TABLE="soko_analytics";
    //Event type THUMBS-UP=1,THUMBS-DOWN=2,
    //EventJson {"deviceId":"12672","reason":"bad content"}
    public static final  String PRODUCT_ANALYSIS="CREATE TABLE "+ANALYSIS_TABLE+" (" +
            "  id integer PRIMARY KEY autoincrement," +
            "  event_type INT NOT NULL DEFAULT 1," +
            "  event_json TEXT NULL," +
            "  pid INT NOT NULL," +
            "  timestamp text not null);";
    public static final String REQUESTS_TABLE="soko_requests";
    public static final  String REQUESTS="CREATE TABLE "+REQUESTS_TABLE+" ("+
            "  id integer PRIMARY KEY AUTOINCREMENT,"+
            "  reqtype INT NOT NULL DEFAULT 1,"+
            "  reqjson TEXT NULL,"+
            "  status INT NOT NULL DEFAULT 0,"+
            "  keyid INT NOT NULL,"+
            "  timestamp text not null);";
    public static final String QUIZ_TABLE="mnewquestions";
    public static final  String NEW_QUIZES ="CREATE TABLE "+QUIZ_TABLE+" ("+
            "  quizid integer PRIMARY KEY AUTOINCREMENT,"+
            "  question TEXT NOT NULL,"+
            "  channel TEXT NOT NULL,"+
            "  userid INT NOT NULL,"+
            "  status INT NOT NULL DEFAULT 1,"+
            "  quizjson TEXT NULL,"+
            "  timestamp text not null);";
    public static final String ANSWERS_TABLE="mnewanswers";
    public static final  String ANSWERSFOR_QUIZES ="CREATE TABLE "+ANSWERS_TABLE+" (" +
            "  answerid INTEGER  PRIMARY KEY AUTOINCREMENT," +
            "  quizid INT NOT NULL," +
            "  status INT NOT NULL DEFAULT 1," +
            "  answer TEXT NOT NULL," +
            "  userid INT NOT NULL," +
            "  timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP," +
            "  answerjson TEXT NULL);";

    public static final String BUYERS_TABLE="buyer_items";
    public static final  String BUYER_ITEMS ="CREATE TABLE "+BUYERS_TABLE+" (" +
            "  id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "  buyer_location TEXT NOT NULL," +
            "  buyer_email TEXT NULL," +
            "  buyer_phone TEXT NOT NULL," +
            "  buyjson TEXT NULL," +
            "  timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP," +
            "  status INT NOT NULL DEFAULT 1," +
            "  expiry DATETIME NOT NULL);";

    public static final String USERS_TABLE="wv_tbl_user";

    public static final String CREATE_USERS_TABLE="CREATE TABLE "+USERS_TABLE+" (" +
            "  wv_uid integer primary key autoincrement," +
            "  full_name varchar(200) NOT NULL," +
            "  user_name varchar(20) NOT NULL," +
            "  user_email varchar(200) NOT NULL," +
            "  user_lastlogin varchar(200) DEFAULT NULL," +
            "  user_passcode varchar(40) NOT NULL," +
            "  user_newsletter integer NOT NULL DEFAULT 1," +
            "  user_confirm integer NOT NULL DEFAULT 0," +
            "  user_creationdate text NOT NULL," +
            "  user_confirmdate text DEFAULT NULL," +
            "  user_status integer NOT NULL DEFAULT 0," +
            "  user_cell varchar(200) DEFAULT NULL," +
            "  userip varchar(20) NOT NULL," +
            "  user_device char(4) NOT NULL"+
            ");";


    public static final String DEVICES_TABLE="user_devices";

    public static final String CREATE_DEVICES_TABLE="CREATE TABLE "+DEVICES_TABLE+" (" +
            "  id integer primary key autoincrement," +
            "  user_email varchar(200) DEFAULT NULL," +
            "  timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP," +
            "  user_device text NOT NULL"+
            ");";

    public static final String SUPER_CATEGORY_TABLE="supercategory";

    public static final String CREATE_SUPERCATEGORY_TABLE="CREATE TABLE "+SUPER_CATEGORY_TABLE+" (" +
            "  id integer primary key autoincrement," +
            "  name text"+
            ");";

    public static  final String CATEGORY_TABLE="category";

    public static final String CREATE_CATEGORY_TABLE="CREATE TABLE "+CATEGORY_TABLE+" (" +
            "  id integer primary key autoincrement," +
            "  name text NOT NULL," +
            "  image text"+
            ");";

    public static final String CHANNELS_TABLE="channels";

    public static final String CREATE_CHANNELS_TABLE="CREATE TABLE "+CHANNELS_TABLE+" (" +
            "  id integer primary key autoincrement," +
            "  name text NOT NULL," +
            "  categoryid integer NOT NULL"+
            ");";

    public  static final String SUBS_TABLE="subs";
    public static final String CREATE_SUBS_TABLE="CREATE TABLE "+SUBS_TABLE+" (" +
            "  id integer primary key autoincrement," +
            "  userid integer NOT NULL," +
            "  channelid integer NOT NULL"+
            ");";

    public static final String TABLE_userdet = "sym_console_user";


    public static final String COLUMN_ID = "user_id";
    public static final String COLUMN_fname = "first_name";
    public static final String COLUMN_lname = "last_name";
    public static final String COLUMN_email = "email";
    public static final String COLUMN_phone = "email";
    public static final String COLUMN_cardmask = "card_mask";
    public static final String COLUMN_cardmd5 = "card_md5";
    public static final String COLUMN_pass = "hashed_password";
    public static final String COLUMN_out = "logout";
    public static final String COLUMN_auth_meth = "auth_meth";
    public static final String COLUMN_user_role = "user_role";
    public static final String sym_confTable = "create table "
            + TABLE_userdet + "(" + COLUMN_ID
            + " text not null, " + COLUMN_fname
            + " text not null, "  + COLUMN_lname
            + " text not null, "  + COLUMN_email
            + " text null, "  + COLUMN_auth_meth
            + " text null   ,  "  + COLUMN_user_role
            + " text null    , "  + COLUMN_pass
            + " text null    );";
}

package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * Created by robertnyangate on 2/8/16.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "mkulimaapp.db";
    // current live version 8
    private static final int DATABASE_VERSION = 1;

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        //
        database.execSQL(Tables.CREATE_SOKO_TABLE);
        database.execSQL(Tables.CREATE_USERS_TABLE);
        database.execSQL(Tables.CREATE_CATEGORY_TABLE);
        database.execSQL(Tables.CREATE_SUPERCATEGORY_TABLE);
        database.execSQL(Tables.CREATE_CHANNELS_TABLE);
        database.execSQL(Tables.CREATE_SUBS_TABLE);
        database.execSQL(Tables.sym_confTable);
        database.execSQL(Tables.PRODUCT_ANALYSIS);
        database.execSQL(Tables.REQUESTS);
        database.execSQL(Tables.NEW_QUIZES);
        database.execSQL(Tables.ANSWERSFOR_QUIZES);
        database.execSQL(Tables.BUYER_ITEMS);
        database.execSQL(Tables.CREATE_DEVICES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading DataSource from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");


    }


}
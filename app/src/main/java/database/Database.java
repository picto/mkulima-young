package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.mkulimayoung.Answer;
import com.mkulimayoung.BuyerItems;
import com.mkulimayoung.DatabaseTable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import models.Product;
import models.Question;
import models.User;

/**
 * Created by robertnyangate on 3/16/16.
 */
public class Database {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private Context context;


    public Database(Context context) {
        this.context = context;
        dbHelper = new MySQLiteHelper(context);

    }

    public MySQLiteHelper MysDbHElper(Context context) {
        context = context;
        return dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public synchronized ArrayList<Product> getProducts(String country) {

        ArrayList<Product> products = new ArrayList<>();
        Cursor cursor = database.rawQuery("select * from " + Tables.ITEMS_TABLE +" where  " +
                "pdct_status>0 " +
                "and pdct_google_location ='"+country+"'" +
                " order " +
                "by pdct_pub_date" +
                " desc " +
                " limit 110", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Product product = new Product();
            //check country currency
//            product.setPrice("" + cursor.getDouble(cursor.getColumnIndex("pdct_unitpce")));
//            product.setId(cursor.getInt(cursor.getColumnIndex("pid")));
//            product.setImageUrl(cursor.getString(cursor.getColumnIndex("pdct_photo")));
//            product.setName(cursor.getString(cursor.getColumnIndex("pdct_title")));
//            product.setUserid(cursor.getInt(cursor.getColumnIndex("pdct_ownr")));
//            product.setTown(cursor.getString(cursor.getColumnIndex("pdct_town")));
//            product.setDescription(cursor.getString(cursor.getColumnIndex("pdct_desc")));
//            product.setCountry(cursor.getString(cursor.getColumnIndex("pdct_google_location")));
//            product.setCategory(cursor.getString(cursor.getColumnIndex("pdct_category")));
//            product.setDescription(cursor.getString(cursor.getColumnIndex("pdct_desc")));
            setProductOwner(product);
            products.add(product);

            cursor.moveToNext();
        }
        cursor.close();


        return products;

    }
    public synchronized ArrayList<Product> getProducts() {

        ArrayList<Product> products = new ArrayList<>();
        Cursor cursor = database.rawQuery("select * from " + Tables.ITEMS_TABLE +" where  " +
                "pdct_status>0 " +

                " order " +
                "by pdct_pub_date" +
                " desc " +
                " ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Product product = new Product();
            //check country currency
//            product.setPrice("" + cursor.getDouble(cursor.getColumnIndex("pdct_unitpce")));
//            product.setId(cursor.getInt(cursor.getColumnIndex("pid")));
//            product.setImageUrl(cursor.getString(cursor.getColumnIndex("pdct_photo")));
//            product.setName(cursor.getString(cursor.getColumnIndex("pdct_title")));
//            product.setUserid(cursor.getInt(cursor.getColumnIndex("pdct_ownr")));
//            product.setTown(cursor.getString(cursor.getColumnIndex("pdct_town")));
//            product.setDescription(cursor.getString(cursor.getColumnIndex("pdct_desc")));
//            product.setCountry(cursor.getString(cursor.getColumnIndex("pdct_google_location")));
//            product.setCategory(cursor.getString(cursor.getColumnIndex("pdct_category")));
//            product.setDescription(cursor.getString(cursor.getColumnIndex("pdct_desc")));
            setProductOwner(product);
            products.add(product);

            cursor.moveToNext();
        }
        cursor.close();


        return products;

    }

    public Product getProduct(Integer integer) {
        Cursor cursor = database.rawQuery("select * from " + Tables.ITEMS_TABLE + " where pid=" + integer
                + " " +
                "order by" +
                " " +
                "pid desc " +
                "", null);
        cursor.moveToFirst();
        Product product = null;
        while (!cursor.isAfterLast()) {
            //check country currency
            product=new Product();
//            product.setPrice("" + cursor.getDouble(cursor.getColumnIndex("pdct_unitpce")));
//            product.setId(cursor.getInt(cursor.getColumnIndex("pid")));
//            product.setImageUrl(cursor.getString(cursor.getColumnIndex("pdct_photo")));
//            product.setName(cursor.getString(cursor.getColumnIndex("pdct_title")));
//            product.setUserid(cursor.getInt(cursor.getColumnIndex("pdct_ownr")));
//            product.setTown(cursor.getString(cursor.getColumnIndex("pdct_town")));
//            product.setCountry(cursor.getString(cursor.getColumnIndex("pdct_google_location")));
//            product.setCategory(cursor.getString(cursor.getColumnIndex("pdct_category")));
//            product.setDescription(cursor.getString(cursor.getColumnIndex("pdct_desc")));
//            product.setStatus(cursor.getInt(cursor.getColumnIndex("pdct_status")));
            setProductOwner(product);

            cursor.moveToNext();
        }
        cursor.close();

        return product;

    }

    public void setProductOwner(Product productOwner) {
        Cursor c = database.rawQuery("select * from wv_tbl_user where wv_uid=" + productOwner.getFull_name(),
                null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
//            productOwner.setMobile("" + c.getString(c.getColumnIndex("user_cell")));
//            productOwner.setOwner("" + c.getString(c.getColumnIndex("full_name")));
            c.moveToNext();
        }
        c.close();
    }

    public User getUser(String email, String password) {
        Cursor c = database.rawQuery("select * from wv_tbl_user where user_email = '" + email + "' and " +
                        "user_passcode = '" + password + "'",
                null);
        c.moveToFirst();
        User user = null;
        while (!c.isAfterLast()) {
            user = new User();
            user.setMobile("" + c.getString(c.getColumnIndex("user_cell")));
            user.setEmail(email);
            user.setName("" + c.getString(c.getColumnIndex("full_name")));
            user.setId(c.getInt(c.getColumnIndex("wv_uid")));
            c.moveToNext();
        }
        c.close();
        return user;
    }

    public boolean isPhoneInUse(String phone) {
        Cursor c = database.rawQuery("select * from wv_tbl_user where  " +
                        "user_cell = '" + phone + "'",
                null);
        c.moveToFirst();
        boolean user = false;
        while (!c.isAfterLast()) {
            user = true;
            c.moveToNext();
        }
        c.close();
        return user;
    }

    public boolean isEmailInUse(String email) {
        Cursor c = database.rawQuery("select * from wv_tbl_user where  " +
                        "user_email = '" + email + "'",
                null);
        c.moveToFirst();
        boolean user = false;
        while (!c.isAfterLast()) {
            user = true;
            c.moveToNext();
        }
        c.close();
        return user;
    }

    public User registerUser(String email, String phone, String name, String password) {

        database.execSQL("insert into wv_tbl_user (user_email,user_passcode,full_name,user_cell," +
                "user_name,user_creationdate,userip,user_device)" +
                " " +
                "values ('" + email + "','" + password + "','" + name + "','" + phone + "','" + phone + "'," +
                "'" + properFormat.format(new Date()) + "','" + phone + "','" + phone + "')");
        return getUser(email, password);

    }

    public static SimpleDateFormat properFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static int THUMBS_UP = 1;
    public static int THUMBS_DOWN = 2;
    public static int VIEWS = 3;

    public int getMyEvents(int pid, String deviceId, int eventTypes) {

        JSONObject jsons = new JSONObject();
        int events = 0;
        Cursor cursor = database.rawQuery("select * from " + Tables.ANALYSIS_TABLE + " where event_type" +
                        " = " + eventTypes + " and pid = " + pid,
                null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            try {
                jsons = new JSONObject(cursor.getString(cursor.getColumnIndex("event_json")));
                if (jsons.has("deviceid")) {
                    events = cursor.getInt(cursor.getColumnIndex("id"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            cursor.moveToNext();
        }
        cursor.close();
        return events;
    }

    public int getAllEvents(int pid, String deviceId, int eventTypes) {

        JSONObject jsons = new JSONObject();
        int events = 0;
        Cursor cursor = database.rawQuery("select * from " + Tables.ANALYSIS_TABLE + " where event_type" +
                        " = " + eventTypes + " and pid = " + pid,
                null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            events++;
            try {
                jsons = new JSONObject(cursor.getString(cursor.getColumnIndex("event_json")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            cursor.moveToNext();
        }
        cursor.close();
        return events;
    }

//    public void thumbDown(Product itemOnView, String deviceId) {
//
//        JSONObject jsons = new JSONObject();
//        try {
//            jsons.accumulate("deviceid", deviceId);
//            jsons.accumulate("reason", "default");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        int id = getMyEvents(itemOnView.getId(), deviceId, THUMBS_UP);
//        if (id > 0) {
//            database.execSQL("update " + Tables.ANALYSIS_TABLE + " set event_type=" + THUMBS_DOWN + " " +
//                    " where id = " + id, new String[]{});
//        } else
//            database.execSQL("insert into " + Tables.ANALYSIS_TABLE + " (pid,event_type,event_json," +
//                    "timestamp) values(" + itemOnView.getId() + "," + THUMBS_DOWN + ",'" + jsons.toString() + "'," +
//                    "'" + properFormat.format(new Date()) + "'" +
//                    ")");
//    }

    public void itsAView(Product itemOnView, String deviceId) {
     if(itemOnView==null)
         return;
        JSONObject jsons = new JSONObject();
        try {
            jsons.accumulate("deviceid", deviceId);
            jsons.accumulate("reason", "default");
        } catch (Exception e) {
            e.printStackTrace();
        }
        int id = 0;
        if (id > 0) {

        } else
            database.execSQL("insert into " + Tables.ANALYSIS_TABLE + " (pid,event_type,event_json," +
                    "timestamp) values(" + itemOnView.getPid() + "," + VIEWS + ",'" + jsons.toString
                    () + "'," +
                    "'" + properFormat.format(new Date()) + "'" +
                    ")");
    }

    public void thumbUp(Product itemOnView, String deviceId) {
        JSONObject jsons = new JSONObject();
        try {
            jsons.accumulate("deviceid", deviceId);
            jsons.accumulate("reason", "default");
        } catch (Exception e) {
            e.printStackTrace();
        }
        int id = 0;
        if (id > 0) {
            database.execSQL("update " + Tables.ANALYSIS_TABLE + " set event_type=" + THUMBS_UP + " where" +
                    " id=" + id, new String[]{});
        } else
            database.execSQL("insert into " + Tables.ANALYSIS_TABLE + " (pid,event_type,event_json," +
                    "timestamp) values(" + itemOnView.getPid() + "," + THUMBS_UP + ",'" + jsons
                    .toString() + "'," +
                    "'" + properFormat.format(new Date()) + "'" +
                    ")");
    }

    public ArrayList<Question> getAllQuestions() {
        ArrayList<Question> allQuestions = new ArrayList<>();
        Cursor cursor = database.rawQuery("select * from " + Tables.QUIZ_TABLE + " where status=1",
                null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Question question = new Question();
            question.setChannel(cursor.getString(cursor.getColumnIndex("channel")));
            question.setQuestion_json(cursor.getString(cursor.getColumnIndex("quizjson")));
            question.setTitle(cursor.getString(cursor.getColumnIndex("question")));
            question.setId(cursor.getInt(cursor.getColumnIndex("quizid")));
            question.setDatePosted(cursor.getString(cursor.getColumnIndex("timestamp")));
            question.setUsername(getUserName(cursor.getInt(cursor.getColumnIndex("userid"))));
            question.setEmail(getUserEmail(cursor.getInt(cursor.getColumnIndex("userid"))));
            fillAnswers(question);
            allQuestions.add(question);
            cursor.moveToNext();
        }
        cursor.close();
        return allQuestions;
    }
    public ArrayList<Question> getAllPestQuestions() {
        ArrayList<Question> allQuestions = new ArrayList<>();
        Cursor cursor = database.rawQuery("select * from " + Tables.QUIZ_TABLE + " where status=5",
                null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Question question = new Question();
            question.setChannel(cursor.getString(cursor.getColumnIndex("channel")));
            question.setQuestion_json(cursor.getString(cursor.getColumnIndex("quizjson")));
            question.setTitle(cursor.getString(cursor.getColumnIndex("question")));
            question.setId(cursor.getInt(cursor.getColumnIndex("quizid")));
            question.setDatePosted(cursor.getString(cursor.getColumnIndex("timestamp")));
            question.setUsername(getUserName(cursor.getInt(cursor.getColumnIndex("userid"))));
            question.setEmail(getUserEmail(cursor.getInt(cursor.getColumnIndex("userid"))));
            fillAnswers(question);
            allQuestions.add(question);
            cursor.moveToNext();
        }
        cursor.close();
        return allQuestions;
    }

    private String getUserName(int userid) {
        Cursor cursor = database.rawQuery("select user_name from " + Tables.USERS_TABLE + " where  wv_uid= " +
                        "" + userid,
                null);
        cursor.moveToFirst();
        String username = "farmer";

        while (!cursor.isAfterLast()) {
            username = cursor.getString(cursor.getColumnIndex("user_name"));
            cursor.moveToNext();
        }
        cursor.close();
        return username;
    }
    private String getUserEmail(int userid) {
        Cursor cursor = database.rawQuery("select user_email from " + Tables.USERS_TABLE + " where  " +
                        "wv_uid= " +
                        "" + userid,
                null);
        cursor.moveToFirst();
        String username = "email";

        while (!cursor.isAfterLast()) {
            username = cursor.getString(cursor.getColumnIndex("user_email"));
            cursor.moveToNext();
        }
        cursor.close();
        return username;
    }
    public String getUserPhone(String email) {
        Cursor cursor = database.rawQuery("select user_cell from " + Tables.USERS_TABLE + " where" +
                "  " +
                        "user_email= '" +
                        "" + email+"'",
                null);
        cursor.moveToFirst();
        String username = "";

        while (!cursor.isAfterLast()) {
            username = cursor.getString(cursor.getColumnIndex("user_cell"));
            cursor.moveToNext();
        }
        cursor.close();
        return username;
    }

    private void fillAnswers(Question question) {
        Cursor cursor = database.rawQuery("select * from " + Tables.ANSWERS_TABLE + " where quizid = " +
                        "" + question.getId(),
                null);
        cursor.moveToFirst();
        JSONArray jsonArr=new JSONArray();
        while (!cursor.isAfterLast()) {
            JSONObject json=new JSONObject();
            try {
                json.accumulate("answerid",cursor.getInt(cursor.getColumnIndex("answerid")));
                json.accumulate("status",cursor.getInt(cursor.getColumnIndex("status")));
                json.accumulate("answer",cursor.getString(cursor.getColumnIndex("answer")));
                json.accumulate("userid",cursor.getInt(cursor.getColumnIndex("userid")));
                json.accumulate("timestamp",cursor.getString(cursor.getColumnIndex("timestamp")));
                json.accumulate("username",getUserName(cursor.getInt(cursor.getColumnIndex
                        ("userid"))));
                jsonArr.put(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            cursor.moveToNext();
        }
        question.setAnswers_json(jsonArr.toString());
        cursor.close();

    }

    public void createQuestion(String question, String channel, int userid) {
        database.execSQL("insert into " + Tables.QUIZ_TABLE + " (question,timestamp,channel,userid) values('" + question + "','" + properFormat.format(new Date()) + "'," +
                "'" + channel + "'," + userid + ")");
    }
    public void createPestQuestion(String question, String channel, int userid) {
        database.execSQL("insert into " + Tables.QUIZ_TABLE + " (question,timestamp,channel," +
                "userid,status) values('" + question + "','" + properFormat.format(new Date()) +
                "'," +
                "'" + channel + "'," + userid + ",5)");
    }

    public void saveAnswer(int quizid, String message, int id) {
        database.execSQL("insert into " + Tables.ANSWERS_TABLE + " (quizid,timestamp,answer," +
                "userid) values" +
                "(" + quizid + ",'" + properFormat.format(new Date()) + "'," +
                "'" + message + "'," + id + ")");
    }
    public  ArrayList<Answer> getAnswers(int quizid){
        ArrayList<Answer> allQuestions = new ArrayList<>();
        Cursor cursor = database.rawQuery("select * from " + Tables.ANSWERS_TABLE + " where " +
                "quizid="+quizid+" and status=1", null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Answer answer = new Answer();
            answer.setMessage(cursor.getString(cursor.getColumnIndex("answer")));
            answer.setUserid(cursor.getInt(cursor.getColumnIndex("userid")));
            answer.setAnswerid(cursor.getInt(cursor.getColumnIndex("answerid")));
            answer.setTimestamp(cursor.getString(cursor.getColumnIndex("timestamp")));
            answer.setUsername(getUserName(cursor.getInt(cursor.getColumnIndex
                    ("userid"))));
            allQuestions.add(answer);
            cursor.moveToNext();
        }
        cursor.close();
        return allQuestions;
    }

    public void deleteAnswer(int answerid) {
        database.execSQL("update " + Tables.ANSWERS_TABLE + " set status=" + 0 + " where" +
                " answerid =" + answerid, new String[]{});
    }
    public void updateAnswer(int answerid,String newAnswer) {
        database.execSQL("update " + Tables.ANSWERS_TABLE + " set answer= '" + newAnswer + "' where" +
                " answerid =" + answerid, new String[]{});
    }

    public void updatePassword(String email, String deviceId) {
        database.execSQL("update " + Tables.USERS_TABLE + " set user_passcode= '" + deviceId +
                "' where" +
                " user_email ='" + email+"'", new String[]{});
    }
    public void postProduct(ContentValues cv) {
        database.insert(Tables.ITEMS_TABLE,null,cv);
        Log.d("PRODUCT", "product posted");
    }

    public void updateProductSold(int catid,int status) {
        database.execSQL("update " + Tables.ITEMS_TABLE + " set pdct_status= " + status +
                " where" +
                " pid ='" + catid + "'", new String[]{});
    }

    public  ArrayList<Product> getMyProducts(int id) {

        ArrayList<Product> products = new ArrayList<>();
        Cursor cursor = database.rawQuery("select * from " + Tables.ITEMS_TABLE +" where  " +
                "pdct_ownr= " +id+" "+

                " order " +
                "by pdct_pub_date" +
                " desc " +
                " ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Product product = new Product();
            //check country currency
//            product.setPrice("" + cursor.getDouble(cursor.getColumnIndex("pdct_unitpce")));
//            product.setId(cursor.getInt(cursor.getColumnIndex("pid")));
//            product.setImageUrl(cursor.getString(cursor.getColumnIndex("pdct_photo")));
//            product.setName(cursor.getString(cursor.getColumnIndex("pdct_title")));
//            product.setUserid(cursor.getInt(cursor.getColumnIndex("pdct_ownr")));
//            product.setTown(cursor.getString(cursor.getColumnIndex("pdct_town")));
//            product.setDescription(cursor.getString(cursor.getColumnIndex("pdct_desc")));
//            product.setCountry(cursor.getString(cursor.getColumnIndex("pdct_google_location")));
//            product.setCategory(cursor.getString(cursor.getColumnIndex("pdct_category")));
//            product.setDescription(cursor.getString(cursor.getColumnIndex("pdct_desc")));
            setProductOwner(product);
            products.add(product);

            cursor.moveToNext();
        }
        cursor.close();


        return products;

    }

    public void makeDevice(String myDeviceId) {
        database.execSQL("insert into " + Tables.DEVICES_TABLE + " (user_device) values" +
                "(" + myDeviceId + ")");
    }
    public String getMyDevice() {
        Cursor c=database.rawQuery("select user_device from " + Tables.DEVICES_TABLE + " order by id desc " +
                "limit 1", null);
        c.moveToFirst();
        String mydevice=null;
        while (!c.isAfterLast()){
            mydevice=c.getString(c.getColumnIndex("user_device"));
            c.moveToNext();
        }c.close();
    return mydevice;
    }

    public ArrayList<BuyerItems> getBuyerItems(String email) {

        ArrayList<BuyerItems> products = new ArrayList<>();
        Cursor cursor = database.rawQuery("select * from " + Tables.BUYERS_TABLE +" where  " +
                "buyer_email= '" +email+"' and status=1 "+

                " order " +
                "by timestamp" +
                " desc " +
                " ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            BuyerItems product = new BuyerItems();
            //check country currency
            product.setId(cursor.getInt(cursor.getColumnIndex("id")));
            product.setDate(cursor.getString(cursor.getColumnIndex("timestamp")));
            try{
                JSONObject json=new JSONObject(cursor.getString(cursor.getColumnIndex("buyjson")));
                product.setName(json.getString("name"));
                product.setCountry(json.getString("country"));
                product.setTown(json.getString("town"));
                product.setDescription(json.getString("desc"));
            }catch (Exception e){

            }
            products.add(product);

            cursor.moveToNext();
        }
        cursor.close();


        return products;

    }
    public void addBuyerItem(User user,String name,String desc,String country,String town){

        JSONObject json=new JSONObject();
        try {
            json.accumulate("name",name);
            json.accumulate("desc",desc);
            json.accumulate("country",country);
            json.accumulate("town",town);

        }catch (Exception e){

        }

        ContentValues cv=new ContentValues();
        cv.put("buyjson",json.toString());
        cv.put("buyer_email",user.getEmail());
        cv.put("buyer_phone",user.getMobile());
        cv.put("expiry",properFormat.format(new Date()));
        cv.put("buyer_location", country + " - " + town);
        database.insert(Tables.BUYERS_TABLE, null, cv);

    }
    public void deleteBuyerItem(int buyid){

        database.execSQL("update " + Tables.BUYERS_TABLE + " set status= " + 0 +
                " where" +
                " id =" + buyid + "", new String[]{});


    }

    public void updateUser(User thisUser,String fullname) {
        database.execSQL("update " + Tables.USERS_TABLE + " set user_cell= '" + thisUser.getMobile() +
                "',user_name='"+thisUser.getUsername()+"',full_name='"+fullname+"'  where" +
                " user_email ='" + thisUser.getEmail()+"'", new String[]{});
    }

    public void updateProduct(ContentValues product, int id) {
        database.update(Tables.ITEMS_TABLE,product,"pid="+id,new String[]{});
    }
}


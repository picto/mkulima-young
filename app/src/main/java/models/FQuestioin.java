package models;

/**
 * Created by robertnyangate on 4/24/16.
 */
public class FQuestioin {

    public FQuestioin(){}

    private String title;
    private String user;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String email;
    private String date;

    public String getQuizid() {
        return quizid;
    }

    public void setQuizid(String quizid) {
        this.quizid = quizid;
    }

    private String quizid;

    public FQuestioin(String quiz,String user,String date,String quizid,String email){
        this.date=date;
        this.title=quiz;
        this.user=user;
        this.quizid=quizid;
        this.email=email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

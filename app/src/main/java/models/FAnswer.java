package models;

/**
 * Created by robertnyangate on 4/24/16.
 */
public class FAnswer {
    private String user;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String email;
    private String answer;
    private String date;
    private String questionid;

    public String getAnswerid() {
        return answerid;
    }

    public void setAnswerid(String answerid) {
        this.answerid = answerid;
    }

    private String answerid;


    public FAnswer(String answer,String user,String date,String questionid,String answerid,
                   String email){
        this.answer=answer;
        this.questionid=questionid;
        this.date=date;
        this.answerid=answerid;
        this.user=user;
        this.email=email;
    }
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getQuestionid() {
        return questionid;
    }

    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    public FAnswer(){}

}

package models;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.mkulimayoung.ImageLoader;
import com.mkulimayoung.mkulimaApp.R;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by robertnyangate on 4/1/16.
 */
public class QuizCard extends Card
{

    protected TextView mTitleTV,examplesTV;

    private String name, examples;
    private int catid;
    private ImageView image;
    private Drawable imagedr;
    ImageLoader loader;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCatid() {
        return catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }

    public String getExamples() {
        return examples;
    }

    public void setExamples(String examples) {
        this.examples = examples;
    }

    public PopupMenu getPopupMenu() {
        return popupMenu;
    }

    public void setPopupMenu(PopupMenu popupMenu) {
        this.popupMenu = popupMenu;
    }

    private PopupMenu popupMenu;

    public QuizCard(Context context)
    {
        this(context, R.layout.catcard);
    }

    public QuizCard(Context context, int i)
    {
        super(context, i);
        init();
    }
private String imageUrl;
    public QuizCard(Context context, String name,String other,int catid,Drawable drawable)
    {
        this(context, R.layout.quizcard);
        this.name=name;
        this.examples=other;
        this.catid=catid;
        this.imagedr=drawable;
    } public QuizCard(Context context, String name,String other,int catid,String imageurl)
    {
        this(context, R.layout.quizcard);
        this.name=name;
        this.examples=other;
        this.catid=catid;
        loader=new ImageLoader(context);
        this.imageUrl=imageurl;

    }


    private void init()
    {
        setOnClickListener(new OnCardClickListener() {


            public void onClick(Card card, View view) {
            }


        });
    }

    public void setValues(String s, int i)
    {
        if (name != null)
        {
            mTitleTV.setText(s);
        }


        notifyDataSetChanged();
    }

    public void setImage(Drawable rid) {
        if(image!=null)
            image.setImageDrawable(rid);
    }

    public void setupInnerViewElements(ViewGroup viewgroup, View view)
    {
        mTitleTV = (TextView)viewgroup.findViewById(R.id.titleTv);
        examplesTV = (TextView)viewgroup.findViewById(R.id.examplesTV);
        image = (ImageView)viewgroup.findViewById(R.id.colorBorder);
        if (name!=null)
        {
            mTitleTV.setText(name);
        }
        if (examples != null)
        {
            examplesTV.setText(examples);
        }
        if(imagedr!=null)
            image.setImageDrawable(imagedr);
        if(loader!=null && imageUrl!=null)
            loader.DisplayImage(imageUrl,image);

    }


}


package models;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by robertnyangate on 3/13/16.
 */
public class Product extends RealmObject {
    @Expose
    private String pdct_photo;
    @Expose
    private String pdct_ttl_selling;
    @Expose
    private String pdct_pub_date;
    @Expose
    @PrimaryKey
    private int pid;
    @Expose
    private String pdct_title;
    @Expose
    private String user_cell;
    @Expose
    private String user_name;
    @Expose
    private String pdct_google_location;
    @Expose
    private String pdct_ownr;
    @Expose
    private String pdct_category;
    @Expose
    private String pdct_desc;
    @Expose
    private String pdct_town;
    @Expose
    private String pdct_status;
    @Expose
    private String pdct_unitpce;
    @Expose
    private String full_name;

    public String getPdct_photo ()
    {
        return pdct_photo;
    }

    public void setPdct_photo (String pdct_photo)
    {
        this.pdct_photo = pdct_photo;
    }

    public String getPdct_ttl_selling()
    {
        return pdct_ttl_selling;
    }

    public void setPdct_ttl_selling(String Pdct_ttl_selling)
    {
        this.pdct_ttl_selling=Pdct_ttl_selling;
    }

    public String getPdct_pub_date ()
    {
        return pdct_pub_date;
    }

    public void setPdct_pub_date (String pdct_pub_date)
    {
        this.pdct_pub_date = pdct_pub_date;
    }

    public int getPid ()
    {
        return pid;
    }

    public void setPid (int pid)
    {
        this.pid = pid;
    }

    public String getPdct_title ()
    {
        return pdct_title;
    }

    public void setPdct_title (String pdct_title)
    {
        this.pdct_title = pdct_title;
    }

    public String getUser_cell ()
    {
        return user_cell;
    }

    public void setUser_cell (String user_cell)
    {
        this.user_cell = user_cell;
    }

    public String getUser_name ()
    {
        return user_name;
    }

    public void setUser_name (String user_name)
    {
        this.user_name = user_name;
    }

    public String getPdct_google_location ()
    {
        return pdct_google_location;
    }

    public void setPdct_google_location (String pdct_google_location)
    {
        this.pdct_google_location = pdct_google_location;
    }

    public String getPdct_ownr ()
    {
        return pdct_ownr;
    }

    public void setPdct_ownr (String pdct_ownr)
    {
        this.pdct_ownr = pdct_ownr;
    }

    public String getPdct_category ()
    {
        return pdct_category;
    }

    public void setPdct_category (String pdct_category)
    {
        this.pdct_category = pdct_category;
    }

    public String getPdct_desc ()
    {
        return pdct_desc;
    }

    public void setPdct_desc (String pdct_desc)
    {
        this.pdct_desc = pdct_desc;
    }

    public String getPdct_town ()
    {
        return pdct_town;
    }

    public void setPdct_town (String pdct_town)
    {
        this.pdct_town = pdct_town;
    }

    public String getPdct_status ()
    {
        return pdct_status;
    }

    public void setPdct_status (String pdct_status)
    {
        this.pdct_status = pdct_status;
    }

    public String getPdct_unitpce ()
    {
        return pdct_unitpce;
    }

    public void setPdct_unitpce (String pdct_unitpce)
    {
        this.pdct_unitpce = pdct_unitpce;
    }

    public String getFull_name ()
    {
        return full_name;
    }

    public void setFull_name (String full_name)
    {
        this.full_name = full_name;
    }

}

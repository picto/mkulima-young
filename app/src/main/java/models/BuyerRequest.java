package models;

/**
 * Created by robertnyangate on 4/24/16.
 */
public class BuyerRequest {
    public BuyerRequest(){}

    public BuyerRequest(String product,String description,String location,String date,String
            user,String id,String email){

        this.user=user;
        this.location=location;
        this.date=date;
        this.description=description;
        this.product=product;
        this.id=id;
        this.email=email;
    }

    private String user;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    private String country;
    private String product;
    private String description;
    private String location;
    private String date;
    private String id;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String email;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

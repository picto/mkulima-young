package models;

/**
 * Created by robertnyangate on 4/1/16.
 */
public class Question {
    private int id;
    private String channel;
    private String title;
    private String answers_json;

    public String getQuestion_json() {
        return question_json;
    }

    public void setQuestion_json(String question_json) {
        this.question_json = question_json;
    }

    private String question_json;
    private String datePosted;
    private String username;
    private String email;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String imageUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnswers_json() {
        return answers_json;
    }

    public void setAnswers_json(String answers_json) {
        this.answers_json = answers_json;
    }

    public String getDatePosted() {
        return datePosted;
    }

    public void setDatePosted(String datePosted) {
        this.datePosted = datePosted;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

package models;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by robertnyangate on 3/13/16.
 */
public class Category extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private String slug;
    private String imageUrl;

    public int getSuperid() {
        return superid;
    }

    public void setSuperid(int superid) {
        this.superid = superid;
    }

    @Index
    private int superid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

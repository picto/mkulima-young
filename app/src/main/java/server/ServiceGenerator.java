package server;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by robertnyangate on 4/21/16.
 */
public class ServiceGenerator {
    public static final String API_BASE_URL = "http://mkulimayoung.com/api/v1/";
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();


    private static Gson gson = new GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .setLenient()
            .create();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson));
    public static <S> S createService(Class<S> serviceClass) {
        httpClient.connectTimeout(3, TimeUnit.MINUTES);
        httpClient.readTimeout(3,TimeUnit.MINUTES);
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }


    public static final String MYBASE_URL = "http://mkulimayoung.com/";
    private static Retrofit.Builder builderupload =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson));

    public static <S> S createUploadService(Class<S> serviceClass) {
        httpClient.connectTimeout(5, TimeUnit.MINUTES);
        httpClient.readTimeout(5,TimeUnit.MINUTES);
        Retrofit retrofit = builderupload.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
}

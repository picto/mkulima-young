package server;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mkulimayoung.Functions;
import com.mkulimayoung.MyApp;
import com.mkulimayoung.TestInternet;

import java.util.List;

import database.Database;
import io.realm.Realm;
import models.Farmers;
import models.Product;
import models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tools.Configs;

/**
 * Created by robertnyangate on 4/21/16.
 */
public class SokoContext {

    public static void setProducts(final Context context) {
        SokoClient client = ServiceGenerator.createService(SokoClient.class);
        final Call<List<Product>> productjson = client.products();
        echo("Loading products next");
        productjson.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                try {
                    if (response.body() != null && response.body().size() > 0) {
                        SokoContext.echo("received products data " + response.body());

                        final Realm realm  =Configs.getRealmInstance(context);
                        realm.beginTransaction();
                        realm.allObjects(Product.class).deleteAllFromRealm();
                        realm.copyToRealmOrUpdate(response.body());
                        realm.commitTransaction();
                        SokoContext.echo("received products " + realm.allObjects(Product
                                .class).size());

                    }



                } catch (Exception e) {
                    e.printStackTrace();
                    echoError("Error loading products: " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                echoError("failed to load products: " + t.getMessage());
            }
        });


    }

    public static User getUser(Farmers farmer) {
        User user = new User();
        user.setEmail(farmer.getEmail());
        user.setMobile(farmer.getMobile());
        user.setName(farmer.getName());
        user.setUsername(farmer.getUsername());
        user.setPasscode(farmer.getPasscode());
        user.setRegisterDate(farmer.getRegisterDate());
        user.setId(farmer
                .getId());
        return user;
    }

    public static Farmers getFarmer(User farmer) {
        Farmers user = new Farmers();
        user.setEmail(farmer.getEmail());
        user.setMobile(farmer.getMobile());
        user.setPasscode(farmer.getPasscode());
        user.setUsername(farmer.getUsername());
        user.setName(farmer.getName());
        user.setRegisterDate(farmer.getRegisterDate());
        user.setId(farmer
                .getId());
        return user;
    }

    public static void setUsers(final Context context) {

        SokoUsers.Members client = ServiceGenerator.createService(SokoUsers.Members.class);
        final Call<List<Farmers>> productjson = client.setMembers();

        productjson.enqueue(new Callback<List<Farmers>>() {
            @Override
            public void onResponse(Call<List<Farmers>> call, Response<List<Farmers>> response) {
                try {
                    SokoContext.echo("received members data " + response.body());
                    final Realm realm  =Configs.getRealmInstance(context);
                    realm.beginTransaction();

                    realm.copyToRealmOrUpdate(response.body());
                    realm.commitTransaction();

                } catch (Exception e) {
                    e.printStackTrace();
                    SokoContext.echoError("error in received members data " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<List<Farmers>> call, Throwable t) {
                echoError("Error loading users " + t.getLocalizedMessage());
            }
        });

    }

    public static void updateUser(final User user) {
        SokoUsers.UpdateMember client = ServiceGenerator.createService(SokoUsers.UpdateMember.class);
        final Call<List<User>> productjson = client.updateprofile(user);
        SokoContext.echo("received profile data. updating profile");
        productjson.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                try {
                    for (User user1 : response.body())
                        SokoContext.echo("received profile data " + user1.getName());


                } catch (Exception e) {
                    e.printStackTrace();
                    SokoContext.echoError("error in received profile data " + e.getLocalizedMessage
                            ());
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                echoError("Error loading profile " + t.getLocalizedMessage());
            }
        });

    }

    public static void echo(String message) {
        Log.d("SOKO", ">>>" + message);
    }

    public static void echoError(String message) {
        Log.e("SOKO_ERROR", ">>>" + message);
    }
}

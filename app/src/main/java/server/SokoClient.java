package server;

import com.mkulimayoung.GlobalItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import models.Product;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by robertnyangate on 4/21/16.
 */
public interface SokoClient {
    @GET("soko.php")
//    Call<JSONArray>products();
    Call<List<Product>> products();




}

package server;

import java.util.List;

import models.Farmers;
import models.Product;
import models.User;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit.mime.TypedFile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by robertnyangate on 4/23/16.
 */
public class SokoUsers {
    public interface SokoLogin {
        @POST("login.php")
        Call<List<User>> profile(@Body User user);

    }
    public interface PostProduct {
        @POST("createProduct.php")
        Call<List<Product>> postProduct(@Body Product product);

    }
    public interface UpdateProduct {
        @POST("updateProduct.php")
        Call<List<Product>> postProductUpdate(@Body Product product);

    }
    public interface NewMember {
        @POST("registerme.php")
        Call<List<User>> profile(@Body User user);

    }

    public interface UpdateMember {
        @POST("updateme.php")
        Call<List<User>> updateprofile(@Body User user);

    }

    public interface Members {
        @POST("rnmku.php")
        Call<List<Farmers>> setMembers();

    }

    public interface FileUploadService {


        @Multipart
        @POST("uploaded.php")
        Call<ResponseBody> upload(@Part("name") RequestBody description,
                                  @Part MultipartBody.Part file);
    }
}
